﻿Imports CommonLibrary

Public Class frmEAUpdateSummary

    Private SummaryData As New clsEAUpdateSummaryData
    Private ListData As New clsListData

    Private EANameGridState As clsGridState
    Private CGAGridState As clsGridState
    Private VisitedNotLiveGridState As clsGridState
    Private ArrangementMadeGridState As clsGridState
    Private FeeAddedGridState As clsGridState
    Private CollectedGridState As clsGridState
    Private NOIGridState As clsGridState
    Private VisitedGridState As clsGridState
    Private WorkTypeGridState As clsGridState
    Private AuditCheckGridState As clsGridState
    Private GoneAwayGridState As clsGridState
    Private VisitAfterPIFGridState As clsGridState
    Private AuditCheckByGridState As clsGridState
    Private ClientNameGridState As clsGridState
    Private NumberOfVisitsPAGridState As clsGridState

    Private ColSort As String

    Private ParamList As String

#Region "New and open"

    Public Sub New()
        Try
            InitializeComponent()

            SetFormIcon(Me)

            cmdRefreshDB.Visible = UserCanRefresh

            ParamList = UserCompanyID.ToString & ",null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,12"

            ' Now instantiate these
            EANameGridState = New clsGridState(dgvEAName)
            CGAGridState = New clsGridState(dgvCGA)
            VisitedNotLiveGridState = New clsGridState(dgvVisitedNotLive)
            ArrangementMadeGridState = New clsGridState(dgvArrangementMade)
            FeeAddedGridState = New clsGridState(dgvFeeAdded)
            CollectedGridState = New clsGridState(dgvCollected)
            NOIGridState = New clsGridState(dgvNOI)
            VisitedGridState = New clsGridState(dgvVisited)
            WorkTypeGridState = New clsGridState(dgvWorkType)
            AuditCheckGridState = New clsGridState(dgvAuditCheck)
            GoneAwayGridState = New clsGridState(dgvGoneAway)
            VisitAfterPIFGridState = New clsGridState(dgvVisitAfterPIF)
            AuditCheckByGridState = New clsGridState(dgvAuditCheckBy)
            ClientNameGridState = New clsGridState(dgvClientName)
            NumberOfVisitsPAGridState = New clsGridState(dgvNumberOfVisitsPA)

            AddControlHandlers()

            cmsSummary.Items.Add("View Cases")
            cmsSummary.Items.Add("Copy")
            cmsSummary.Items.Add("Select All")

            cmsList.Items.Add("Copy")
            cmsList.Items.Add("Select All")

            cmsForm.Items.Add("Allocation View")
            cmsForm.Items.Add("Bailiff View")
            cmsForm.Items.Add("Post Enforcement View")

            cboEATeam.SelectedIndex = 0

            SummaryData.GetSummary(ParamList, True)
            dgvSummary.DataSource = SummaryData.SummaryDataView

            FormatGridColumns(dgvSummary)

            ListData.GetEAUpdateList(ParamList & ",'Total'")
            SetListGrids()
            FormatListColumns()

            cboPeriodType.DataSource = SummaryData.PeriodTypeDataView
            cboPeriodType.ValueMember = "PeriodTypeID"
            cboPeriodType.DisplayMember = "PeriodTypeDesc"

            cboCompany.DataSource = SummaryData.CompanyDataView
            cboCompany.ValueMember = "CompanyID"
            cboCompany.DisplayMember = "CompanyDesc"

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub frmSummary_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Try
            RemoveSelectionHandlers()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub frmCaseSummary_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Me.MouseUp
        Try
            If e.Button = MouseButtons.Right Then
                cmsForm.Show(sender, e.Location)
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub frmSummary_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        Try
            SetSelections() 'Arrays will be nothing so clear all selections - by default the first row in a datagridview is always selected
            dgvSummary.ClearSelection()

            radAbs.Checked = True
            cboCompany.Text = UserCompany

            AddHandler radAbs.CheckedChanged, AddressOf radAbs_CheckedChanged
            AddHandler cboEATeam.SelectedValueChanged, AddressOf cboEATeam_SelectedValueChanged

            AddSelectionHandlers()

            SetcmdRefreshTip()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub
#End Region

#Region "Refresh"

    Private Sub RefreshGrid()
        Try
            ParamList = cboCompany.SelectedValue.ToString & ","
            ParamList &= GetParam(dgvEAName, "EAName") & ","
            ParamList &= cboEATeam.SelectedIndex & ","
            ParamList &= GetParam(dgvCGA, "CGA") & ","
            ParamList &= GetParam(dgvVisitedNotLive, "VisitedNotLive") & ","
            ParamList &= GetParam(dgvArrangementMade, "ArrangementMade") & ","
            ParamList &= GetParam(dgvFeeAdded, "FeeAdded") & ","
            ParamList &= GetParam(dgvCollected, "Collected") & ","
            ParamList &= GetParam(dgvNOI, "NOI") & ","
            ParamList &= GetParam(dgvVisited, "Visited") & ","
            ParamList &= GetParam(dgvWorkType, "WorkType") & ","
            ParamList &= GetParam(dgvAuditCheck, "AuditCheck") & ","
            ParamList &= GetParam(dgvGoneAway, "GoneAway") & ","
            ParamList &= GetParam(dgvVisitAfterPIF, "VisitAfterPIF") & ","
            ParamList &= GetParam(dgvAuditCheckBy, "AuditCheckBy") & ","
            ParamList &= GetParam(dgvClientName, "ClientName") & ","
            ParamList &= GetParam(dgvNumberOfVisitsPA, "NumberOfVisitsPA")

            ParamList &= "," & cboPeriodType.SelectedValue.ToString

            PreRefresh(Me)

            RemoveSelectionHandlers()

            GetSelections()

            SummaryData.GetSummary(ParamList, radAbs.Checked)
            dgvSummary.DataSource = SummaryData.SummaryDataView

            FormatGridColumns(dgvSummary)

            ListData.GetEAUpdateList(ParamList & ",'Total'") ' Total added. Request ref 63536
            SetListGrids()
            FormatListColumns()

            SetSelections()

            AddSelectionHandlers()

            PostRefresh(Me)

            dgvSummary.ClearSelection()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub RefreshLists()
        Try

            ParamList = cboCompany.SelectedValue.ToString & ","

            If dgvSummary.SelectedCells.Count = 1 And dgvEAName.SelectedRows.Count > 1 Then
                ParamList &= "'" & dgvSummary.Rows(dgvSummary.SelectedCells(0).RowIndex).Cells("EAName").Value & "',"
            Else
                ParamList &= GetParam(dgvEAName, "EAName") & ","
            End If

            ParamList &= cboEATeam.SelectedIndex & ","

            If dgvSummary.SelectedCells.Count = 1 And dgvCGA.SelectedRows.Count > 1 Then
                ParamList &= "'" & dgvSummary.Rows(dgvSummary.SelectedCells(0).RowIndex).Cells("CGA").Value & "',"
            Else
                ParamList &= GetParam(dgvCGA, "CGA") & ","
            End If

            If dgvSummary.SelectedCells.Count = 1 And dgvVisitedNotLive.SelectedRows.Count > 1 Then
                ParamList &= "'" & dgvSummary.Rows(dgvSummary.SelectedCells(0).RowIndex).Cells("VisitedNotLive").Value & "',"
            Else
                ParamList &= GetParam(dgvVisitedNotLive, "VisitedNotLive") & ","
            End If

            If dgvSummary.SelectedCells.Count = 1 And dgvArrangementMade.SelectedRows.Count > 1 Then
                ParamList &= "'" & dgvSummary.Rows(dgvSummary.SelectedCells(0).RowIndex).Cells("ArrangementMade").Value & "',"
            Else
                ParamList &= GetParam(dgvArrangementMade, "ArrangementMade") & ","
            End If

            If dgvSummary.SelectedCells.Count = 1 And dgvFeeAdded.SelectedRows.Count > 1 Then
                ParamList &= "'" & dgvSummary.Rows(dgvSummary.SelectedCells(0).RowIndex).Cells("FeeAdded").Value & "',"
            Else
                ParamList &= GetParam(dgvFeeAdded, "FeeAdded") & ","
            End If

            If dgvSummary.SelectedCells.Count = 1 And dgvCollected.SelectedRows.Count > 1 Then
                ParamList &= "'" & dgvSummary.Rows(dgvSummary.SelectedCells(0).RowIndex).Cells("Collected").Value & "',"
            Else
                ParamList &= GetParam(dgvCollected, "Collected") & ","
            End If

            If dgvSummary.SelectedCells.Count = 1 And dgvNOI.SelectedRows.Count > 1 Then
                ParamList &= "'" & dgvSummary.Rows(dgvSummary.SelectedCells(0).RowIndex).Cells("NOI").Value & "',"
            Else
                ParamList &= GetParam(dgvNOI, "NOI") & ","
            End If

            If dgvSummary.SelectedCells.Count = 1 And dgvVisited.SelectedRows.Count > 1 Then
                ParamList &= "'" & dgvSummary.Rows(dgvSummary.SelectedCells(0).RowIndex).Cells("Visited").Value & "',"
            Else
                ParamList &= GetParam(dgvVisited, "Visited") & ","
            End If

            If dgvSummary.SelectedCells.Count = 1 And dgvWorkType.SelectedRows.Count > 1 Then
                ParamList &= "'" & dgvSummary.Rows(dgvSummary.SelectedCells(0).RowIndex).Cells("WorkType").Value & "',"
            Else
                ParamList &= GetParam(dgvWorkType, "WorkType") & ","
            End If

            If dgvSummary.SelectedCells.Count = 1 And dgvAuditCheck.SelectedRows.Count > 1 Then
                ParamList &= "'" & dgvSummary.Rows(dgvSummary.SelectedCells(0).RowIndex).Cells("AuditCheck").Value & "',"
            Else
                ParamList &= GetParam(dgvAuditCheck, "AuditCheck") & ","
            End If

            If dgvSummary.SelectedCells.Count = 1 And dgvGoneAway.SelectedRows.Count > 1 Then
                ParamList &= "'" & dgvSummary.Rows(dgvSummary.SelectedCells(0).RowIndex).Cells("GoneAway").Value & "',"
            Else
                ParamList &= GetParam(dgvGoneAway, "GoneAway") & ","
            End If

            If dgvSummary.SelectedCells.Count = 1 And dgvVisitAfterPIF.SelectedRows.Count > 1 Then
                ParamList &= "'" & dgvSummary.Rows(dgvSummary.SelectedCells(0).RowIndex).Cells("VisitAfterPIF").Value & "',"
            Else
                ParamList &= GetParam(dgvVisitAfterPIF, "VisitAfterPIF") & ","
            End If

            If dgvSummary.SelectedCells.Count = 1 And dgvAuditCheckBy.SelectedRows.Count > 1 Then
                ParamList &= "'" & dgvSummary.Rows(dgvSummary.SelectedCells(0).RowIndex).Cells("AuditCheckBy").Value & "',"
            Else
                ParamList &= GetParam(dgvAuditCheckBy, "AuditCheckBy") & ","
            End If

            If dgvSummary.SelectedCells.Count = 1 And dgvClientName.SelectedRows.Count > 1 Then
                ParamList &= "'" & dgvSummary.Rows(dgvSummary.SelectedCells(0).RowIndex).Cells("ClientName").Value & "',"
            Else
                ParamList &= GetParam(dgvClientName, "ClientName") & ","
            End If

            If dgvSummary.SelectedCells.Count = 1 And dgvNumberOfVisitsPA.SelectedRows.Count > 1 Then
                ParamList &= "'" & dgvSummary.Rows(dgvSummary.SelectedCells(0).RowIndex).Cells("NumberOfVisitsPA").Value & "'"
            Else
                ParamList &= GetParam(dgvNumberOfVisitsPA, "NumberOfVisitsPA")
            End If

            ParamList &= "," & cboPeriodType.SelectedValue.ToString

            PreRefresh(Me)

            RemoveSelectionHandlers()

            GetSelections()

            'SummaryData.GetSummary(ParamList, radAbs.Checked)
            'dgvSummary.DataSource = SummaryData.SummaryDataView

            'FormatGridColumns(dgvSummary)

            ' add the period to the param list if only one cell in dgvSummary is selected and it is a data column. Request ref 63536
            If dgvSummary.SelectedCells.Count = 1 AndAlso DataColumnsList.Contains(dgvSummary.Columns(dgvSummary.SelectedCells(0).ColumnIndex).Name) Then
                ParamList &= "," & dgvSummary.Columns(dgvSummary.SelectedCells(0).ColumnIndex).Name
            Else
                ParamList &= ",'Total'"
            End If

            ListData.GetEAUpdateList(ParamList)
            SetListGrids()
            FormatListColumns()

            SetSelections()

            'dgvSummary.ClearSelection() ' The first cell always get selected moved 63536

            AddSelectionHandlers()

            PostRefresh(Me)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Function GetParam(ByRef DataGrid As DataGridView, ByVal ColumnName As String) As String
        GetParam = Nothing

        Try
            ' Used when selection criteria change
            Dim Param As String = Nothing

            For Each dr As DataGridViewRow In DataGrid.SelectedRows
                If Not IsNothing(Param) Then Param += vbTab
                Param += dr.Cells(ColumnName).Value.ToString
            Next dr

            If IsNothing(Param) Then
                Param = "null"
            Else
                Param = "'" & Param.Replace("'", "''''") & "'" ' Portal task ref 16716
            End If

            Return Param

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Function

    Private Function GetParam(ByVal ParamColumn As String, ByRef Cell As DataGridViewCell, ByRef ListDataGridView As DataGridView)
        GetParam = Nothing

        Try
            ' Used when detail for a particular cell in the summary grid is retrieved
            Dim Param As String = ""

            If IsNothing(Cell.DataGridView.Columns(ParamColumn)) Then
                ' The column may not be present as only one criteria is applicable
                If ListDataGridView.SelectedRows.Count = 1 Then
                    ' Param = "'" & ListDataGridView.SelectedRows(0).Cells(0).Value & "'"
                    Param = "'" & CStr(ListDataGridView.SelectedRows(0).Cells(0).Value).Replace("'", "''''") & "'" ' Portal task ref 16716
                Else
                    Param = "null"
                End If
            Else
                Param = "'" & CStr(Cell.DataGridView.Item(ParamColumn, Cell.RowIndex).Value).Replace("'", "''''") & "'" ' Portal task ref 16716
            End If

            Return Param

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Function
#End Region

#Region "Grid selection events"

    Private Sub cboCompany_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) ' Handles cboCompany.SelectedValueChanged
        Try
            For Each Control As Control In Me.Controls
                If TypeOf Control Is DataGridView Then
                    CType(Control, DataGridView).FirstDisplayedScrollingRowIndex = 0
                    CType(Control, DataGridView).ClearSelection()
                End If
            Next Control

            RefreshGrid()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cboPeriodType_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) ' Handles cboPeriodType.SelectedValueChanged
        Try
            RefreshGrid()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvSelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            RefreshGrid()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvMouseEnter(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            ' This is needed to make mousewheel scroll list items
            sender.Select()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvMouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        Try
            Dim hti As DataGridView.HitTestInfo = sender.HitTest(e.X, e.Y)

            If hti.Type = DataGridViewHitTestType.ColumnHeader Then
                GetSelections()
                RemoveSelectionHandlers()
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvMouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        Try
            Dim hti As DataGridView.HitTestInfo = sender.HitTest(e.X, e.Y)

            If hti.Type = DataGridViewHitTestType.ColumnHeader Then
                SetSelections()
                AddSelectionHandlers()
            End If

            If e.Button = MouseButtons.Right Then
                cmsList.Show(sender, e.Location)
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvSummary_DataBindingComplete(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewBindingCompleteEventArgs) Handles dgvSummary.DataBindingComplete
        Try
            ' This needs to be handled differently. I'm not sure why but doing this later causes events to fire. Recommended practice on the following url
            ' http://msdn.microsoft.com/en-us/library/system.windows.forms.datagridviewcolumn.visible.aspx

            If dgvSummary.Columns.Contains("RowID") Then dgvSummary.Columns("RowID").Visible = False

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvSummary_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgvSummary.MouseDown
        Try
            Dim hti As DataGridView.HitTestInfo = sender.HitTest(e.X, e.Y)

            If e.Button = Windows.Forms.MouseButtons.Right AndAlso hti.Type = DataGridViewHitTestType.Cell AndAlso Not dgvSummary.Item(hti.ColumnIndex, hti.RowIndex).Selected Then
                dgvSummary.Item(hti.ColumnIndex, hti.RowIndex).Selected = True
            End If

            If hti.Type = DataGridViewHitTestType.ColumnHeader Then
                ColSort = SummaryData.SummaryDataView.Sort
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvSummary_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgvSummary.MouseUp
        Try
            If e.Button = MouseButtons.Right Then cmsSummary.Show(dgvSummary, e.Location)

            Dim hti As DataGridView.HitTestInfo = sender.HitTest(e.X, e.Y)

            If hti.Type = DataGridViewHitTestType.ColumnHeader Then
                If ColSort <> "" Then SummaryData.SummaryDataView.Sort += "," & ColSort
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvSummary_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) ' Handles dgvSummary.SelectionChanged
        Try
            Dim TotalCases As Integer = 0

            If dgvSummary.SelectedCells.Count = 0 Or radAbs.Checked = False Then
                lblSummary.Text = ""
            Else
                For Each Cell As DataGridViewCell In dgvSummary.SelectedCells
                    If DataColumnsList.Contains(dgvSummary.Columns(Cell.ColumnIndex).Name) Then TotalCases += Cell.Value
                Next Cell
                lblSummary.Text = TotalCases.ToString & " case" & If(TotalCases > 1, "s", "")
            End If

            RefreshLists() ' Request ref 63536

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub ShowDetail()
        Try
            Dim DetailParamList As String
            Dim Detail As New diaCaseDetail(Me)

            PreRefresh(Me)

            For Each Cell As DataGridViewCell In dgvSummary.SelectedCells
                If DataColumnsList.Contains(dgvSummary.Columns(Cell.ColumnIndex).Name) Then
                    DetailParamList = cboCompany.SelectedValue.ToString & ","
                    DetailParamList &= GetParam("EAName", Cell, dgvEAName) & ","
                    DetailParamList &= cboEATeam.SelectedIndex & ","
                    DetailParamList &= GetParam("CGA", Cell, dgvCGA) & ","
                    DetailParamList &= GetParam("VisitedNotLive", Cell, dgvVisitedNotLive) & ","
                    DetailParamList &= GetParam("ArrangementMade", Cell, dgvArrangementMade) & ","
                    DetailParamList &= GetParam("FeeAdded", Cell, dgvFeeAdded) & ","
                    DetailParamList &= GetParam("Collected", Cell, dgvCollected) & ","
                    DetailParamList &= GetParam("NOI", Cell, dgvNOI) & ","
                    DetailParamList &= GetParam("Visited", Cell, dgvVisited) & ","
                    DetailParamList &= GetParam("WorkType", Cell, dgvWorkType) & ","
                    DetailParamList &= GetParam("AuditCheck", Cell, dgvAuditCheck) & ","
                    DetailParamList &= GetParam("GoneAway", Cell, dgvGoneAway) & ","
                    DetailParamList &= GetParam("VisitAfterPIF", Cell, dgvVisitAfterPIF) & ","
                    DetailParamList &= GetParam("AuditCheckBy", Cell, dgvAuditCheckBy) & ","
                    DetailParamList &= GetParam("ClientName", Cell, dgvClientName) & ","
                    DetailParamList &= GetParam("NumberOfVisitsPA", Cell, dgvNumberOfVisitsPA)

                    DetailParamList &= "," & cboPeriodType.SelectedValue.ToString

                    Detail.AddCases(DetailParamList, dgvSummary.Columns(Cell.ColumnIndex).Name)

                End If
            Next Cell

            PostRefresh(Me)

            If Detail.CaseCount > 0 Then Detail.ShowDialog() ' Portal task ref 16716
            Detail.Dispose()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub


#End Region

#Region "'All' and 'Clear' button click events"

    Private Sub cmdBailiffNameAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdEANameAll.Click
        Try
            dgvEAName.SelectAll()
            EANameGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdBailiffNameClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdEANameClear.Click
        Try
            dgvEAName.ClearSelection()
            EANameGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdCGAAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCGAAll.Click
        Try
            dgvCGA.SelectAll()
            CGAGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdCGAClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCGAClear.Click
        Try
            dgvCGA.ClearSelection()
            CGAGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdVisitedNotLiveAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdVisitedNotLiveAll.Click
        Try
            dgvVisitedNotLive.SelectAll()
            VisitedNotLiveGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdVisitedNotLiveClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdVisitedNotLiveClear.Click
        Try
            dgvVisitedNotLive.ClearSelection()
            VisitedNotLiveGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdArrangementMadeAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdArrangementMadeAll.Click
        Try
            dgvArrangementMade.SelectAll()
            ArrangementMadeGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdArrangementMadeClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdArrangementMadeClear.Click
        Try
            dgvArrangementMade.ClearSelection()
            ArrangementMadeGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdFeeAddedAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdFeeAddedAll.Click
        Try
            dgvFeeAdded.SelectAll()
            FeeAddedGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdFeeAddedClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdFeeAddedClear.Click
        Try
            dgvFeeAdded.ClearSelection()
            FeeAddedGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub


    Private Sub cmdCollectedAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCollectedAll.Click
        Try
            dgvCollected.SelectAll()
            CollectedGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdCollectedClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCollectedClear.Click
        Try
            dgvCollected.ClearSelection()
            CollectedGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdNOIAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdNOIAll.Click
        Try
            dgvNOI.SelectAll()
            NOIGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdNOIClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdNOIClear.Click
        Try
            dgvNOI.ClearSelection()
            NOIGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdVisitedAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdVisitedAll.Click
        Try
            dgvVisited.SelectAll()
            VisitedGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdVisitedClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdVisitedClear.Click
        Try
            dgvVisited.ClearSelection()
            VisitedGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdWorkTypeAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdWorkTypeAll.Click
        Try
            dgvWorkType.SelectAll()
            WorkTypeGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdWorkTypeClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdWorkTypeClear.Click
        Try
            dgvWorkType.ClearSelection()
            WorkTypeGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdAuditCheckAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAuditCheckAll.Click
        Try
            dgvAuditCheck.SelectAll()
            AuditCheckGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdAuditCheckClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAuditCheckClear.Click
        Try
            dgvAuditCheck.ClearSelection()
            AuditCheckGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdGoneAwayAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdGoneAwayAll.Click
        Try
            dgvGoneAway.SelectAll()
            GoneAwayGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdGoneAwayClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdGoneAwayClear.Click
        Try
            dgvGoneAway.ClearSelection()
            GoneAwayGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub


    Private Sub cmdVisitAfterPIFAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdVisitAfterPIFAll.Click
        Try
            dgvVisitAfterPIF.SelectAll()
            VisitAfterPIFGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdVisitAfterPIFClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdVisitAfterPIFClear.Click
        Try
            dgvVisitAfterPIF.ClearSelection()
            VisitAfterPIFGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdAuditCheckByAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAuditCheckByAll.Click
        Try
            dgvAuditCheckBy.SelectAll()
            AuditCheckByGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdAuditCheckByClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAuditCheckByClear.Click
        Try
            dgvAuditCheckBy.ClearSelection()
            AuditCheckByGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdClientNameAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdClientNameAll.Click
        Try
            dgvClientName.SelectAll()
            ClientNameGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdClientNameClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdClientNameClear.Click
        Try
            dgvClientName.ClearSelection()
            ClientNameGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdNumberOfVisitsPAAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdNumberOfVisitsPAAll.Click
        Try
            dgvNumberOfVisitsPA.SelectAll()
            NumberOfVisitsPAGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdNumberOfVisitsPAClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdNumberOfVisitsPAClear.Click
        Try
            dgvNumberOfVisitsPA.ClearSelection()
            NumberOfVisitsPAGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

#End Region

#Region "Handlers"

    Private Sub RemoveSelectionHandlers()
        Try
            ' Need to find a way to do these at once but these SelectionChanged event is specific to the datagridview class and not control - the collection on the form
            RemoveHandler dgvEAName.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvCGA.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvVisitedNotLive.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvArrangementMade.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvFeeAdded.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvCollected.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvNOI.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvVisited.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvWorkType.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvAuditCheck.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvGoneAway.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvVisitAfterPIF.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvAuditCheckBy.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvClientName.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvNumberOfVisitsPA.SelectionChanged, AddressOf dgvSelectionChanged

            RemoveHandler dgvSummary.SelectionChanged, AddressOf dgvSummary_SelectionChanged ' Request ref 63536

            RemoveHandler cboCompany.SelectedValueChanged, AddressOf cboCompany_SelectedValueChanged
            RemoveHandler cboPeriodType.SelectedValueChanged, AddressOf cboPeriodType_SelectedValueChanged

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub AddSelectionHandlers()
        Try
            AddHandler dgvEAName.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvCGA.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvVisitedNotLive.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvArrangementMade.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvFeeAdded.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvCollected.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvNOI.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvVisited.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvWorkType.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvAuditCheck.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvGoneAway.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvVisitAfterPIF.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvAuditCheckBy.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvClientName.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvNumberOfVisitsPA.SelectionChanged, AddressOf dgvSelectionChanged

            AddHandler dgvSummary.SelectionChanged, AddressOf dgvSummary_SelectionChanged ' Request ref 63536

            AddHandler cboCompany.SelectedValueChanged, AddressOf cboCompany_SelectedValueChanged
            AddHandler cboPeriodType.SelectedValueChanged, AddressOf cboPeriodType_SelectedValueChanged

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub AddControlHandlers()
        Try
            ' All these grids and buttons call the same events so no point declaring them all separately
            For Each Control As Control In Me.Controls
                If TypeOf Control Is DataGridView Then
                    AddHandler Control.MouseDown, AddressOf dgvMouseDown
                    AddHandler Control.MouseUp, AddressOf dgvMouseUp
                    AddHandler Control.MouseEnter, AddressOf dgvMouseEnter
                    'AddHandler Control.MouseHover, AddressOf dgvMouseHover
                ElseIf TypeOf Control Is ComboBox Or TypeOf Control Is Button Or TypeOf Control Is RadioButton Or TypeOf Control Is CheckBox Then
                    'AddHandler Control.MouseHover, AddressOf dgvMouseHover
                End If
            Next Control

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

#End Region

#Region "DataGridView methods"

    Private Sub SetListGrids()
        Try
            dgvEAName.DataSource = ListData.BailiffName
            EANameGridState.SetSort()

            dgvCGA.DataSource = ListData.CGA
            CGAGridState.SetSort()

            dgvVisitedNotLive.DataSource = ListData.VisitedNotLive
            VisitedNotLiveGridState.SetSort()

            dgvArrangementMade.DataSource = ListData.ArrangementMade
            ArrangementMadeGridState.SetSort()

            dgvFeeAdded.DataSource = ListData.FeeAdded
            FeeAddedGridState.SetSort()

            dgvCollected.DataSource = ListData.Collected
            CollectedGridState.SetSort()

            dgvNOI.DataSource = ListData.NOI
            NOIGridState.SetSort()

            dgvVisited.DataSource = ListData.Visited
            VisitedGridState.SetSort()

            dgvWorkType.DataSource = ListData.WorkType
            WorkTypeGridState.SetSort()

            dgvAuditCheck.DataSource = ListData.AuditCheck
            AuditCheckGridState.SetSort()

            dgvGoneAway.DataSource = ListData.GoneAway
            GoneAwayGridState.SetSort()

            dgvVisitAfterPIF.DataSource = ListData.VisitAfterPIF
            VisitAfterPIFGridState.SetSort()

            dgvAuditCheckBy.DataSource = ListData.AuditCheckBy
            AuditCheckByGridState.SetSort()

            dgvClientName.DataSource = ListData.ClientName
            ClientNameGridState.SetSort()

            dgvNumberOfVisitsPA.DataSource = ListData.NumberOfVisitsPA
            NumberOfVisitsPAGridState.SetSort()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub FormatListColumns()
        Try
            ' This is slightly different to settings in modCommon as we want some of the fields wider than they need to be for presentation
            For Each Control As Control In Me.Controls
                If TypeOf Control Is DataGridView Then
                    If Control.Name <> "dgvSummary" Then ' This is not a list grid
                        For Each Column In CType(Control, DataGridView).Columns
                            Select Case Column.Name
                                Case "Total"
                                    Column.Width = 40
                                Case "EAName"
                                    Column.Width = 100
                                    Column.HeaderText = "Agent"
                                Case "CGA"
                                    Column.Width = 70
                                Case "ArrangementMade"
                                    Column.Width = 70
                                    Column.HeaderText = "Arrangement"
                                Case "FeeAdded"
                                    Column.Width = 70
                                    Column.HeaderText = "Fee"
                                Case "Collected"
                                    Column.Width = 70
                                Case "VisitedNotLive"
                                    Column.Width = 70
                                    Column.HeaderText = "Not live"
                                Case "NOI"
                                    Column.Width = 70
                                    Column.HeaderText = "NOI"
                                Case "Visited"
                                    Column.Width = 70
                                    Column.HeaderText = "Visited"
                                Case "WorkType"
                                    Column.Width = 100
                                    Column.HeaderText = "Work Type"
                                Case "AuditCheck"
                                    Column.Width = 70
                                    Column.HeaderText = "Audit"
                                Case "GoneAway"
                                    Column.Width = 70
                                    Column.HeaderText = "Gone away"
                                Case "VisitAfterPIF"
                                    Column.Width = 70
                                    Column.HeaderText = "V after PIF"
                                Case "AuditCheckBy"
                                    Column.Width = 100
                                    Column.HeaderText = "User"
                                Case "ClientName"
                                    Column.Width = 100
                                    Column.HeaderText = "Client"
                                Case "NumberOfVisitsPA"
                                    Column.width = 60
                                    Column.HeaderText = "# Visits"
                                Case Else
                                    Column.Width = 40
                            End Select
                        Next Column
                    End If
                End If
            Next Control

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub


    Private Sub GetSelections()
        Try
            EANameGridState.GetState()
            CGAGridState.GetState()
            VisitedNotLiveGridState.GetState()
            ArrangementMadeGridState.GetState()
            FeeAddedGridState.GetState()
            CollectedGridState.GetState()
            NOIGridState.GetState()
            VisitedGridState.GetState()
            WorkTypeGridState.GetState()
            AuditCheckGridState.GetState()
            GoneAwayGridState.GetState()
            VisitAfterPIFGridState.GetState()
            AuditCheckByGridState.GetState()
            ClientNameGridState.GetState()
            NumberOfVisitsPAGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub SetSelections()
        Try
            EANameGridState.SetState()
            CGAGridState.SetState()
            VisitedNotLiveGridState.SetState()
            ArrangementMadeGridState.SetState()
            FeeAddedGridState.SetState()
            CollectedGridState.SetState()
            NOIGridState.SetState()
            VisitedGridState.SetState()
            WorkTypeGridState.SetState()
            AuditCheckGridState.SetState()
            GoneAwayGridState.SetState()
            VisitAfterPIFGridState.SetState()
            AuditCheckByGridState.SetState()
            ClientNameGridState.SetState()
            NumberOfVisitsPAGridState.SetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

#End Region

#Region "Additional functionality"

    Private Sub cmdClearAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdClearAll.Click
        Try
            RemoveSelectionHandlers()

            cmdbailiffnameClear_Click(sender, New System.EventArgs)
            cmdCGAClear_Click(sender, New System.EventArgs)
            cmdVisitedNotLiveClear_Click(sender, New System.EventArgs)
            cmdArrangementMadeClear_Click(sender, New System.EventArgs)
            cmdFeeAddedClear_Click(sender, New System.EventArgs)
            cmdCollectedClear_Click(sender, New System.EventArgs)
            cmdNOIClear_Click(sender, New System.EventArgs)
            cmdVisitedClear_Click(sender, New System.EventArgs)
            cmdWorkTypeClear_Click(sender, New System.EventArgs)
            cmdAuditCheckClear_Click(sender, New System.EventArgs)
            cmdGoneAwayClear_Click(sender, New System.EventArgs)
            cmdVisitAfterPIFClear_Click(sender, New System.EventArgs)
            cmdAuditCheckByClear_Click(sender, New System.EventArgs)
            cmdClientNameClear_Click(sender, New System.EventArgs)
            cmdNumberOfVisitsPAClear_Click(sender, New System.EventArgs)

            dgvSummary.ClearSelection() ' Request ref 63536

            AddSelectionHandlers()

            RefreshGrid()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub radAbs_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) 'Handles radAbs.CheckedChanged
        Try
            RefreshGrid()
            If radAbs.Checked Then dgvSummary.DefaultCellStyle.Format = "" Else dgvSummary.DefaultCellStyle.Format = "N2"

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub radPerc_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radPerc.CheckedChanged
        Try
            RefreshGrid()
            If radAbs.Checked Then dgvSummary.DefaultCellStyle.Format = "" Else dgvSummary.DefaultCellStyle.Format = "N2"

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdRefreshDB_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdRefreshDB.Click
        Try
            If MsgBox("This could take several minutes, are you sure?", vbYesNo + vbDefaultButton2 + vbQuestion) = vbYes Then

                PreRefresh(Me)
                SummaryData.RefreshDatabase()
                RefreshGrid()
                PostRefresh(Me)
                MsgBox("Refresh complete.", vbOKOnly + vbInformation)
                SetcmdRefreshTip()
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdRefreshDB_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdRefreshDB.MouseLeave
        Try
            ' on XP machines there is a bug that prevents the tooltip showing a second time. This addresses the problem.
            ToolTip1.Active = False
            ToolTip1.Active = True

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub SetcmdRefreshTip()
        Try
            SummaryData.GetLastLoad()
            ToolTip1.SetToolTip(cmdRefreshDB, "Last refreshed at " & SummaryData.LastLoadDataView.Item(0).Item(0).ToString & " by " & SummaryData.LastLoadDataView.Item(0).Item(1).ToString)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmsSummary_ItemClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles cmsSummary.ItemClicked
        Try
            Select Case e.ClickedItem.Text
                Case "View Cases"
                    ShowDetail()
                Case "Copy"
                    dgvSummary.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText
                    Clipboard.SetDataObject(dgvSummary.GetClipboardContent())
                Case "Select All"
                    dgvSummary.SelectAll()
            End Select

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmsList_ItemClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles cmsList.ItemClicked
        Try
            ' Slightly more complicated than the summary grid as this event fires for all other datagridviews.
            Select Case e.ClickedItem.Text
                Case "Copy"
                    sender.SourceControl.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText
                    Clipboard.SetDataObject(sender.SourceControl.GetClipboardContent())

                Case "Select All"
                    sender.SourceControl.SelectAll()

            End Select

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmsForm_ItemClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles cmsForm.ItemClicked
        Try
            Select Case e.ClickedItem.Text
                Case "Allocation View"
                    If FormOpen("frmCaseSummary") Then
                        frmCaseSummary.Activate()
                    Else
                        frmCaseSummary.ShowDialog()
                    End If
                Case "Bailiff View"
                    If FormOpen("frmBailiffSummary") Then
                        frmBailiffSummary.Activate()
                    Else
                        frmBailiffSummary.ShowDialog()
                    End If
                Case "Post Enforcement View"
                    If FormOpen("frmPostEnforcementSummary") Then
                        frmPostEnforcementSummary.Activate()
                    Else
                        frmPostEnforcementSummary.ShowDialog()
                    End If
            End Select

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cboEATeam_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) ' Handles cboEATeam.SelectedValueChanged
        Try
            RefreshScroll = False

            ' Not ideal but we need to refresh the bailiff list before the main refresh to exclude any bailiffs selected that would not be in the top list
            RemoveSelectionHandlers()

            GetSelections()
            ListData.GetEANameList(ParamList)
            SetSelections()

            AddSelectionHandlers()

            RefreshGrid()

            RefreshScroll = True

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub
#End Region

End Class


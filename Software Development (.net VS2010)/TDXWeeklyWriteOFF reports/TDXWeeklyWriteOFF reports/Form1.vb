Public Class Form1
    Dim outfile As String
    Public ascii As New System.Text.ASCIIEncoding()
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
       
        Dim totalBalance As Decimal
        Dim caseNo As Integer
        Dim user As String = My.User.Name
        'get client number
        Dim clID As Integer
        Select Case client_cbox.Text
            Case "Anglia Water"
                clID = 1766
            Case "BT"
                clID = 1523
            Case "HMRC"
                clID = 1736
        End Select

        'get all client schemes on branch 2
        param2 = "select _rowid from clientScheme where clientID = " & clID & _
            " and branchID = 2"
        Dim cs_ds As DataSet = get_dataset("onestep", param2)
        Dim csRows As Integer = no_of_rows - 1
        For csIDX = 0 To csRows
            Dim csID As Integer = cs_ds.Tables(0).Rows(csIDX).Item(0)
            'get all remits for period
            param2 = "select _rowid, date from remit where clientSchemeID = " & csID & _
                     " order by _rowid"
            Dim remit_ds As DataSet = get_dataset("onestep", param2)
            Dim remitRows As Integer = no_of_rows - 1
            For remitIDX = 0 To remitRows
                Dim remitDate As Date = remit_ds.Tables(0).Rows(remitIDX).Item(1)
                If Format(remitDate, "yyyy-MM-dd") > Format(end_dtp.Value, "yyyy-MM-dd") Then
                    Continue For
                End If
                If Format(remitDate, "yyyy-MM-dd") < Format(start_dtp.Value, "yyyy-MM-dd") Then
                    Continue For
                End If
                Dim remitID As Integer = remit_ds.Tables(0).Rows(remitIDX).Item(0)
                'get all returned cases for remits
                param2 = "select offenceLocation, client_ref, offence_number, offenceCourtName, clientSchemeReturnID, _rowid, status" & _
                    " from debtor where clientSchemeID = " & csID & _
                    " and (status = 'C' or status = 'X' or status = 'S')" & _
                    " and return_remitID = " & remitID
                Dim debt_ds As DataSet = get_dataset("onestep", param2)
                'save details in outfile
                Dim debtRows As Integer = no_of_rows - 1
                For debtIDX = 0 To debtRows
                    Try
                        ProgressBar.Value = (debtIDX / debtRows) * 100
                    Catch ex As Exception

                    End Try
                    Application.DoEvents()
                    Dim DebtorID As Integer = debt_ds.Tables(0).Rows(debtIDX).Item(5)
                    'exclude cases on TDXWriteOffs table
                    param2 = "select tdx_listID from TDXWriteOffs where tdx_debtorID = " & DebtorID
                    Dim tdx_ds As DataSet = get_dataset("Fees", param2)
                    If no_of_rows > 0 Then
                        Continue For
                    End If
                    caseNo += 1
                    Dim offenceLocation As String = debt_ds.Tables(0).Rows(debtIDX).Item(0)
                    Dim clientRef As String = debt_ds.Tables(0).Rows(debtIDX).Item(1)

                    Dim offenceNumber As String = ""
                    Try
                        offenceNumber = debt_ds.Tables(0).Rows(debtIDX).Item(2)
                    Catch ex As Exception

                    End Try
                    Dim offenceCourtname As String = debt_ds.Tables(0).Rows(debtIDX).Item(3)
                    Dim status As String = debt_ds.Tables(0).Rows(debtIDX).Item(6)
                    Dim clientSchemeReturnID As Integer = 0
                    If status <> "S" Then
                        Try
                            clientSchemeReturnID = debt_ds.Tables(0).Rows(debtIDX).Item(4)
                        Catch ex As Exception

                        End Try

                    End If
                    'get brand from notes
                    param2 = "select text from Note where debtorID = " & DebtorID & _
                        " and text like 'Client Brand: %'" & _
                        " order by _rowid desc"
                    Dim note_ds As DataSet = get_dataset("onestep", param2)
                    Dim brand As String = ""
                    If no_of_rows > 0 Then
                        brand = note_ds.Tables(0).Rows(0).Item(0)
                        brand = Microsoft.VisualBasic.Right(brand, brand.Length - 13)
                        'remove % at end
                        brand = Trim(Microsoft.VisualBasic.Left(brand, brand.Length - 1))
                    End If
                    'get client balance
                    Dim clientBalance As Decimal = 0
                    param2 = "select sum(fee_amount), sum(remited_fee) from fee where debtorID = " & DebtorID &
                        " and fee_remit_col < 3"
                    Dim fee_ds As DataSet = get_dataset("onestep", param2)
                    clientBalance = fee_ds.Tables(0).Rows(0).Item(0) - fee_ds.Tables(0).Rows(0).Item(1)
                    totalBalance += clientBalance
                    'get assignment from notes
                    param2 = "select text from Note where debtorID = " & DebtorID & _
                        " and text like 'Assignment ID: %'" & _
                        " order by _rowid desc"
                    Dim note2_ds As DataSet = get_dataset("onestep", param2)
                    Dim assignment As String = ""
                    If no_of_rows > 0 Then
                        assignment = note2_ds.Tables(0).Rows(0).Item(0)
                        assignment = Microsoft.VisualBasic.Right(assignment, assignment.Length - 15)
                        'remove % at end
                        assignment = Trim(Microsoft.VisualBasic.Left(assignment, assignment.Length - 1))
                    End If
                    'get client return code
                    Dim clientReturnCode As String = ""

                    If status = "S" Then
                        clientReturnCode = "PIF"
                    Else
                        param2 = "select clientReturnCode from clientSchemeReturn where _rowid = " & clientSchemeReturnID
                        Dim cs_ret_ds As DataSet = get_dataset("onestep", param2)
                        Try
                            clientReturnCode = cs_ret_ds.Tables(0).Rows(0).Item(0)
                        Catch ex As Exception

                        End Try

                    End If
                    outfile &= Chr(34) & offenceLocation & Chr(34) & "," &
                    Chr(34) & clientRef & Chr(34) & "," &
                    Chr(34) & offenceNumber & Chr(34) & "," &
                    Chr(34) & offenceCourtname & Chr(34) & "," &
                    Chr(34) & brand & Chr(34) & "," &
                    clientBalance & "," &
                    Format(Now, "dd/MM/yyyy") & "," &
                    Chr(34) & assignment & Chr(34) & "," &
                    Chr(34) & clientReturnCode & Chr(34) & vbNewLine
                Next
            Next
        Next

        'add header to outfile
        outfile = Chr(34) & "WriteOff            " & Chr(34) & "," & Format(Now, "dd/MM/yyyy") & "," & _
        Chr(34) & "RossendalesWriteoffs" & Format(Now, "yyyyMMdd") & Space(22) & Chr(34) & "," & _
        totalBalance & "," & caseNo & vbNewLine & outfile

        'add trailer
        outfile &= Chr(34) & "ROSSENDALES         " & Chr(34)

        With SaveFileDialog1
            .Title = "Save file"
            .Filter = "CSV files |*.csv"
            .DefaultExt = ".csv"
            .OverwritePrompt = True
            .FileName = "RossendalesWriteoffs" & Format(Now, "YYYYMMDD") & ".csv"
        End With
        Dim filename As String
        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            filename = SaveFileDialog1.FileName
            Application.DoEvents()
            MsgBox("Completed")
            My.Computer.FileSystem.WriteAllText(filename, outfile, False, ascii)
        End If
        Me.Close()
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        start_dtp.Value = DateAdd(DateInterval.Day, -Weekday(Now) - 5, Now)
        end_dtp.Value = DateAdd(DateInterval.Day, +6, start_dtp.Value)
        client_cbox.SelectedIndex = 0
    End Sub
End Class

Public Class clientsfrm
    Dim saved_email As String
    Dim cl_id As Integer

    Private Sub clientsfrm_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        cl_id = 0
    End Sub
    Private Sub clientsfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        DataGridView1.Rows.Clear()
        mainfrm.Bailiff_in_areaTableAdapter.Fill(mainfrm.FeesSQLDataSet.Bailiff_in_area)
        Dim cl_rows As Integer = mainfrm.FeesSQLDataSet.Bailiff_in_area.Rows.Count - 1
        Dim idx As Integer
        For idx = 0 To cl_rows
            mainfrm.ProgressBar1.Value = idx / cl_rows * 100
            Dim cl_id As Integer = mainfrm.FeesSQLDataSet.Bailiff_in_area.Rows(idx).Item(0)
            Dim email As String = mainfrm.FeesSQLDataSet.Bailiff_in_area.Rows(idx).Item(1)
            Dim last_date_str As String
            Try
                last_date_str = mainfrm.FeesSQLDataSet.Bailiff_in_area.Rows(idx).Item(2)
            Catch ex As Exception
                last_date_str = " "
            End Try
            If last_date_str > " " Then
                Try
                    last_date_str = Format(CDate(last_date_str), "yyyy-MM-dd")
                Catch
                    last_date_str = " "
                End Try
            End If
            param1 = "onestep"
            param2 = "select name from Client where _rowid = " & cl_id
            Dim cl_dataset As DataSet = get_dataset(param1, param2)
            If no_of_rows <> 1 Then
                MsgBox("Client name not found for client ID = " & cl_id)
                Continue For
            End If
            Dim cl_name As String = Trim(cl_dataset.Tables(0).Rows(0).Item(0).ToString)
            DataGridView1.Rows.Add(cl_id, cl_name, last_date_str, email)
        Next
        mainfrm.ProgressBar1.Value = 0
    End Sub

    Private Sub DataGridView1_CellValidating(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellValidatingEventArgs) Handles DataGridView1.CellValidating
        If e.ColumnIndex = 0 Then
            Try
                Dim new_row As Boolean = DataGridView1.Rows(e.RowIndex + 1).IsNewRow
            Catch ex As Exception
                Exit Sub
            End Try
            If DataGridView1.Rows(e.RowIndex + 1).IsNewRow Then
                If e.FormattedValue = 0 Then
                    Exit Sub
                End If
                param1 = "onestep"
                param2 = "select name from Client where _rowid = " & e.FormattedValue
                Dim cl_dataset As DataSet = get_dataset(param1, param2)
                If no_of_rows <> 1 Then
                    MsgBox("Client ID not found on onestep")
                    e.Cancel = True
                    Exit Sub
                End If
                DataGridView1.Rows(e.RowIndex).Cells(1).Value = Trim(cl_dataset.Tables(0).Rows(0).Item(0).ToString)
            Else
                If e.FormattedValue <> DataGridView1.Rows(e.RowIndex).Cells(e.ColumnIndex).Value Then
                    MsgBox("Can't update client number - change it back to " & DataGridView1.Rows(e.RowIndex).Cells(e.ColumnIndex).Value)
                    e.Cancel = True
                End If
            End If
        End If
        If e.ColumnIndex = 2 Then
            Dim last_date_str As String = e.FormattedValue.ToString
            If Microsoft.VisualBasic.Len(Trim(last_date_str)) = 0 Then
                Exit Sub
            End If
            If DataGridView1.Rows(e.RowIndex + 1).IsNewRow Then
                MsgBox("Leave date blank for new entry")
                e.Cancel = True
                Exit Sub
            End If

            Try
                Dim last_date As Date = CDate(last_date_str)
            Catch
                MsgBox("Invalid date")
                e.Cancel = True
            End Try
        End If
        If e.ColumnIndex = 3 Then
            If Microsoft.VisualBasic.Len(Trim(e.FormattedValue.ToString)) = 0 Then
                Exit Sub
            End If
            If InStr(e.FormattedValue.ToString, """") > 0 Then
                MsgBox("Email address must not contain quotes")
                e.Cancel = True
                Exit Sub
            End If
            If InStr(e.FormattedValue.ToString, ",") > 0 Then
                MsgBox("Email address must not contain a comma")
                e.Cancel = True
            End If
            If InStr(e.FormattedValue.ToString, "@") = 0 Then
                MsgBox("Email address must contain @")
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub DataGridView1_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellValueChanged
        If e.RowIndex < 0 Then
            Exit Sub
        End If
        cl_id = DataGridView1.Rows(e.RowIndex).Cells(0).Value
        Dim email As String = DataGridView1.Rows(e.RowIndex).Cells(3).Value
        If e.ColumnIndex = 2 Then
            Dim date_str As String = DataGridView1.Rows(e.RowIndex).Cells(2).Value
            If date_str.Length > 0 Then
                mainfrm.Bailiff_in_areaTableAdapter.UpdateQuery(CDate(date_str), email, cl_id)
            End If

        End If
        If e.ColumnIndex = 3 Then
            If saved_email = Nothing Then
                Try
                    mainfrm.Bailiff_in_areaTableAdapter.InsertQuery(cl_id, email)
                Catch ex As Exception
                    MsgBox(ex.Message)
                End Try
            Else
                Try
                    mainfrm.Bailiff_in_areaTableAdapter.UpdateQuery1(email, saved_email, cl_id)
                Catch ex As Exception
                    MsgBox(ex.Message)
                End Try
            End If
        End If
    End Sub

    Private Sub DataGridView1_RowEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.RowEnter
        cl_id = DataGridView1.Rows(e.RowIndex).Cells(0).Value
        saved_email = DataGridView1.Rows(e.RowIndex).Cells(3).Value
    End Sub

    Private Sub DataGridView1_RowsRemoved(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowsRemovedEventArgs) Handles DataGridView1.RowsRemoved
        If e.RowIndex >= 0 And cl_id > 0 Then
            Try
                mainfrm.Bailiff_in_areaTableAdapter.DeleteQuery(saved_email, cl_id)
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If

    End Sub

    Private Sub findbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles findbtn.Click
        first_letters = InputBox("Enter first part of client name", "Select client")
        first_letters = Trim(first_letters)
        first_letters = UCase(Microsoft.VisualBasic.Left(first_letters, 1)) & _
        Microsoft.VisualBasic.Right(first_letters, first_letters.Length - 1)
        If first_letters.Length = 0 Then
            MsgBox("NO Letters entered")
            Exit Sub
        End If
        clientfrm.ShowDialog()
    End Sub
End Class
﻿'---------------------------------------------------------------------
'  This file is part of the Microsoft .NET Framework SDK Code Samples.
' 
'  Copyright (C) Microsoft Corporation.  All rights reserved.
' 
'This source code is intended only as a supplement to Microsoft
'Development Tools and/or on-line documentation.  See these other
'materials for detailed information regarding Microsoft code samples.
' 
'THIS CODE AND INFORMATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY
'KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
'IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
'PARTICULAR PURPOSE.
'---------------------------------------------------------------------

Imports System.net.mail

Module Mailer
    Function email(ByVal toaddress, ByVal fromaddress, ByVal subject, ByVal body, ByVal file_name)
        Dim mailServerName As String = "192.168.103.241"
        log_message = "email about to go for " & cl_name & vbNewLine
        write_log()
        log_message = cl_name & " sent to " & toaddress
        error_message = cl_name & " due to be sent to := " & toaddress
        Try
            Using message As New MailMessage(fromaddress, toaddress, subject, body)
                Dim mailClient As New SmtpClient(mailServerName)
                mailClient.UseDefaultCredentials = True
                If cc_all > " " Then
                    message.CC.Add(cc_all)
                End If
                Dim att1 As New Attachment(file_name)
                message.Attachments.Add(att1)
                mailClient.Send(message)
            End Using
            log_message = log_message & vbNewLine
            write_log()
            Return (0)
        Catch ex As Exception
            error_message = error_message & vbNewLine & "not sent due to := " & ex.Message & vbNewLine
            write_error()
            Return (10)
        End Try
    End Function
End Module

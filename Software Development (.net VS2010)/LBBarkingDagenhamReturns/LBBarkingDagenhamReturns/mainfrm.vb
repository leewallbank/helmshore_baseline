Public Class mainfrm
    Dim first_letter As String
    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub mainfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim days As Integer = Weekday(Now)
        date_picker.Value = DateAdd(DateInterval.Day, -days, Now)
        selected_scheme_name = ""
        selected_cl_name = ""
    End Sub

    Private Sub retnbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles retnbtn.Click
        
        disable_buttons()
        ProgressBar1.Value = 5
        retn_month = Format(date_picker.Value, "MM_MMM")
        retn_day = Format(date_picker.Value, "dd_ddd")
        'get remittance number
        param2 = "select _rowid from Remit where clientschemeID = 4881" & _
        " and date = '" & Format(date_picker.Value, "yyyy-MM-dd") & "'"
        Dim remit_dataset As DataSet = get_dataset("onestep", param2)
        If no_of_rows = 0 Then
            MsgBox("There are no remits for this client on " & Format(date_picker.Value, "dd/MM/yyyy"))
            enable_buttons()
            Exit Sub
        End If
        Dim remit_no As Integer = remit_dataset.Tables(0).Rows(0).Item(0)
        Dim controlFile As String = ""
        file_path = "O:\DebtRecovery\Archives\" & Format(date_picker.Value, "yyyy") & "\" & retn_month & "\" & _
                                   retn_day & "\Remittances\" & Format(remit_no, "#") & "\Returns\"
        Dim file_name, client_ref As String
        'create directories for returns
        Dim dir_name As String = file_path & "CTBAIL"
        Try
            Dim di As IO.DirectoryInfo = New IO.DirectoryInfo(dir_name)
            If System.IO.Directory.Exists(dir_name) = False Then
                di = System.IO.Directory.CreateDirectory(dir_name)
            Else
                System.IO.Directory.Delete(dir_name, True)
                di = System.IO.Directory.CreateDirectory(dir_name)
            End If
        Catch ex As Exception
            MsgBox("Unable to create folder")
            End
        End Try
        dir_name = file_path & "CTBCLNT"
        Try
            Dim di As IO.DirectoryInfo = New IO.DirectoryInfo(dir_name)
            If System.IO.Directory.Exists(dir_name) = False Then
                di = System.IO.Directory.CreateDirectory(dir_name)
            Else
                System.IO.Directory.Delete(dir_name, True)
                di = System.IO.Directory.CreateDirectory(dir_name)
            End If
        Catch ex As Exception
            MsgBox("Unable to create folder")
            End
        End Try
        dir_name = file_path & "CTBNULL"
        Try
            Dim di As IO.DirectoryInfo = New IO.DirectoryInfo(dir_name)
            If System.IO.Directory.Exists(dir_name) = False Then
                di = System.IO.Directory.CreateDirectory(dir_name)
            Else
                System.IO.Directory.Delete(dir_name, True)
                di = System.IO.Directory.CreateDirectory(dir_name)
            End If
        Catch ex As Exception
            MsgBox("Unable to create folder")
            End
        End Try
        dir_name = file_path & "CTBGONE"
        Try
            Dim di As IO.DirectoryInfo = New IO.DirectoryInfo(dir_name)
            If System.IO.Directory.Exists(dir_name) = False Then
                di = System.IO.Directory.CreateDirectory(dir_name)
            Else
                System.IO.Directory.Delete(dir_name, True)
                di = System.IO.Directory.CreateDirectory(dir_name)
            End If
        Catch ex As Exception
            MsgBox("Unable to create folder")
            End
        End Try
        Dim debtor, retn_codeid As Integer
        Dim files_found As Boolean = False
        For Each foundFile As String In My.Computer.FileSystem.GetFiles _
        (file_path, FileIO.SearchOption.SearchAllSubDirectories, "*.pdf")
            'get debtor number
            files_found = True
            Try
                ProgressBar1.Value += 5
            Catch ex As Exception
                ProgressBar1.Value = 5
            End Try

            Try

                debtor = Mid(foundFile, foundFile.Length - 11, 8)
            Catch ex As Exception
                Continue For
            End Try

            'get return code for this debtor - and status 
            param2 = "select return_codeID, client_ref, status_open_closed, status, return_form from Debtor " & _
            " where _rowid = " & debtor
            Dim debtor_dataset As DataSet = get_dataset("onestep", param2)
            If no_of_rows = 0 Then
                MsgBox("Unable to find case number" & debtor)
                Exit Sub
            End If
            Dim status_open_closed As String = debtor_dataset.Tables(0).Rows(0).Item(2)
            If status_open_closed = "O" Then
                Continue For
            End If
            Dim retn_group As String = "Other"
            retn_codeid = 0
            Dim Status As String = debtor_dataset.Tables(0).Rows(0).Item(3)
            If Status = "S" Then
                retn_group = "CTBAIL"
            Else
                Try
                    retn_codeid = debtor_dataset.Tables(0).Rows(0).Item(0)
                Catch ex As Exception
                    retn_codeid = 0
                End Try
            End If

            'get retn_group for non-zero retn_codeid
            If retn_codeid > 0 Then
                'see if return form conatins nullbona
                Dim return_form As String = debtor_dataset.Tables(0).Rows(0).Item(4)
                If InStr(return_form, "Nulla") > 0 Then
                    retn_group = "CTBNULL"
                Else
                    param2 = "select fee_category from CodeReturns" & _
                                   " where _rowid = " & retn_codeid
                    Dim cr_dataset As DataSet = get_dataset("onestep", param2)
                    If no_of_rows = 1 Then
                        Select Case cr_dataset.Tables(0).Rows(0).Item(0)
                            Case 1, 6
                                retn_group = "CTBGONE"
                            Case 2, 4
                                retn_group = "CTBCLNT"
                            Case 3
                                retn_group = "CTBNULL"
                        End Select
                    End If
                End If
            End If
            client_ref = Trim(debtor_dataset.Tables(0).Rows(0).Item(1))
            file_name = file_path & retn_group & "\Return-" & client_ref & "-" & debtor & ".pdf"
            My.Computer.FileSystem.CopyFile(foundFile, file_name)
            controlFile = client_ref & "," & "Return-" & client_ref & "-" & debtor & ".pdf" & "," & retn_group & vbNewLine
            file_name = file_path & retn_group & "\ControlFile.txt"
            My.Computer.FileSystem.WriteAllText(file_name, controlFile, True)
        Next
        If files_found Then
            MsgBox("All reports copied to directories")
        Else
            MsgBox("No reports were found")
        End If
        Me.Close()
    End Sub


   
    Private Sub disable_buttons()
        exitbtn.Enabled = False
        retnbtn.Enabled = False
    End Sub
    Private Sub enable_buttons()
        exitbtn.Enabled = True
        retnbtn.Enabled = True
    End Sub
End Class

﻿Public Class Form1
    Dim upd_txt As String
   
    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ConnectDb2("DebtRecovery")
        ConnectDb2Fees("FeesSQL")
        'delete rows from table
        upd_txt = "delete from TracePayments " & _
            " where trace_BatchNumber >=500" & _
            " and trace_Batchnumber < 540"
        update_sql(upd_txt)

        upd_txt = "delete from TracePayments2 " & _
           " where trace_BatchNumber >=500" & _
            " and trace_Batchnumber < 540"
        update_sql(upd_txt)

        upd_txt = "delete from TracePayments3 " & _
          " where trace_BatchNumber >=500" & _
            " and trace_Batchnumber < 540"

        update_sql(upd_txt)
        Dim lastMonthIDX As Integer = (Year(Now) - 2010) * 12
        lastMonthIDX += Month(Now)
        Dim endDate As Date = CDate(Format(Now, "MMM") & " 01," & Format(Now, "yyyy") & " 00:00:00")
        'get all cases from trace table
        Dim trace_dt As New DataTable
        LoadDataTable2("FeesSQL", "SELECT BatchNumber, SupplierID, DebtorID, TraceResultDate " & _
                                               "FROM TriBureauxTraceResults " & _
                                               " where BatchNumber >=500" & _
                                               " and BatchNumber < 540" & _
                                               " order by BatchNumber, SupplierID, DebtorID", trace_dt, False)
        Dim lastSupplierID As Integer = 0
        Dim lastBatchNumber As Integer = 0
        Dim feesPaid As Decimal = 0
        Dim clientPaid As Decimal = 0
        Dim waiting As Decimal = 0
        Dim vat As Decimal = 0
        Dim links As Integer = 0
        Dim ga As Integer = 0
        Dim other As Integer = 0
        Dim paidMonthArray(100) As Decimal
        Dim feesMonthArray(100) As Decimal
        Dim casesMonthArray(100) As Integer
        Dim casesSetMonthArray(100) As Boolean
        Dim batchNumber As Integer
        Dim supplierID As Integer
        Dim visits As Integer = 0
        Dim linkArray(9999) As Integer
        Dim linkMAX As Integer = 0
        For Each tracerow In trace_dt.Rows
            batchNumber = tracerow(0)
            supplierID = tracerow(1)
            If batchNumber <> lastBatchNumber Or supplierID <> lastSupplierID Then
                linkMAX = 0
                If lastBatchNumber <> 0 Then
                    'save details
                    upd_txt = "insert into TracePayments (trace_batchNumber, " & _
                     "trace_supplierID, trace_ClientPaid, trace_FeesPaid," & _
                     "trace_Waiting, trace_links, trace_ga, trace_visits, trace_other, trace_vat," & _
                     "Nov10,Dec10,Jan11,Feb11,Mar11,Apr11,May11,Jun11,Jul11," & _
                     "Aug11,Sep11,Oct11,Nov11,Dec11," & _
                       "Jan12,Feb12,Mar12,Apr12,May12,Jun12,Jul12," & _
                     "Aug12,Sep12,Oct12,Nov12,Dec12," & _
                       "Jan13,Feb13,Mar13,Apr13,May13,Jun13,Jul13," & _
                     "Aug13,Sep13,Oct13,Nov13,Dec13," & _
                     "Jan14,Feb14,Mar14,Apr14,May14,Jun14,Jul14," & _
                     "Aug14,Sep14,Oct14,Nov14,Dec14," & _
                      "Jan15,Feb15,Mar15,Apr15,May15,Jun15,Jul15," & _
                     "Aug15,Sep15,Oct15,Nov15,Dec15," & _
                      "Jan16,Feb16,Mar16,Apr16,May16,Jun16,Jul16," & _
                     "Aug16,Sep16,Oct16,Nov16,Dec16" & ")" & _
                      "values (" & lastBatchNumber & "," & lastSupplierID & "," & _
                      clientPaid & "," & feesPaid & "," & waiting & "," & links & "," & _
                      ga & "," & visits & "," & other & "," & vat & "," & _
                    paidMonthArray(11) & "," & paidMonthArray(12) & "," & paidMonthArray(13) & "," & paidMonthArray(14) & "," & _
                    paidMonthArray(15) & "," & paidMonthArray(16) & "," & paidMonthArray(17) & "," & paidMonthArray(18) & "," & _
                    paidMonthArray(19) & "," & paidMonthArray(20) & "," & paidMonthArray(21) & "," & paidMonthArray(22) & "," & _
                    paidMonthArray(23) & "," & paidMonthArray(24) & "," & paidMonthArray(25) & "," & paidMonthArray(26) & "," & _
                    paidMonthArray(27) & "," & paidMonthArray(28) & "," & paidMonthArray(29) & "," & paidMonthArray(30) & "," & _
                    paidMonthArray(31) & "," & paidMonthArray(32) & "," & paidMonthArray(33) & "," & paidMonthArray(34) & "," & _
                    paidMonthArray(35) & "," & paidMonthArray(36) & "," & paidMonthArray(37) & "," & paidMonthArray(38) & "," & _
                    paidMonthArray(39) & "," & paidMonthArray(40) & "," & paidMonthArray(41) & "," & paidMonthArray(42) & "," & _
                    paidMonthArray(43) & "," & paidMonthArray(44) & "," & paidMonthArray(45) & "," & paidMonthArray(46) & "," & _
                    paidMonthArray(47) & "," & paidMonthArray(48) & "," & paidMonthArray(49) & "," & paidMonthArray(50) & "," & _
                    paidMonthArray(51) & "," & paidMonthArray(52) & "," & paidMonthArray(53) & "," & paidMonthArray(54) & "," & _
                    paidMonthArray(55) & "," & paidMonthArray(56) & "," & paidMonthArray(57) & "," & paidMonthArray(58) & "," & _
                    paidMonthArray(59) & "," & paidMonthArray(60) & "," & _
                    paidMonthArray(61) & "," & paidMonthArray(62) & "," & paidMonthArray(63) & "," & paidMonthArray(64) & "," & _
                    paidMonthArray(65) & "," & paidMonthArray(66) & "," & paidMonthArray(67) & "," & paidMonthArray(68) & "," & _
                    paidMonthArray(69) & "," & paidMonthArray(70) & "," & paidMonthArray(71) & "," & paidMonthArray(72) & "," & _
                      paidMonthArray(73) & "," & paidMonthArray(74) & "," & paidMonthArray(75) & "," & paidMonthArray(76) & "," & _
                    paidMonthArray(77) & "," & paidMonthArray(78) & "," & paidMonthArray(79) & "," & paidMonthArray(80) & "," & _
                    paidMonthArray(81) & "," & paidMonthArray(82) & "," & paidMonthArray(83) & "," & paidMonthArray(84) & ")"
                    update_sql(upd_txt)

                    upd_txt = "insert into TracePayments2 (trace_batchNumber, " & _
                     "trace_supplierID,Nov10,Dec10,Jan11,Feb11,Mar11,Apr11,May11,Jun11,Jul11," & _
                     "Aug11,Sep11,Oct11,Nov11,Dec11," & _
                       "Jan12,Feb12,Mar12,Apr12,May12,Jun12,Jul12," & _
                     "Aug12,Sep12,Oct12,Nov12,Dec12," & _
                       "Jan13,Feb13,Mar13,Apr13,May13,Jun13,Jul13," & _
                     "Aug13,Sep13,Oct13,Nov13,Dec13," & _
                     "Jan14,Feb14,Mar14,Apr14,May14,Jun14,Jul14," & _
                     "Aug14,Sep14,Oct14,Nov14,Dec14," & _
                      "Jan15,Feb15,Mar15,Apr15,May15,Jun15,Jul15," & _
                     "Aug15,Sep15,Oct15,Nov15,Dec15," & _
                      "Jan16,Feb16,Mar16,Apr16,May16,Jun16,Jul16," & _
                     "Aug16,Sep16,Oct16,Nov16,Dec16" & ")" & _
                      "values (" & lastBatchNumber & "," & lastSupplierID & "," & _
                    feesMonthArray(11) & "," & feesMonthArray(12) & "," & feesMonthArray(13) & "," & feesMonthArray(14) & "," & _
                   feesMonthArray(15) & "," & feesMonthArray(16) & "," & feesMonthArray(17) & "," & feesMonthArray(18) & "," & _
                   feesMonthArray(19) & "," & feesMonthArray(20) & "," & feesMonthArray(21) & "," & feesMonthArray(22) & "," & _
                   feesMonthArray(23) & "," & feesMonthArray(24) & "," & feesMonthArray(25) & "," & feesMonthArray(26) & "," & _
                   feesMonthArray(27) & "," & feesMonthArray(28) & "," & feesMonthArray(29) & "," & feesMonthArray(30) & "," & _
                   feesMonthArray(31) & "," & feesMonthArray(32) & "," & feesMonthArray(33) & "," & feesMonthArray(34) & "," & _
                   feesMonthArray(35) & "," & feesMonthArray(36) & "," & feesMonthArray(37) & "," & feesMonthArray(38) & "," & _
                   feesMonthArray(39) & "," & feesMonthArray(40) & "," & feesMonthArray(41) & "," & feesMonthArray(42) & "," & _
                   feesMonthArray(43) & "," & feesMonthArray(44) & "," & feesMonthArray(45) & "," & feesMonthArray(46) & "," & _
                   feesMonthArray(47) & "," & feesMonthArray(48) & "," & feesMonthArray(49) & "," & feesMonthArray(50) & "," & _
                   feesMonthArray(51) & "," & feesMonthArray(52) & "," & feesMonthArray(53) & "," & feesMonthArray(54) & "," & _
                   feesMonthArray(55) & "," & feesMonthArray(56) & "," & feesMonthArray(57) & "," & feesMonthArray(58) & "," & _
                   feesMonthArray(59) & "," & feesMonthArray(60) & "," & _
                   feesMonthArray(61) & "," & feesMonthArray(62) & "," & feesMonthArray(63) & "," & feesMonthArray(64) & "," & _
                   feesMonthArray(65) & "," & feesMonthArray(66) & "," & feesMonthArray(67) & "," & feesMonthArray(68) & "," & _
                   feesMonthArray(69) & "," & feesMonthArray(70) & "," & feesMonthArray(71) & "," & feesMonthArray(72) & "," & _
                     feesMonthArray(73) & "," & feesMonthArray(74) & "," & feesMonthArray(75) & "," & feesMonthArray(76) & "," & _
                   feesMonthArray(77) & "," & feesMonthArray(78) & "," & feesMonthArray(79) & "," & feesMonthArray(80) & "," & _
                   feesMonthArray(81) & "," & feesMonthArray(82) & "," & feesMonthArray(83) & "," & feesMonthArray(84) & ")"
                    update_sql(upd_txt)

                    upd_txt = "insert into TracePayments3 (trace_batchNumber, " & _
                    "trace_supplierID,Nov10,Dec10,Jan11,Feb11,Mar11,Apr11,May11,Jun11,Jul11," & _
                    "Aug11,Sep11,Oct11,Nov11,Dec11," & _
                      "Jan12,Feb12,Mar12,Apr12,May12,Jun12,Jul12," & _
                    "Aug12,Sep12,Oct12,Nov12,Dec12," & _
                      "Jan13,Feb13,Mar13,Apr13,May13,Jun13,Jul13," & _
                    "Aug13,Sep13,Oct13,Nov13,Dec13," & _
                    "Jan14,Feb14,Mar14,Apr14,May14,Jun14,Jul14," & _
                    "Aug14,Sep14,Oct14,Nov14,Dec14," & _
                      "Jan15,Feb15,Mar15,Apr15,May15,Jun15,Jul15," & _
                     "Aug15,Sep15,Oct15,Nov15,Dec15," & _
                      "Jan16,Feb16,Mar16,Apr16,May16,Jun16,Jul16," & _
                     "Aug16,Sep16,Oct16,Nov16,Dec16" & ")" & _
                     "values (" & lastBatchNumber & "," & lastSupplierID & "," & _
                   casesMonthArray(11) & "," & casesMonthArray(12) & "," & casesMonthArray(13) & "," & casesMonthArray(14) & "," & _
                  casesMonthArray(15) & "," & casesMonthArray(16) & "," & casesMonthArray(17) & "," & casesMonthArray(18) & "," & _
                  casesMonthArray(19) & "," & casesMonthArray(20) & "," & casesMonthArray(21) & "," & casesMonthArray(22) & "," & _
                  casesMonthArray(23) & "," & casesMonthArray(24) & "," & casesMonthArray(25) & "," & casesMonthArray(26) & "," & _
                  casesMonthArray(27) & "," & casesMonthArray(28) & "," & casesMonthArray(29) & "," & casesMonthArray(30) & "," & _
                  casesMonthArray(31) & "," & casesMonthArray(32) & "," & casesMonthArray(33) & "," & casesMonthArray(34) & "," & _
                  casesMonthArray(35) & "," & casesMonthArray(36) & "," & casesMonthArray(37) & "," & casesMonthArray(38) & "," & _
                  casesMonthArray(39) & "," & casesMonthArray(40) & "," & casesMonthArray(41) & "," & casesMonthArray(42) & "," & _
                  casesMonthArray(43) & "," & casesMonthArray(44) & "," & casesMonthArray(45) & "," & casesMonthArray(46) & "," & _
                  casesMonthArray(47) & "," & casesMonthArray(48) & "," & casesMonthArray(49) & "," & casesMonthArray(50) & "," & _
                  casesMonthArray(51) & "," & casesMonthArray(52) & "," & casesMonthArray(53) & "," & casesMonthArray(54) & "," & _
                  casesMonthArray(55) & "," & casesMonthArray(56) & "," & casesMonthArray(57) & "," & casesMonthArray(58) & "," & _
                  casesMonthArray(59) & "," & casesMonthArray(60) & "," & _
                    casesMonthArray(61) & "," & casesMonthArray(62) & "," & casesMonthArray(63) & "," & casesMonthArray(64) & "," & _
                  casesMonthArray(65) & "," & casesMonthArray(66) & "," & casesMonthArray(67) & "," & casesMonthArray(68) & "," & _
                  casesMonthArray(69) & "," & casesMonthArray(70) & "," & casesMonthArray(71) & "," & casesMonthArray(72) & "," & _
                  casesMonthArray(73) & "," & casesMonthArray(74) & "," & casesMonthArray(75) & "," & casesMonthArray(76) & "," & _
                  casesMonthArray(77) & "," & casesMonthArray(78) & "," & casesMonthArray(79) & "," & casesMonthArray(80) & "," & _
                  casesMonthArray(81) & "," & casesMonthArray(82) & "," & casesMonthArray(83) & "," & casesMonthArray(84) & ")"
                    update_sql(upd_txt)
                    'reset values
                    feesPaid = 0
                    clientPaid = 0
                    waiting = 0
                    links = 0
                    ga = 0
                    other = 0
                    ReDim paidMonthArray(100)
                    ReDim feesMonthArray(100)
                    ReDim casesMonthArray(100)
                    visits = 0
                    vat = 0

                End If
            End If
            lastBatchNumber = batchNumber
            lastSupplierID = supplierID
            Dim debtorID As Integer = tracerow(2)
            Dim resultDate As Date = tracerow(3)
            'get any visits since trace
            Dim visit_dt As New DataTable
            LoadDataTable2("DebtRecovery", "select count(*) from Visit " & _
                           " where debtorID = " & debtorID & _
                           " and date_visited > '" & Format(resultDate, "yyyy-MM-dd") & "'" & _
                           " and date_visited < '" & Format(endDate, "yyyy-MM-dd") & "'", visit_dt, False)
            If visit_dt.Rows(0).Item(0) > 0 Then
                visits += 1
            End If
            'get cancelled cases
            Dim can_dt As New DataTable
            LoadDataTable2("DebtRecovery", " select C._rowID, C.fee_category from Debtor D, codeReturns C" & _
                           " where D._rowID = " & debtorID & _
                           " and D.status = 'C'" & _
                           " and return_date > '" & Format(resultDate, "yyyy-MM-dd") & "'" & _
                             " and return_date < '" & Format(endDate, "yyyy-MM-dd") & "'" & _
                           " and C._rowID = D.return_codeID", can_dt, False)
            Try
                If can_dt.Rows(0).Item(0) = 33 Then
                    ga += 1
                Else
                    If can_dt.Rows(0).Item(1) = 1 Then
                        ga += 1
                    Else
                        other += 1
                    End If
                End If
            Catch ex As Exception

            End Try

            'get all payments for the case since results date
            For idx = 0 To 100
                casesSetMonthArray(idx) = False
            Next
            Dim pay_dt As New DataTable
            LoadDataTable2("DebtRecovery", " select split_debt, split_costs, split_fees, split_van,split_other, date,status, amount, split_vat " & _
                           " from Payment" & _
                           " where debtorID = " & debtorID & _
                           " and status in('R','W')" & _
                           " and date >= '" & Format(resultDate, "yyyy-MM-dd") & "'" & _
                           " and status_date < '" & Format(endDate, "yyyy-MM-dd") & "'", pay_dt, False)
            For Each payRow In pay_dt.Rows
                Dim paidDate As Date = payRow(5)
                If Year(paidDate) < 2010 Then
                    Continue For
                End If
                clientPaid = clientPaid + payRow(0) + payRow(1)
                feesPaid = feesPaid + payRow(2) + payRow(3) + payRow(4)
                If payRow(6) = "W" Then
                    waiting += payRow(7)
                End If
                Dim monthIDX As Integer = (Year(paidDate) - 2010) * 12
                monthIDX += Month(paidDate)
                paidMonthArray(monthIDX) += payRow(7)
                feesMonthArray(monthIDX) += payRow(2) + payRow(3) + payRow(4)
                If casesSetMonthArray(monthIDX) = False Then
                    casesMonthArray(monthIDX) += 1
                    casesSetMonthArray(monthIDX) = True
                End If
                vat += payRow(8)

            Next
            'now get any links
            Dim linkID As Integer
            linkID = GetSQLResults2("DebtRecovery", "select linkID " & _
                                " from debtor " & _
                                " where _rowid = " & debtorID)
            If linkID > 0 Then
                'get any visits since trace if not already done
                Dim linkFound As Boolean = False
                For linkIDX = 1 To linkMAX
                    If linkArray(linkIDX) = linkID Then
                        linkFound = True
                        Exit For
                    End If
                Next
                If linkFound = False Then
                    Dim visit2_dt As New DataTable
                    LoadDataTable2("DebtRecovery", "select count(*), D._rowID from Visit V, debtor D " & _
                                   " where D._rowID <> " & debtorID & _
                                   " and V.DebtorID = D._rowID" & _
                                   " and linkID = " & linkID & _
                                   " and date_visited > '" & Format(resultDate, "yyyy-MM-dd") & "'" & _
                                    " and date_visited < '" & Format(endDate, "yyyy-MM-dd") & "'" & _
                                   " group by D._rowID", visit2_dt, False)
                    For Each visit2Row In visit2_dt.Rows
                        Dim d As Integer = visit2Row(1)
                        If visit2Row(0) > 0 Then
                            visits += 1
                        End If
                    Next
                    linkMAX += 1
                    linkArray(linkMAX) = linkID
                End If

                'get number of links and check if returned since trace
                Dim link3_dt As New DataTable
                LoadDataTable2("DebtRecovery", "Select count(*) from debtor " & _
                               " where linkID = " & linkID & _
                               " and _rowID <> " & debtorID, link3_dt, False)
                links += link3_dt.Rows(0).Item(0)

                Dim link2_dt As New DataTable
                LoadDataTable2("DebtRecovery", "select _rowID, status,return_date, return_CodeID from debtor" & _
                               " where linkID = " & linkID & _
                               " and _rowid <> " & debtorID, link2_dt, False)
                Dim gaFound As Boolean = False
                Dim otherFound As Boolean = False
                For Each link2Row In link2_dt.Rows
                    'links += 1
                    If link2Row(1) = "C" Then
                        Dim returnDate As Date
                        Try
                            returnDate = link2Row(2)
                        Catch ex As Exception
                            Continue For
                        End Try

                        If Format(returnDate, "yyyy-MM-dd") > Format(resultDate, "yyyy-MM-dd") And _
                            Format(returnDate, "yyyy-MM-dd") < Format(endDate, "yyyy-MM-dd") Then
                            If link2Row(3) = 33 Then
                                gaFound = True
                                Exit For
                            Else
                                'get fee_cat from code returns
                                Dim feeCategory As Integer
                                feeCategory = GetSQLResults2("DebtRecovery", "select fee_category " & _
                                                    " from codeReturns " & _
                                                    " where _rowid = " & link2Row(3))
                                If feeCategory = 1 Then
                                    gaFound = True
                                    Exit For
                                Else
                                    otherFound = True
                                    Exit For
                                End If
                            End If
                        End If
                    End If
                Next
                If gaFound Then
                    ga += 1
                ElseIf otherFound Then
                    other += 1
                End If

                Dim link_dt As New DataTable
                Dim lastLinkDebtorID As Integer = 0
                LoadDataTable2("DebtRecovery", " select split_debt, split_costs, split_fees, split_van, split_other, date, D._rowID, P.status, P.amount, split_vat " & _
                               " from Payment P, debtor D" & _
                               " where P.debtorID = D._rowID" & _
                               " and D._rowid <> " & debtorID & _
                               " and D.linkID = " & linkID & _
                               " and P.status in ('R','W') " & _
                               " and P.date >= '" & Format(resultDate, "yyyy-MM-dd") & "'" & _
                                " and P.status_date < '" & Format(endDate, "yyyy-MM-dd") & "'" & _
                               " order by D._rowID", link_dt, False)
                For Each linkRow In link_dt.Rows
                    Dim linkDebtorID As Integer = linkRow(6)
                    If linkDebtorID <> lastLinkDebtorID Then
                        For idx = 0 To 100
                            casesSetMonthArray(idx) = False
                        Next
                    End If
                    Dim paidDate As Date = linkRow(5)
                    If Year(paidDate) < 2010 Then
                        Continue For
                    End If
                    vat += linkRow(9)
                    clientPaid = clientPaid + linkRow(0) + linkRow(1)
                    feesPaid = feesPaid + linkRow(2) + linkRow(3) + linkRow(4)
                    If linkRow(7) = "W" Then
                        waiting += linkRow(8)
                    End If
                    Dim monthIDX As Integer = (Year(paidDate) - 2010) * 12
                    monthIDX += Month(paidDate)
                    paidMonthArray(monthIDX) += linkRow(8)
                    feesMonthArray(monthIDX) += linkRow(2) + linkRow(3) + linkRow(4)
                    If casesSetMonthArray(monthIDX) = False Then
                        casesMonthArray(monthIDX) += 1
                        casesSetMonthArray(monthIDX) = True
                    End If
                    lastLinkDebtorID = linkDebtorID
                Next

            End If

        Next
        'save last batch
        upd_txt = "insert into TracePayments (trace_batchNumber, " & _
         "trace_supplierID, trace_ClientPaid, trace_FeesPaid," & _
         "trace_Waiting, trace_links, trace_ga, trace_visits,trace_other,trace_vat," & _
        "Nov10,Dec10,Jan11,Feb11,Mar11,Apr11,May11,Jun11,Jul11," & _
                     "Aug11,Sep11,Oct11,Nov11,Dec11," & _
                       "Jan12,Feb12,Mar12,Apr12,May12,Jun12,Jul12," & _
                     "Aug12,Sep12,Oct12,Nov12,Dec12," & _
                       "Jan13,Feb13,Mar13,Apr13,May13,Jun13,Jul13," & _
                     "Aug13,Sep13,Oct13,Nov13,Dec13," & _
                     "Jan14,Feb14,Mar14,Apr14,May14,Jun14,Jul14," & _
                     "Aug14,Sep14,Oct14,Nov14,Dec14," & _
                       "Jan15,Feb15,Mar15,Apr15,May15,Jun15,Jul15," & _
                     "Aug15,Sep15,Oct15,Nov15,Dec15," & _
                      "Jan16,Feb16,Mar16,Apr16,May16,Jun16,Jul16," & _
                     "Aug16,Sep16,Oct16,Nov16,Dec16" & ")" & _
          "values (" & batchNumber & "," & supplierID & "," & _
          clientPaid & "," & feesPaid & "," & waiting & "," & links & "," & _
          ga & "," & visits & "," & other & "," & vat & "," & _
        paidMonthArray(11) & "," & paidMonthArray(12) & "," & paidMonthArray(13) & "," & paidMonthArray(14) & "," & _
        paidMonthArray(15) & "," & paidMonthArray(16) & "," & paidMonthArray(17) & "," & paidMonthArray(18) & "," & _
        paidMonthArray(19) & "," & paidMonthArray(20) & "," & paidMonthArray(21) & "," & paidMonthArray(22) & "," & _
        paidMonthArray(23) & "," & paidMonthArray(24) & "," & paidMonthArray(25) & "," & paidMonthArray(26) & "," & _
        paidMonthArray(27) & "," & paidMonthArray(28) & "," & paidMonthArray(29) & "," & paidMonthArray(30) & "," & _
        paidMonthArray(31) & "," & paidMonthArray(32) & "," & paidMonthArray(33) & "," & paidMonthArray(34) & "," & _
        paidMonthArray(35) & "," & paidMonthArray(36) & "," & paidMonthArray(37) & "," & paidMonthArray(38) & "," & _
        paidMonthArray(39) & "," & paidMonthArray(40) & "," & paidMonthArray(41) & "," & paidMonthArray(42) & "," & _
        paidMonthArray(43) & "," & paidMonthArray(44) & "," & paidMonthArray(45) & "," & paidMonthArray(46) & "," & _
        paidMonthArray(47) & "," & paidMonthArray(48) & "," & paidMonthArray(49) & "," & paidMonthArray(50) & "," & _
        paidMonthArray(51) & "," & paidMonthArray(52) & "," & paidMonthArray(53) & "," & paidMonthArray(54) & "," & _
        paidMonthArray(55) & "," & paidMonthArray(56) & "," & paidMonthArray(57) & "," & paidMonthArray(58) & "," & _
        paidMonthArray(59) & "," & paidMonthArray(60) & "," & _
          paidMonthArray(61) & "," & paidMonthArray(62) & "," & paidMonthArray(63) & "," & paidMonthArray(64) & "," & _
                    paidMonthArray(65) & "," & paidMonthArray(66) & "," & paidMonthArray(67) & "," & paidMonthArray(68) & "," & _
                    paidMonthArray(69) & "," & paidMonthArray(70) & "," & paidMonthArray(71) & "," & paidMonthArray(72) & "," & _
                      paidMonthArray(73) & "," & paidMonthArray(74) & "," & paidMonthArray(75) & "," & paidMonthArray(76) & "," & _
                    paidMonthArray(77) & "," & paidMonthArray(78) & "," & paidMonthArray(79) & "," & paidMonthArray(80) & "," & _
                    paidMonthArray(81) & "," & paidMonthArray(82) & "," & paidMonthArray(83) & "," & paidMonthArray(84) & ")"
        update_sql(upd_txt)
        upd_txt = "insert into TracePayments2 (trace_batchNumber, " & _
                    "trace_supplierID,Nov10,Dec10,Jan11,Feb11,Mar11,Apr11,May11,Jun11,Jul11," & _
                    "Aug11,Sep11,Oct11,Nov11,Dec11," & _
                      "Jan12,Feb12,Mar12,Apr12,May12,Jun12,Jul12," & _
                    "Aug12,Sep12,Oct12,Nov12,Dec12," & _
                      "Jan13,Feb13,Mar13,Apr13,May13,Jun13,Jul13," & _
                    "Aug13,Sep13,Oct13,Nov13,Dec13," & _
                    "Jan14,Feb14,Mar14,Apr14,May14,Jun14,Jul14," & _
                    "Aug14,Sep14,Oct14,Nov14,Dec14," & _
                      "Jan15,Feb15,Mar15,Apr15,May15,Jun15,Jul15," & _
                     "Aug15,Sep15,Oct15,Nov15,Dec15," & _
                      "Jan16,Feb16,Mar16,Apr16,May16,Jun16,Jul16," & _
                     "Aug16,Sep16,Oct16,Nov16,Dec16" & ")" & _
                     "values (" & batchNumber & "," & supplierID & "," & _
                   feesMonthArray(11) & "," & feesMonthArray(12) & "," & feesMonthArray(13) & "," & feesMonthArray(14) & "," & _
                  feesMonthArray(15) & "," & feesMonthArray(16) & "," & feesMonthArray(17) & "," & feesMonthArray(18) & "," & _
                  feesMonthArray(19) & "," & feesMonthArray(20) & "," & feesMonthArray(21) & "," & feesMonthArray(22) & "," & _
                  feesMonthArray(23) & "," & feesMonthArray(24) & "," & feesMonthArray(25) & "," & feesMonthArray(26) & "," & _
                  feesMonthArray(27) & "," & feesMonthArray(28) & "," & feesMonthArray(29) & "," & feesMonthArray(30) & "," & _
                  feesMonthArray(31) & "," & feesMonthArray(32) & "," & feesMonthArray(33) & "," & feesMonthArray(34) & "," & _
                  feesMonthArray(35) & "," & feesMonthArray(36) & "," & feesMonthArray(37) & "," & feesMonthArray(38) & "," & _
                  feesMonthArray(39) & "," & feesMonthArray(40) & "," & feesMonthArray(41) & "," & feesMonthArray(42) & "," & _
                  feesMonthArray(43) & "," & feesMonthArray(44) & "," & feesMonthArray(45) & "," & feesMonthArray(46) & "," & _
                  feesMonthArray(47) & "," & feesMonthArray(48) & "," & feesMonthArray(49) & "," & feesMonthArray(50) & "," & _
                  feesMonthArray(51) & "," & feesMonthArray(52) & "," & feesMonthArray(53) & "," & feesMonthArray(54) & "," & _
                  feesMonthArray(55) & "," & feesMonthArray(56) & "," & feesMonthArray(57) & "," & feesMonthArray(58) & "," & _
                  feesMonthArray(59) & "," & feesMonthArray(60) & "," & _
                   feesMonthArray(61) & "," & feesMonthArray(62) & "," & feesMonthArray(63) & "," & feesMonthArray(64) & "," & _
                   feesMonthArray(65) & "," & feesMonthArray(66) & "," & feesMonthArray(67) & "," & feesMonthArray(68) & "," & _
                   feesMonthArray(69) & "," & feesMonthArray(70) & "," & feesMonthArray(71) & "," & feesMonthArray(72) & "," & _
                     feesMonthArray(73) & "," & feesMonthArray(74) & "," & feesMonthArray(75) & "," & feesMonthArray(76) & "," & _
                   feesMonthArray(77) & "," & feesMonthArray(78) & "," & feesMonthArray(79) & "," & feesMonthArray(80) & "," & _
                   feesMonthArray(81) & "," & feesMonthArray(82) & "," & feesMonthArray(83) & "," & feesMonthArray(84) & ")"
        update_sql(upd_txt)
        upd_txt = "insert into TracePayments3 (trace_batchNumber, " & _
                   "trace_supplierID,Nov10,Dec10,Jan11,Feb11,Mar11,Apr11,May11,Jun11,Jul11," & _
                   "Aug11,Sep11,Oct11,Nov11,Dec11," & _
                     "Jan12,Feb12,Mar12,Apr12,May12,Jun12,Jul12," & _
                   "Aug12,Sep12,Oct12,Nov12,Dec12," & _
                     "Jan13,Feb13,Mar13,Apr13,May13,Jun13,Jul13," & _
                   "Aug13,Sep13,Oct13,Nov13,Dec13," & _
                   "Jan14,Feb14,Mar14,Apr14,May14,Jun14,Jul14," & _
                   "Aug14,Sep14,Oct14,Nov14,Dec14," & _
                     "Jan15,Feb15,Mar15,Apr15,May15,Jun15,Jul15," & _
                     "Aug15,Sep15,Oct15,Nov15,Dec15," & _
                      "Jan16,Feb16,Mar16,Apr16,May16,Jun16,Jul16," & _
                     "Aug16,Sep16,Oct16,Nov16,Dec16" & ")" & _
                    "values (" & batchNumber & "," & supplierID & "," & _
                  casesMonthArray(11) & "," & casesMonthArray(12) & "," & casesMonthArray(13) & "," & casesMonthArray(14) & "," & _
                 casesMonthArray(15) & "," & casesMonthArray(16) & "," & casesMonthArray(17) & "," & casesMonthArray(18) & "," & _
                 casesMonthArray(19) & "," & casesMonthArray(20) & "," & casesMonthArray(21) & "," & casesMonthArray(22) & "," & _
                 casesMonthArray(23) & "," & casesMonthArray(24) & "," & casesMonthArray(25) & "," & casesMonthArray(26) & "," & _
                 casesMonthArray(27) & "," & casesMonthArray(28) & "," & casesMonthArray(29) & "," & casesMonthArray(30) & "," & _
                 casesMonthArray(31) & "," & casesMonthArray(32) & "," & casesMonthArray(33) & "," & casesMonthArray(34) & "," & _
                 casesMonthArray(35) & "," & casesMonthArray(36) & "," & casesMonthArray(37) & "," & casesMonthArray(38) & "," & _
                 casesMonthArray(39) & "," & casesMonthArray(40) & "," & casesMonthArray(41) & "," & casesMonthArray(42) & "," & _
                 casesMonthArray(43) & "," & casesMonthArray(44) & "," & casesMonthArray(45) & "," & casesMonthArray(46) & "," & _
                 casesMonthArray(47) & "," & casesMonthArray(48) & "," & casesMonthArray(49) & "," & casesMonthArray(50) & "," & _
                 casesMonthArray(51) & "," & casesMonthArray(52) & "," & casesMonthArray(53) & "," & casesMonthArray(54) & "," & _
                 casesMonthArray(55) & "," & casesMonthArray(56) & "," & casesMonthArray(57) & "," & casesMonthArray(58) & "," & _
                 casesMonthArray(59) & "," & casesMonthArray(60) & "," & _
                 casesMonthArray(61) & "," & casesMonthArray(62) & "," & casesMonthArray(63) & "," & casesMonthArray(64) & "," & _
                  casesMonthArray(65) & "," & casesMonthArray(66) & "," & casesMonthArray(67) & "," & casesMonthArray(68) & "," & _
                  casesMonthArray(69) & "," & casesMonthArray(70) & "," & casesMonthArray(71) & "," & casesMonthArray(72) & "," & _
                  casesMonthArray(73) & "," & casesMonthArray(74) & "," & casesMonthArray(75) & "," & casesMonthArray(76) & "," & _
                  casesMonthArray(77) & "," & casesMonthArray(78) & "," & casesMonthArray(79) & "," & casesMonthArray(80) & "," & _
                  casesMonthArray(81) & "," & casesMonthArray(82) & "," & casesMonthArray(83) & "," & casesMonthArray(84) & ")"
        update_sql(upd_txt)
        Me.Close()

        'ConnectDb2("DebtRecovery")
        'ConnectDb2Fees("FeesSQL")
        ''delete rows from table
        'upd_txt = "delete from TracePayments " & _
        '    " where trace_BatchNumber >=500"
        'update_sql(upd_txt)
        'upd_txt = "delete from TracePayments2 " & _
        '   " where trace_BatchNumber >=500"
        'update_sql(upd_txt)
        'upd_txt = "delete from TracePayments3 " & _
        '  " where trace_BatchNumber >=500"

        'update_sql(upd_txt)
        'Dim lastMonthIDX As Integer = (Year(Now) - 2010) * 12
        'lastMonthIDX += Month(Now)
        ''get all cases from trace table
        'Dim trace_dt As New DataTable
        'LoadDataTable2("FeesSQL", "SELECT BatchNumber, SupplierID, DebtorID, TraceResultDate " & _
        '                                       "FROM TriBureauxTraceResults " & _
        '                                       " where BatchNumber >= 500" & _
        '                                       " order by BatchNumber, SupplierID, DebtorID", trace_dt, False)
        'Dim lastSupplierID As Integer = 0
        'Dim lastBatchNumber As Integer = 0
        'Dim feesPaid As Decimal = 0
        'Dim clientPaid As Decimal = 0
        'Dim waiting As Decimal = 0
        'Dim vat As Decimal = 0
        'Dim links As Integer = 0
        'Dim ga As Integer = 0
        'Dim other As Integer = 0
        'Dim paidMonthArray(100) As Decimal
        'Dim feesMonthArray(100) As Decimal
        'Dim casesMonthArray(100) As Integer
        'Dim casesSetMonthArray(100) As Boolean
        'Dim batchNumber As Integer
        'Dim supplierID As Integer
        'Dim visits As Integer = 0
        'Dim linkArray(9999) As Integer
        'Dim linkMAX As Integer = 0
        'For Each tracerow In trace_dt.Rows

        '    batchNumber = tracerow(0)
        '    supplierID = tracerow(1)
        '    If batchNumber <> lastBatchNumber Or supplierID <> lastSupplierID Then
        '        linkMAX = 0
        '        If lastBatchNumber <> 0 Then
        '            'save details
        '            upd_txt = "insert into TracePayments (trace_batchNumber, " & _
        '             "trace_supplierID, trace_ClientPaid, trace_FeesPaid," & _
        '             "trace_Waiting, trace_links, trace_ga, trace_visits, trace_other, trace_vat," & _
        '             "Nov10,Dec10,Jan11,Feb11,Mar11,Apr11,May11,Jun11,Jul11," & _
        '             "Aug11,Sep11,Oct11,Nov11,Dec11," & _
        '               "Jan12,Feb12,Mar12,Apr12,May12,Jun12,Jul12," & _
        '             "Aug12,Sep12,Oct12,Nov12,Dec12," & _
        '               "Jan13,Feb13,Mar13,Apr13,May13,Jun13,Jul13," & _
        '             "Aug13,Sep13,Oct13,Nov13,Dec13," & _
        '             "Jan14,Feb14,Mar14,Apr14,May14,Jun14,Jul14," & _
        '             "Aug14,Sep14,Oct14,Nov14,Dec14" & ")" & _
        '              "values (" & lastBatchNumber & "," & lastSupplierID & "," & _
        '              clientPaid & "," & feesPaid & "," & waiting & "," & links & "," & _
        '              ga & "," & visits & "," & other & "," & vat & "," & _
        '            paidMonthArray(11) & "," & paidMonthArray(12) & "," & paidMonthArray(13) & "," & paidMonthArray(14) & "," & _
        '            paidMonthArray(15) & "," & paidMonthArray(16) & "," & paidMonthArray(17) & "," & paidMonthArray(18) & "," & _
        '            paidMonthArray(19) & "," & paidMonthArray(20) & "," & paidMonthArray(21) & "," & paidMonthArray(22) & "," & _
        '            paidMonthArray(23) & "," & paidMonthArray(24) & "," & paidMonthArray(25) & "," & paidMonthArray(26) & "," & _
        '            paidMonthArray(27) & "," & paidMonthArray(28) & "," & paidMonthArray(29) & "," & paidMonthArray(30) & "," & _
        '            paidMonthArray(31) & "," & paidMonthArray(32) & "," & paidMonthArray(33) & "," & paidMonthArray(34) & "," & _
        '            paidMonthArray(35) & "," & paidMonthArray(36) & "," & paidMonthArray(37) & "," & paidMonthArray(38) & "," & _
        '            paidMonthArray(39) & "," & paidMonthArray(40) & "," & paidMonthArray(41) & "," & paidMonthArray(42) & "," & _
        '            paidMonthArray(43) & "," & paidMonthArray(44) & "," & paidMonthArray(45) & "," & paidMonthArray(46) & "," & _
        '            paidMonthArray(47) & "," & paidMonthArray(48) & "," & paidMonthArray(49) & "," & paidMonthArray(50) & "," & _
        '            paidMonthArray(51) & "," & paidMonthArray(52) & "," & paidMonthArray(53) & "," & paidMonthArray(54) & "," & _
        '            paidMonthArray(55) & "," & paidMonthArray(56) & "," & paidMonthArray(57) & "," & paidMonthArray(58) & "," & _
        '            paidMonthArray(59) & "," & paidMonthArray(60) & ")"
        '            update_sql(upd_txt)

        '            upd_txt = "insert into TracePayments2 (trace_batchNumber, " & _
        '             "trace_supplierID,Nov10,Dec10,Jan11,Feb11,Mar11,Apr11,May11,Jun11,Jul11," & _
        '             "Aug11,Sep11,Oct11,Nov11,Dec11," & _
        '               "Jan12,Feb12,Mar12,Apr12,May12,Jun12,Jul12," & _
        '             "Aug12,Sep12,Oct12,Nov12,Dec12," & _
        '               "Jan13,Feb13,Mar13,Apr13,May13,Jun13,Jul13," & _
        '             "Aug13,Sep13,Oct13,Nov13,Dec13," & _
        '             "Jan14,Feb14,Mar14,Apr14,May14,Jun14,Jul14," & _
        '             "Aug14,Sep14,Oct14,Nov14,Dec14" & ")" & _
        '              "values (" & lastBatchNumber & "," & lastSupplierID & "," & _
        '            feesMonthArray(11) & "," & feesMonthArray(12) & "," & feesMonthArray(13) & "," & feesMonthArray(14) & "," & _
        '           feesMonthArray(15) & "," & feesMonthArray(16) & "," & feesMonthArray(17) & "," & feesMonthArray(18) & "," & _
        '           feesMonthArray(19) & "," & feesMonthArray(20) & "," & feesMonthArray(21) & "," & feesMonthArray(22) & "," & _
        '           feesMonthArray(23) & "," & feesMonthArray(24) & "," & feesMonthArray(25) & "," & feesMonthArray(26) & "," & _
        '           feesMonthArray(27) & "," & feesMonthArray(28) & "," & feesMonthArray(29) & "," & feesMonthArray(30) & "," & _
        '           feesMonthArray(31) & "," & feesMonthArray(32) & "," & feesMonthArray(33) & "," & feesMonthArray(34) & "," & _
        '           feesMonthArray(35) & "," & feesMonthArray(36) & "," & feesMonthArray(37) & "," & feesMonthArray(38) & "," & _
        '           feesMonthArray(39) & "," & feesMonthArray(40) & "," & feesMonthArray(41) & "," & feesMonthArray(42) & "," & _
        '           feesMonthArray(43) & "," & feesMonthArray(44) & "," & feesMonthArray(45) & "," & feesMonthArray(46) & "," & _
        '           feesMonthArray(47) & "," & feesMonthArray(48) & "," & feesMonthArray(49) & "," & feesMonthArray(50) & "," & _
        '           feesMonthArray(51) & "," & feesMonthArray(52) & "," & feesMonthArray(53) & "," & feesMonthArray(54) & "," & _
        '           feesMonthArray(55) & "," & feesMonthArray(56) & "," & feesMonthArray(57) & "," & feesMonthArray(58) & "," & _
        '           feesMonthArray(59) & "," & feesMonthArray(60) & ")"
        '            update_sql(upd_txt)

        '            upd_txt = "insert into TracePayments3 (trace_batchNumber, " & _
        '            "trace_supplierID,Nov10,Dec10,Jan11,Feb11,Mar11,Apr11,May11,Jun11,Jul11," & _
        '            "Aug11,Sep11,Oct11,Nov11,Dec11," & _
        '              "Jan12,Feb12,Mar12,Apr12,May12,Jun12,Jul12," & _
        '            "Aug12,Sep12,Oct12,Nov12,Dec12," & _
        '              "Jan13,Feb13,Mar13,Apr13,May13,Jun13,Jul13," & _
        '            "Aug13,Sep13,Oct13,Nov13,Dec13," & _
        '            "Jan14,Feb14,Mar14,Apr14,May14,Jun14,Jul14," & _
        '            "Aug14,Sep14,Oct14,Nov14,Dec14" & ")" & _
        '             "values (" & lastBatchNumber & "," & lastSupplierID & "," & _
        '           casesMonthArray(11) & "," & casesMonthArray(12) & "," & casesMonthArray(13) & "," & casesMonthArray(14) & "," & _
        '          casesMonthArray(15) & "," & casesMonthArray(16) & "," & casesMonthArray(17) & "," & casesMonthArray(18) & "," & _
        '          casesMonthArray(19) & "," & casesMonthArray(20) & "," & casesMonthArray(21) & "," & casesMonthArray(22) & "," & _
        '          casesMonthArray(23) & "," & casesMonthArray(24) & "," & casesMonthArray(25) & "," & casesMonthArray(26) & "," & _
        '          casesMonthArray(27) & "," & casesMonthArray(28) & "," & casesMonthArray(29) & "," & casesMonthArray(30) & "," & _
        '          casesMonthArray(31) & "," & casesMonthArray(32) & "," & casesMonthArray(33) & "," & casesMonthArray(34) & "," & _
        '          casesMonthArray(35) & "," & casesMonthArray(36) & "," & casesMonthArray(37) & "," & casesMonthArray(38) & "," & _
        '          casesMonthArray(39) & "," & casesMonthArray(40) & "," & casesMonthArray(41) & "," & casesMonthArray(42) & "," & _
        '          casesMonthArray(43) & "," & casesMonthArray(44) & "," & casesMonthArray(45) & "," & casesMonthArray(46) & "," & _
        '          casesMonthArray(47) & "," & casesMonthArray(48) & "," & casesMonthArray(49) & "," & casesMonthArray(50) & "," & _
        '          casesMonthArray(51) & "," & casesMonthArray(52) & "," & casesMonthArray(53) & "," & casesMonthArray(54) & "," & _
        '          casesMonthArray(55) & "," & casesMonthArray(56) & "," & casesMonthArray(57) & "," & casesMonthArray(58) & "," & _
        '          casesMonthArray(59) & "," & casesMonthArray(60) & ")"
        '            update_sql(upd_txt)
        '            'reset values
        '            feesPaid = 0
        '            clientPaid = 0
        '            waiting = 0
        '            links = 0
        '            ga = 0
        '            other = 0
        '            ReDim paidMonthArray(100)
        '            ReDim feesMonthArray(100)
        '            ReDim casesMonthArray(100)
        '            visits = 0
        '            vat = 0

        '        End If
        '    End If
        '    lastBatchNumber = batchNumber
        '    lastSupplierID = supplierID
        '    Dim debtorID As Integer = tracerow(2)
        '    Dim resultDate As Date = tracerow(3)
        '    'get any visits since trace
        '    Dim visit_dt As New DataTable
        '    LoadDataTable2("DebtRecovery", "select count(*) from Visit " & _
        '                   " where debtorID = " & debtorID & _
        '                   " and date_visited > '" & Format(resultDate, "yyyy-MM-dd") & "'", visit_dt, False)
        '    If visit_dt.Rows(0).Item(0) > 0 Then
        '        visits += 1
        '    End If
        '    'get cancelled cases
        '    Dim can_dt As New DataTable
        '    LoadDataTable2("DebtRecovery", " select C._rowID, C.fee_category from Debtor D, codeReturns C" & _
        '                   " where D._rowID = " & debtorID & _
        '                   " and D.status = 'C'" & _
        '                   " and return_date > '" & Format(resultDate, "yyyy-MM-dd") & "'" & _
        '                   " and C._rowID = D.return_codeID", can_dt, False)
        '    Try
        '        If can_dt.Rows(0).Item(0) = 33 Then
        '            ga += 1
        '        Else
        '            If can_dt.Rows(0).Item(1) = 1 Then
        '                ga += 1
        '            Else
        '                other += 1
        '            End If
        '        End If
        '    Catch ex As Exception

        '    End Try

        '    'get all payments for the case since results date
        '    For idx = 0 To 100
        '        casesSetMonthArray(idx) = False
        '    Next
        '    Dim pay_dt As New DataTable
        '    LoadDataTable2("DebtRecovery", " select split_debt, split_costs, split_fees, split_van,split_other, date,status, amount, split_vat " & _
        '                   " from Payment" & _
        '                   " where debtorID = " & debtorID & _
        '                   " and status in('R','W')" & _
        '                   " and date >= '" & Format(resultDate, "yyyy-MM-dd") & "'", pay_dt, False)
        '    For Each payRow In pay_dt.Rows
        '        Dim paidDate As Date = payRow(5)
        '        If Year(paidDate) < 2010 Then
        '            Continue For
        '        End If
        '        clientPaid = clientPaid + payRow(0) + payRow(1)
        '        feesPaid = feesPaid + payRow(2) + payRow(3) + payRow(4)
        '        If payRow(6) = "W" Then
        '            waiting += payRow(7)
        '        End If
        '        Dim monthIDX As Integer = (Year(paidDate) - 2010) * 12
        '        monthIDX += Month(paidDate)
        '        paidMonthArray(monthIDX) += payRow(7)
        '        feesMonthArray(monthIDX) += payRow(2) + payRow(3) + payRow(4)
        '        If casesSetMonthArray(monthIDX) = False Then
        '            casesMonthArray(monthIDX) += 1
        '            casesSetMonthArray(monthIDX) = True
        '        End If
        '        vat += payRow(8)

        '    Next
        '    'now get any links
        '    Dim linkID As Integer
        '    linkID = GetSQLResults2("DebtRecovery", "select linkID " & _
        '                        " from debtor " & _
        '                        " where _rowid = " & debtorID)
        '    If linkID > 0 Then
        '        'get any visits since trace if not already done
        '        Dim linkFound As Boolean = False
        '        For linkIDX = 1 To linkMAX
        '            If linkArray(linkIDX) = linkID Then
        '                linkFound = True
        '                Exit For
        '            End If
        '        Next
        '        If linkFound = False Then
        '            Dim visit2_dt As New DataTable
        '            LoadDataTable2("DebtRecovery", "select count(*), D._rowID from Visit V, debtor D " & _
        '                           " where D._rowID <> " & debtorID & _
        '                           " and V.DebtorID = D._rowID" & _
        '                           " and linkID = " & linkID & _
        '                           " and date_visited > '" & Format(resultDate, "yyyy-MM-dd") & "'" & _
        '                           " group by D._rowID", visit2_dt, False)
        '            For Each visit2Row In visit2_dt.Rows
        '                Dim d As Integer = visit2Row(1)
        '                If visit2Row(0) > 0 Then
        '                    visits += 1
        '                End If
        '            Next
        '            linkMAX += 1
        '            linkArray(linkMAX) = linkID
        '        End If

        '        'get number of links and check if returned since trace
        '        Dim link3_dt As New DataTable
        '        LoadDataTable2("DebtRecovery", "Select count(*) from debtor " & _
        '                       " where linkID = " & linkID & _
        '                       " and _rowID <> " & debtorID, link3_dt, False)
        '        links += link3_dt.Rows(0).Item(0)

        '        Dim link2_dt As New DataTable
        '        LoadDataTable2("DebtRecovery", "select _rowID, status,return_date, return_CodeID from debtor" & _
        '                       " where linkID = " & linkID & _
        '                       " and _rowid <> " & debtorID, link2_dt, False)
        '        Dim gaFound As Boolean = False
        '        Dim otherFound As Boolean = False
        '        For Each link2Row In link2_dt.Rows
        '            'links += 1
        '            If link2Row(1) = "C" Then
        '                Dim returnDate As Date
        '                Try
        '                    returnDate = link2Row(2)
        '                Catch ex As Exception
        '                    Continue For
        '                End Try

        '                If Format(returnDate, "yyyy-MM-dd") > Format(resultDate, "yyyy-MM-dd") Then
        '                    If link2Row(3) = 33 Then
        '                        gaFound = True
        '                        Exit For
        '                    Else
        '                        'get fee_cat from code returns
        '                        Dim feeCategory As Integer
        '                        feeCategory = GetSQLResults2("DebtRecovery", "select fee_category " & _
        '                                            " from codeReturns " & _
        '                                            " where _rowid = " & link2Row(3))
        '                        If feeCategory = 1 Then
        '                            gaFound = True
        '                            Exit For
        '                        Else
        '                            otherFound = True
        '                            Exit For
        '                        End If
        '                    End If
        '                End If
        '            End If
        '        Next
        '        If gaFound Then
        '            ga += 1
        '        ElseIf otherFound Then
        '            other += 1
        '        End If

        '        Dim link_dt As New DataTable
        '        Dim lastLinkDebtorID As Integer = 0
        '        LoadDataTable2("DebtRecovery", " select split_debt, split_costs, split_fees, split_van, split_other, date, D._rowID, P.status, P.amount, split_vat " & _
        '                       " from Payment P, debtor D" & _
        '                       " where P.debtorID = D._rowID" & _
        '                       " and D._rowid <> " & debtorID & _
        '                       " and D.linkID = " & linkID & _
        '                       " and P.status in ('R','W') " & _
        '                       " and P.date >= '" & Format(resultDate, "yyyy-MM-dd") & "'" & _
        '                       " order by D._rowID", link_dt, False)
        '        For Each linkRow In link_dt.Rows
        '            Dim linkDebtorID As Integer = linkRow(6)
        '            If linkDebtorID <> lastLinkDebtorID Then
        '                For idx = 0 To 100
        '                    casesSetMonthArray(idx) = False
        '                Next
        '            End If
        '            Dim paidDate As Date = linkRow(5)
        '            If Year(paidDate) < 2010 Then
        '                Continue For
        '            End If
        '            vat += linkRow(9)
        '            clientPaid = clientPaid + linkRow(0) + linkRow(1)
        '            feesPaid = feesPaid + linkRow(2) + linkRow(3) + linkRow(4)
        '            If linkRow(7) = "W" Then
        '                waiting += linkRow(8)
        '            End If
        '            Dim monthIDX As Integer = (Year(paidDate) - 2010) * 12
        '            monthIDX += Month(paidDate)
        '            paidMonthArray(monthIDX) += linkRow(8)
        '            feesMonthArray(monthIDX) += linkRow(2) + linkRow(3) + linkRow(4)
        '            If casesSetMonthArray(monthIDX) = False Then
        '                casesMonthArray(monthIDX) += 1
        '                casesSetMonthArray(monthIDX) = True
        '            End If
        '            lastLinkDebtorID = linkDebtorID
        '        Next

        '    End If

        'Next
        ''save last batch
        'upd_txt = "insert into TracePayments (trace_batchNumber, " & _
        ' "trace_supplierID, trace_ClientPaid, trace_FeesPaid," & _
        ' "trace_Waiting, trace_links, trace_ga, trace_visits,trace_other,trace_vat," & _
        '"Nov10,Dec10,Jan11,Feb11,Mar11,Apr11,May11,Jun11,Jul11," & _
        '             "Aug11,Sep11,Oct11,Nov11,Dec11," & _
        '               "Jan12,Feb12,Mar12,Apr12,May12,Jun12,Jul12," & _
        '             "Aug12,Sep12,Oct12,Nov12,Dec12," & _
        '               "Jan13,Feb13,Mar13,Apr13,May13,Jun13,Jul13," & _
        '             "Aug13,Sep13,Oct13,Nov13,Dec13," & _
        '             "Jan14,Feb14,Mar14,Apr14,May14,Jun14,Jul14," & _
        '             "Aug14,Sep14,Oct14,Nov14,Dec14" & ")" & _
        '  "values (" & batchNumber & "," & supplierID & "," & _
        '  clientPaid & "," & feesPaid & "," & waiting & "," & links & "," & _
        '  ga & "," & visits & "," & other & "," & vat & "," & _
        'paidMonthArray(11) & "," & paidMonthArray(12) & "," & paidMonthArray(13) & "," & paidMonthArray(14) & "," & _
        'paidMonthArray(15) & "," & paidMonthArray(16) & "," & paidMonthArray(17) & "," & paidMonthArray(18) & "," & _
        'paidMonthArray(19) & "," & paidMonthArray(20) & "," & paidMonthArray(21) & "," & paidMonthArray(22) & "," & _
        'paidMonthArray(23) & "," & paidMonthArray(24) & "," & paidMonthArray(25) & "," & paidMonthArray(26) & "," & _
        'paidMonthArray(27) & "," & paidMonthArray(28) & "," & paidMonthArray(29) & "," & paidMonthArray(30) & "," & _
        'paidMonthArray(31) & "," & paidMonthArray(32) & "," & paidMonthArray(33) & "," & paidMonthArray(34) & "," & _
        'paidMonthArray(35) & "," & paidMonthArray(36) & "," & paidMonthArray(37) & "," & paidMonthArray(38) & "," & _
        'paidMonthArray(39) & "," & paidMonthArray(40) & "," & paidMonthArray(41) & "," & paidMonthArray(42) & "," & _
        'paidMonthArray(43) & "," & paidMonthArray(44) & "," & paidMonthArray(45) & "," & paidMonthArray(46) & "," & _
        'paidMonthArray(47) & "," & paidMonthArray(48) & "," & paidMonthArray(49) & "," & paidMonthArray(50) & "," & _
        'paidMonthArray(51) & "," & paidMonthArray(52) & "," & paidMonthArray(53) & "," & paidMonthArray(54) & "," & _
        'paidMonthArray(55) & "," & paidMonthArray(56) & "," & paidMonthArray(57) & "," & paidMonthArray(58) & "," & _
        'paidMonthArray(59) & "," & paidMonthArray(60) & ")"
        'update_sql(upd_txt)
        'upd_txt = "insert into TracePayments2 (trace_batchNumber, " & _
        '            "trace_supplierID,Nov10,Dec10,Jan11,Feb11,Mar11,Apr11,May11,Jun11,Jul11," & _
        '            "Aug11,Sep11,Oct11,Nov11,Dec11," & _
        '              "Jan12,Feb12,Mar12,Apr12,May12,Jun12,Jul12," & _
        '            "Aug12,Sep12,Oct12,Nov12,Dec12," & _
        '              "Jan13,Feb13,Mar13,Apr13,May13,Jun13,Jul13," & _
        '            "Aug13,Sep13,Oct13,Nov13,Dec13," & _
        '            "Jan14,Feb14,Mar14,Apr14,May14,Jun14,Jul14," & _
        '            "Aug14,Sep14,Oct14,Nov14,Dec14" & ")" & _
        '             "values (" & batchNumber & "," & supplierID & "," & _
        '           feesMonthArray(11) & "," & feesMonthArray(12) & "," & feesMonthArray(13) & "," & feesMonthArray(14) & "," & _
        '          feesMonthArray(15) & "," & feesMonthArray(16) & "," & feesMonthArray(17) & "," & feesMonthArray(18) & "," & _
        '          feesMonthArray(19) & "," & feesMonthArray(20) & "," & feesMonthArray(21) & "," & feesMonthArray(22) & "," & _
        '          feesMonthArray(23) & "," & feesMonthArray(24) & "," & feesMonthArray(25) & "," & feesMonthArray(26) & "," & _
        '          feesMonthArray(27) & "," & feesMonthArray(28) & "," & feesMonthArray(29) & "," & feesMonthArray(30) & "," & _
        '          feesMonthArray(31) & "," & feesMonthArray(32) & "," & feesMonthArray(33) & "," & feesMonthArray(34) & "," & _
        '          feesMonthArray(35) & "," & feesMonthArray(36) & "," & feesMonthArray(37) & "," & feesMonthArray(38) & "," & _
        '          feesMonthArray(39) & "," & feesMonthArray(40) & "," & feesMonthArray(41) & "," & feesMonthArray(42) & "," & _
        '          feesMonthArray(43) & "," & feesMonthArray(44) & "," & feesMonthArray(45) & "," & feesMonthArray(46) & "," & _
        '          feesMonthArray(47) & "," & feesMonthArray(48) & "," & feesMonthArray(49) & "," & feesMonthArray(50) & "," & _
        '          feesMonthArray(51) & "," & feesMonthArray(52) & "," & feesMonthArray(53) & "," & feesMonthArray(54) & "," & _
        '          feesMonthArray(55) & "," & feesMonthArray(56) & "," & feesMonthArray(57) & "," & feesMonthArray(58) & "," & _
        '          feesMonthArray(59) & "," & feesMonthArray(60) & ")"
        'update_sql(upd_txt)
        'upd_txt = "insert into TracePayments3 (trace_batchNumber, " & _
        '           "trace_supplierID,Nov10,Dec10,Jan11,Feb11,Mar11,Apr11,May11,Jun11,Jul11," & _
        '           "Aug11,Sep11,Oct11,Nov11,Dec11," & _
        '             "Jan12,Feb12,Mar12,Apr12,May12,Jun12,Jul12," & _
        '           "Aug12,Sep12,Oct12,Nov12,Dec12," & _
        '             "Jan13,Feb13,Mar13,Apr13,May13,Jun13,Jul13," & _
        '           "Aug13,Sep13,Oct13,Nov13,Dec13," & _
        '           "Jan14,Feb14,Mar14,Apr14,May14,Jun14,Jul14," & _
        '           "Aug14,Sep14,Oct14,Nov14,Dec14" & ")" & _
        '            "values (" & batchNumber & "," & supplierID & "," & _
        '          casesMonthArray(11) & "," & casesMonthArray(12) & "," & casesMonthArray(13) & "," & casesMonthArray(14) & "," & _
        '         casesMonthArray(15) & "," & casesMonthArray(16) & "," & casesMonthArray(17) & "," & casesMonthArray(18) & "," & _
        '         casesMonthArray(19) & "," & casesMonthArray(20) & "," & casesMonthArray(21) & "," & casesMonthArray(22) & "," & _
        '         casesMonthArray(23) & "," & casesMonthArray(24) & "," & casesMonthArray(25) & "," & casesMonthArray(26) & "," & _
        '         casesMonthArray(27) & "," & casesMonthArray(28) & "," & casesMonthArray(29) & "," & casesMonthArray(30) & "," & _
        '         casesMonthArray(31) & "," & casesMonthArray(32) & "," & casesMonthArray(33) & "," & casesMonthArray(34) & "," & _
        '         casesMonthArray(35) & "," & casesMonthArray(36) & "," & casesMonthArray(37) & "," & casesMonthArray(38) & "," & _
        '         casesMonthArray(39) & "," & casesMonthArray(40) & "," & casesMonthArray(41) & "," & casesMonthArray(42) & "," & _
        '         casesMonthArray(43) & "," & casesMonthArray(44) & "," & casesMonthArray(45) & "," & casesMonthArray(46) & "," & _
        '         casesMonthArray(47) & "," & casesMonthArray(48) & "," & casesMonthArray(49) & "," & casesMonthArray(50) & "," & _
        '         casesMonthArray(51) & "," & casesMonthArray(52) & "," & casesMonthArray(53) & "," & casesMonthArray(54) & "," & _
        '         casesMonthArray(55) & "," & casesMonthArray(56) & "," & casesMonthArray(57) & "," & casesMonthArray(58) & "," & _
        '         casesMonthArray(59) & "," & casesMonthArray(60) & ")"
        'update_sql(upd_txt)
        'Me.Close()

    End Sub
End Class

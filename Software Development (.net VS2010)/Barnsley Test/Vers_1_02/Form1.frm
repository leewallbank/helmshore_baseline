VERSION 5.00
Begin VB.Form frmMain 
   Caption         =   "PreProcess Barnsley CTAX Cases Vers. 1.02"
   ClientHeight    =   4980
   ClientLeft      =   165
   ClientTop       =   555
   ClientWidth     =   6945
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   ScaleHeight     =   4980
   ScaleWidth      =   6945
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdViewErrorLog 
      Caption         =   "View &Error Log"
      Height          =   735
      Left            =   1920
      TabIndex        =   4
      Top             =   3000
      Width           =   3255
   End
   Begin VB.CommandButton cmdEndProgram 
      Caption         =   "&Quit Program"
      Height          =   735
      Left            =   1920
      TabIndex        =   3
      Top             =   3840
      Width           =   3255
   End
   Begin VB.CommandButton cmdProcessFile 
      Caption         =   "&Process File"
      Height          =   735
      Left            =   1920
      TabIndex        =   2
      Top             =   480
      Width           =   3255
   End
   Begin VB.CommandButton cmdViewProcessedFile 
      Caption         =   "View &Output File"
      Height          =   735
      Left            =   1920
      TabIndex        =   1
      Top             =   2160
      Width           =   3255
   End
   Begin VB.CommandButton cmdViewSourceFile 
      Caption         =   "View &Input File"
      Height          =   735
      Left            =   1920
      TabIndex        =   0
      Top             =   1320
      Width           =   3255
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' Vers. 1.01 06.05.2014 D.J.Dicks - modified due to the addition of phone numbers.
' Vers. 1.02 24.11.2015 D.J.Dicks - modified due to the addition of phone numbers that can appear 8 or 10 lines from the address
Option Explicit

 Dim strErrorReport As String

' common dialog to open browse

        Dim OpenFile As OPENFILENAME
        Dim SaveFile As SAVEFILENAME
        Dim intLineCounter As Long
        Dim strFileLine() As String
        Dim strDate As String
        Dim sFilter As String
        Dim lReturn As Long
        Dim strClientRef As String
        Dim strWarrantDate As String
        Dim strFirstLiableName As String
        Dim strForwardingAddress As String
        Dim strDebtAddress As String
        Dim strLiabilityOrderNumber As String
        Dim dblLiabilityOrderAmount As Double
        Dim dblOutStandingBalance  As Double
        Dim strComments As String
        Dim ForwardingAddressLine As Long
        Dim DebtAddressLine As Long
        Dim ChargePeriodLine As Long
        
       Private Declare Function GetOpenFileName Lib "comdlg32.dll" Alias _
         "GetOpenFileNameA" (pOpenfilename As OPENFILENAME) As Long

       Private Declare Function GetSaveFileName Lib "comdlg32.dll" Alias _
         "GetSaveFileNameA" (pSaveFileName As SAVEFILENAME) As Long
               
        Private Type OPENFILENAME
         lStructSize As Long
         hwndOwner As Long
         hInstance As Long
         lpstrFilter As String
         lpstrCustomFilter As String
         nMaxCustFilter As Long
         nFilterIndex As Long
         lpstrFile As String
         nMaxFile As Long
         lpstrFileTitle As String
         nMaxFileTitle As Long
         lpstrInitialDir As String
         lpstrTitle As String
         flags As Long
         nFileOffset As Integer
         nFileExtension As Integer
         lpstrDefExt As String
         lCustData As Long
         lpfnHook As Long
         lpTemplateName As String
       End Type
        
       Private Type SAVEFILENAME
         lStructSize As Long
         hwndOwner As Long
         hInstance As Long
         lpstrFilter As String
         lpstrCustomFilter As String
         nMaxCustFilter As Long
         nFilterIndex As Long
         lpstrFile As String
         nMaxFile As Long
         lpstrFileTitle As String
         nMaxFileTitle As Long
         lpstrInitialDir As String
         lpstrTitle As String
         flags As Long
         nFileOffset As Integer
         nFileExtension As Integer
         lpstrDefExt As String
         lCustData As Long
         lpfnHook As Long
         lpTemplateName As String
       End Type
Sub ProcessArray()
 Dim x As Long
Dim NewCase As String
Dim NewCaseHeader As String
Dim iOutPutFile As Integer
Dim NewCaseCounter As Integer
Dim strSaveFile As String
Dim PoundSign As Long
Dim LineLength As Long
Dim PhoneNumber1 As String
Dim PhoneNumber2 As String
Dim StartOfCurrentAddress As Long

On Error GoTo ErrHandler

NewCaseHeader = "Client Reference" & "|" _
        & "1st. Liable Name" & "|" _
        & "Forwarding Address" & "|" _
        & "Debt Address" & "|" _
        & "L/O Date" & "|" _
        & "L/O Number" & "|" _
        & "L/O Amount" & "|" _
        & "OutStanding Balance" & "|" _
        & "Comments" & "|" _
        & "PhoneNumber1" & "|" _
        & "PhoneNumber2" & "|" _

NewCaseCounter = 0

For x = 1 To intLineCounter

    '   Client Ref
    If Left$(strFileLine(x), 15) = "Account Number:" Or x = intLineCounter Then       'start of new case or end of last record in file
        NewCaseCounter = NewCaseCounter + 1
        'New Case - so write out last case
        If NewCaseCounter = 1 Then      'Write file Header
            SaveFile.lpstrFile = Left$(OpenFile.lpstrFile, InStr(1, OpenFile.lpstrFile, ".") - 1) & strDate & "PREPROCESSED" & "." & "txt"
            iOutPutFile = FreeFile()
            Open Trim(SaveFile.lpstrFile) For Output As iOutPutFile
            Print #iOutPutFile, NewCaseHeader
            Close iOutPutFile
            strClientRef = ""
            strFirstLiableName = ""
            strForwardingAddress = ""
            strDebtAddress = ""
            strWarrantDate = ""
            strLiabilityOrderNumber = ""
            dblLiabilityOrderAmount = 0
            dblOutStandingBalance = 0
            strComments = ""
        Else                            ' Write new case record
            If strClientRef <> "" Then  ' This will be the start of a new warrant date so don't write out a record.
                NewCase = strClientRef & "|" _
                        & strFirstLiableName & "|" _
                        & strForwardingAddress & "|" _
                        & strDebtAddress & "|" _
                        & strWarrantDate & "|" _
                        & strLiabilityOrderNumber & "|" _
                        & dblLiabilityOrderAmount & "|" _
                        & dblOutStandingBalance & "|" _
                        & strComments & "|" _
                        & PhoneNumber1 & "|" _
                        & PhoneNumber2
                        
                iOutPutFile = FreeFile()
                Open Trim(SaveFile.lpstrFile) For Append As iOutPutFile
                Print #iOutPutFile, NewCase
                Close iOutPutFile
                strClientRef = ""
                strFirstLiableName = ""
                strForwardingAddress = ""
                strDebtAddress = ""
                strWarrantDate = ""
                strLiabilityOrderNumber = ""
                dblLiabilityOrderAmount = 0
                dblOutStandingBalance = 0
                strComments = ""
                ' Vers. 1.02 24.11.2015 D.J.Dicks - modified due to the addition of phone numbers that can appear 8 or 10 lines from the address
                PhoneNumber1 = ""
                PhoneNumber2 = ""
            End If
        End If
        If Trim(Left$(strFileLine(x), 15)) = "Account Number:" Then
            strClientRef = Trim$(Mid$(strFileLine(x), 16, 255))
        Else
            strClientRef = ""
        End If
    Else
        ' Lead liable person
        If Trim(Left$(strFileLine(x), 10)) = "Lead Name:" Then
            strFirstLiableName = Trim$(Mid$(strFileLine(x), 11, 255))
        End If
        
        ' other liable person(s) - Note Steve Bates does not want this in
        'If Trim(Left$(strFileLine(x), 9)) = "Other(s):" Then
        '    strOtherNames = Trim$(Mid$(strFileLine(x), 10, 255))
        '    OtherNamesLine = x
        '    AdditionalLiablePersons = "Additional Liable Person(s) :"
        '    While Trim(Left$(strFileLine(OtherNamesLine + 1), 16)) <> "Current Address:"
        '        OtherNamesLine = OtherNamesLine + 1
        '        If Not Trim$(strFileLine(OtherNamesLine)) = "" Then
        '            AdditionalLiablePersons = AdditionalLiablePersons & " " & Trim$(strFileLine(OtherNamesLine)) & ","
        '        End If
        '    Wend
        'End If
        
        ' Current Address
        If Trim(Left$(strFileLine(x), 16)) = "Current Address:" Then
            ForwardingAddressLine = x
            StartOfCurrentAddress = x
            While Trim(Left$(strFileLine(ForwardingAddressLine + 1), 12)) <> "Debt Address"
                ForwardingAddressLine = ForwardingAddressLine + 1
                If Not Trim$(strFileLine(ForwardingAddressLine)) = "" Then
                    ' Strip off any " characters
                    strFileLine(ForwardingAddressLine) = Trim$(strFileLine(ForwardingAddressLine))
                    LineLength = Len(strFileLine(ForwardingAddressLine))
                    If Left$(strFileLine(ForwardingAddressLine), 1) = Chr$(34) Then
                        strFileLine(ForwardingAddressLine) = Mid$(strFileLine(ForwardingAddressLine), 2, LineLength)
                        LineLength = Len(strFileLine(ForwardingAddressLine))
                        If Right$(strFileLine(ForwardingAddressLine), 1) = Chr$(34) Then
                            strFileLine(ForwardingAddressLine) = Mid$(strFileLine(ForwardingAddressLine), 1, LineLength - 1)
                        End If
                    End If
                   'strForwardingAddress = strForwardingAddress & "," & Trim$(strFileLine(ForwardingAddressLine))
                    ' Vers. 1.01 06.05.2014 D.J.Dicks - modified due to the addition of phone numbers.
                    ' Vers. 1.02 24.11.2015 D.J.Dicks - modified due to the addition of phone numbers that can appear 8 or 10 lines from the address
                    If ForwardingAddressLine <> StartOfCurrentAddress + 7 And ForwardingAddressLine <> StartOfCurrentAddress + 9 And ForwardingAddressLine <> StartOfCurrentAddress + 8 And ForwardingAddressLine <> StartOfCurrentAddress + 10 Then
                        strForwardingAddress = IIf(strForwardingAddress = "", strForwardingAddress & Trim$(strFileLine(ForwardingAddressLine)), strForwardingAddress & "," & Trim$(strFileLine(ForwardingAddressLine)))
                    End If
                End If
            Wend
        End If
        
        ' Vers. 1.01 06.05.2014 D.J.Dicks - modified due to the addition of phone numbers.
        ' Vers. 1.02 24.11.2015 D.J.Dicks - modified due to the addition of phone numbers that can appear 8 or 10 lines from the address
         Select Case x
            Case StartOfCurrentAddress + 7, StartOfCurrentAddress + 8 'Telephone number
                    If Len(Trim(strFileLine(x))) > 0 Then
                        PhoneNumber1 = Trim(strFileLine(x))
                    End If
            Case StartOfCurrentAddress + 9, StartOfCurrentAddress + 10 'Telephone number
                    If Len(Trim(strFileLine(x))) > 0 Then
                        PhoneNumber2 = Trim(strFileLine(x))
                    End If
        End Select
        
        
        ' Debt Address
        If Trim(Left$(strFileLine(x), 12)) = "Debt Address" Then
            DebtAddressLine = x
            While Trim(Left$(strFileLine(DebtAddressLine + 1), 16)) <> "Unsummonsed Debt"
                DebtAddressLine = DebtAddressLine + 1
                If Not Trim$(strFileLine(DebtAddressLine)) = "" Then
                    ' Strip off any " characters
                    strFileLine(DebtAddressLine) = Trim$(strFileLine(DebtAddressLine))
                    LineLength = Len(strFileLine(DebtAddressLine))
                    If Left$(strFileLine(DebtAddressLine), 1) = Chr$(34) Then
                        strFileLine(DebtAddressLine) = Mid$(strFileLine(DebtAddressLine), 2, LineLength)
                        LineLength = Len(strFileLine(DebtAddressLine))
                        If Right$(strFileLine(DebtAddressLine), 1) = Chr$(34) Then
                            strFileLine(DebtAddressLine) = Mid$(strFileLine(DebtAddressLine), 1, LineLength - 1)
                        End If
                    End If
                    strDebtAddress = IIf(strDebtAddress = "", strDebtAddress & Trim$(strFileLine(DebtAddressLine)), strDebtAddress & "," & Trim$(strFileLine(DebtAddressLine)))
                End If
            Wend
        End If
        
        ' OutStanding amount
        If Trim(Left$(strFileLine(x), 18)) = "Outstanding Amount" Then
            PoundSign = InStr(strFileLine(x), "�")
            dblOutStandingBalance = CDbl(Mid$(strFileLine(x), PoundSign, 255))
        End If
        
        ' Summons/liability order number:
        If Trim(Left$(strFileLine(x), 31)) = "Summons/Liability Order Number:" Then
            strLiabilityOrderNumber = Trim$(Mid$(strFileLine(x), 32, 255))
        End If
        
        ' Hearing Date
        If Trim(Left$(strFileLine(x), 13)) = "Hearing Date:" Then
            strWarrantDate = Trim$(Mid$(strFileLine(x), 14, 255))
        End If
        
        ' L/O Amount Grant for
        If Trim(Left$(strFileLine(x), 19)) = "Amount Granted for:" Then
            dblLiabilityOrderAmount = CDbl(Trim$(Mid$(strFileLine(x), 20, 255)))
        End If
        
        ' Charge period and current debt
        If Trim(Left$(strFileLine(x), 37)) = "Charge period(s) and current debt(s):" Then
            ChargePeriodLine = x
            strComments = "Charge period(s): "
             While Trim(Left$(strFileLine(ChargePeriodLine + 1), 19)) <> "Total Amount Owing:"
                ChargePeriodLine = ChargePeriodLine + 1
                If Not Trim$(strFileLine(ChargePeriodLine)) = "" And Not Trim$(strFileLine(ChargePeriodLine)) = "-----------------------------------------------" Then
                    strComments = strComments & Trim$(strFileLine(ChargePeriodLine)) & ", "
                End If
            Wend
        End If
    End If
Next x

Exit Sub

ErrHandler:
    Call ErrReport("ProcessArray - Error on line " & x & " (" & strFileLine(x) & ") - ", Err.Description)
End Sub
Private Sub ErrReport(tSubName As String, tErrDescription As String)
    Dim ilogfile        As Integer
    Dim strErrorMessage As String
    
    
    DoEvents
    ' Report to screen
    strErrorMessage = Now & vbCr & tSubName _
             & "Note the error and report it to your system administrator." _
             & tErrDescription
             
    MsgBox strErrorMessage
             
    ' Report to log file
    ilogfile = FreeFile()
    strErrorReport = Left$(OpenFile.lpstrFile, InStr(1, OpenFile.lpstrFile, ".") - 1) & strDate & "ERROR" & "." & "log"
    Open strErrorReport For Append As ilogfile
    Print #ilogfile, Now, tErrDescription
    Close ilogfile
    
    cmdViewErrorLog.Enabled = True

    
End Sub
Private Sub LoadSettings()

    OpenFile.lpstrFile = GetSetting(APP_NAME, WORKSTATION_SECTION, "SourceFilePath", "c:\")

End Sub

Private Sub cmdEndProgram_Click()
' Save source directory path & database directory path to registry
    SaveSettings
End
End Sub

Private Sub cmdViewErrorLog_Click()
    Dim RetVal
    Dim strCurrentImportFilename, strWordpadFilename As String
    
    strWordpadFilename = ""
    strWordpadFilename = "C:\program files\windows nt\accessories\wordpad " & """" & strErrorReport & """"
        'strWordpadFilename = "./wordpad " & """" & strCurrentImportFilename & """"

    ' start up wordpad & display the current file being imported
    RetVal = Shell(strWordpadFilename, 1)    ' Run wordpad.
        
    Exit Sub
    
ErrHandler:
    Call ErrReport("View Error Log(cmdViewErrorLog_Click)", Err.Description)
    Exit Sub
End Sub

Private Sub cmdViewProcessedFile_Click()
On Error GoTo ErrHandler

    Dim RetVal
    Dim strCurrentImportFilename, strWordpadFilename As String
    
    strWordpadFilename = ""
    strWordpadFilename = "C:\program files\windows nt\accessories\wordpad " & """" & Trim(SaveFile.lpstrFile) & """"
        'strWordpadFilename = "./wordpad " & """" & strCurrentImportFilename & """"

    ' start up wordpad & display the current file being imported
    RetVal = Shell(strWordpadFilename, 1)    ' Run wordpad.
        
    Exit Sub
    
ErrHandler:
    Call ErrReport("View Processed File(cmdViewProcessedFile_Click)", Err.Description)
    Exit Sub
End Sub

Private Sub cmdProcessFile_Click()

On Error GoTo ErrHandler

    cmdViewErrorLog.Enabled = False
    OpenFile.lStructSize = Len(OpenFile)
    OpenFile.hwndOwner = frmMain.hWnd
    OpenFile.hInstance = App.hInstance
    sFilter = "Text Files (*.txt,*.lis*)" & Chr(0) & "*.txt" & Chr(0)
    OpenFile.lpstrFilter = sFilter
    OpenFile.nFilterIndex = 1
    OpenFile.lpstrFile = String(257, 0)
    OpenFile.nMaxFile = Len(OpenFile.lpstrFile) - 1
    OpenFile.lpstrFileTitle = OpenFile.lpstrFile
    OpenFile.nMaxFileTitle = OpenFile.nMaxFile
    OpenFile.lpstrInitialDir = GetSetting(APP_NAME, WORKSTATION_SECTION, "SourceFilePath", "c:\")
    'SaveFile.lpstrInitialDir = GetSetting(APP_NAME, WORKSTATION_SECTION, "DestinationFilePath", "c:\")
    OpenFile.lpstrTitle = "Rossendales(Allocations) - Create OneStep Query"
    OpenFile.flags = 0
    lReturn = GetOpenFileName(OpenFile)
    
    If lReturn = 1 Then
        SaveSetting APP_NAME, WORKSTATION_SECTION, "SourceFilePath", OpenFile.lpstrInitialDir

        Call ReadDataFile
        Call ProcessArray
    
        MsgBox "Processing completed"
    
        cmdViewSourceFile.Enabled = True
        cmdViewProcessedFile.Enabled = True

    End If
    Exit Sub
    
ErrHandler:
    Call ErrReport("Error Processing File(cmdProcessFile_Click)", Err.Description)
    Exit Sub
   
End Sub

Private Sub cmdViewSourceFile_Click()
On Error GoTo ErrHandler

    Dim RetVal
    Dim strCurrentImportFilename, strWordpadFilename As String
    
    strWordpadFilename = ""
    strWordpadFilename = "C:\program files\windows nt\accessories\wordpad " & """" & Trim(OpenFile.lpstrFile) & """"
        'strWordpadFilename = "./wordpad " & """" & strCurrentImportFilename & """"

    ' start up wordpad & display the current file being imported
    RetVal = Shell(strWordpadFilename, 1)    ' Run wordpad.
        
    Exit Sub
    
ErrHandler:
    Call ErrReport("Error Viewing Source File(cmdViewSourceFile_Click)", Err.Description)
    Exit Sub
End Sub

Private Sub Form_Load()
    cmdViewSourceFile.Enabled = False
    cmdViewProcessedFile.Enabled = False
    cmdEndProgram.Enabled = True
    cmdViewErrorLog.Enabled = False
    cmdProcessFile.Enabled = True
End Sub


Sub ReadDataFile()
' Reads the file line by line into an array fileLine(n)

On Error GoTo ErrHandler

Dim strInputLine As String
Dim intRecordCount As Long
Dim strRenameFilename As String
Dim strTextLine As String

    Open Trim(OpenFile.lpstrFile) For Input As #1 ' Open file.
    intRecordCount = 0 'initialise count of the records
    intLineCounter = 0
    Do While Not EOF(1) ' Loop until end of file.
        DoEvents
        Line Input #1, strTextLine ' Read line into variable.
        intLineCounter = intLineCounter + 1
        ReDim Preserve strFileLine(intLineCounter)
        strFileLine(intLineCounter) = strTextLine
    Loop
Close #1    ' Close file.

Exit Sub

ErrHandler:
    Call ErrReport("Error Reading Source File (ReadDataFile)", Err.Description)

End Sub


Private Sub SaveSettings()

    SaveSetting APP_NAME, WORKSTATION_SECTION, "SourceFilePath", OpenFile.lpstrInitialDir
    SaveSetting APP_NAME, WORKSTATION_SECTION, "DestinationFilePath", SaveFile.lpstrInitialDir
 
End Sub


﻿Imports CommonLibrary
Public Class Form1

    Dim fileCommercial, fileDomestic As String
    Dim ascii As New System.Text.ASCIIEncoding()

    Private UUData As New clsUUData
    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub createbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles createbtn.Click
        exitbtn.Enabled = False
        createbtn.Enabled = False
        fileCommercial = "HDRAGY023Pay" & Format(remit_dtp.Value, "yyyyMMdd") & vbNewLine
        fileDomestic = fileCommercial
        'get all csids for UU
        Dim ClientSchemeDatatable As New DataTable
        LoadDataTable("DebtRecovery", "SELECT _rowid, schemeID " & _
                                                "FROM clientScheme " & _
                                                "WHERE clientID in (1662)", ClientSchemeDatatable, False)
        Dim neg_countCommercial As Integer = 0
        Dim neg_countDomestic As Integer = 0
        Dim neg_amtCommercial As Integer = 0
        Dim neg_amtDomestic As Integer = 0
        Dim pos_countCommercial As Integer = 0
        Dim pos_amtCommercial As Integer = 0
        Dim pos_countDomestic As Integer = 0
        Dim pos_amtDomestic As Integer = 0
        Dim ClientSchemeRow As DataRow
        ProgressBar1.Style = ProgressBarStyle.Marquee

        For Each ClientSchemeRow In ClientSchemeDatatable.Rows
            Dim csID As Integer = ClientSchemeRow(0)
            Dim schemeID As Integer = ClientSchemeRow(1)
            'get nameLong from scheme to see if domestic or commercial
            Dim nameLong As String = ""
            Try
                nameLong = GetSQLResults("DebtRecovery", "SELECT nameLong " & _
                                                   "FROM scheme  " & _
                                                   "WHERE _rowid = " & schemeID)

            Catch ex As Exception
                nameLong = ""
            End Try
            nameLong = LCase(nameLong)
            If nameLong <> "domestic" And nameLong <> "commercial" Then
                MsgBox("Scheme " & schemeID & " long name is not commercial or domestic")
                Me.Close()
                Exit Sub
            End If
            'get all payments for remit date and csID
            Dim PaymentsDatatable As New DataTable
            LoadDataTable("DebtRecovery", "SELECT debtorID, split_debt, split_costs, amount_sourceID, date " & _
                                               "FROM Payment " & _
                                               "WHERE clientSchemeID = " & csID &
                                               " AND status = 'R'" &
                                              " AND status_date ='" & Format(remit_dtp.Value, "yyyy-MM-dd") & "'", PaymentsDatatable, False)
            Dim PaymentRow As DataRow
            For Each PaymentRow In PaymentsDatatable.Rows
                Application.DoEvents()
                Dim SourceID As Integer = PaymentRow.Item(3)
                'ignore direct payments
                Dim DirectPayment As String = UUData.GetSourceID(SourceID)
                If DirectPayment = "Y" Then
                    Continue For
                End If
                Dim pay_amt As Decimal = PaymentRow.Item(1) + PaymentRow.Item(2)
                If pay_amt = 0 Then
                    Continue For
                End If
                'get client ref from Debtor table
                Dim ClientRef As String = UUData.GetClientRef(PaymentRow.Item(0))

                'payment required as pence
                Dim pence As Integer = pay_amt * 100
                Dim sign As String = "+"
                Dim pay_date As Date = PaymentRow.Item(4)
                If nameLong = "commercial" Then
                    fileCommercial &= ClientRef.PadRight(33)
                    If pence < 0 Then
                        sign = "-"
                        neg_countCommercial += 1
                        neg_amtCommercial += pence
                        pence = pence * -1   'displays value without sign
                    Else
                        pos_countCommercial += 1
                        pos_amtCommercial += pence
                    End If

                    fileCommercial &= Format(pence, "000000000") & sign
                    fileCommercial &= Format(pay_date, "ddMMyyyy") & vbNewLine
                Else
                    fileDomestic &= ClientRef.PadRight(33)
                    If pence < 0 Then
                        sign = "-"
                        neg_countDomestic += 1
                        neg_amtDomestic += pence
                        pence = pence * -1   'displays value without sign
                    Else
                        pos_countDomestic += 1
                        pos_amtDomestic += pence
                    End If

                    fileDomestic &= Format(pence, "000000000") & sign
                    fileDomestic &= Format(pay_date, "ddMMyyyy") & vbNewLine
                End If
            Next
        Next
        'Next
        'write trailers
        neg_amtCommercial = neg_amtCommercial * -1  'displays as positive
        fileCommercial &= Format(pos_countCommercial, "00000") & Format(neg_countCommercial, "00000") & Format(pos_amtCommercial, "000000000") & Format(neg_amtCommercial, "000000000") & vbNewLine
        neg_amtDomestic = neg_amtDomestic * -1  'displays as positive
        fileDomestic &= Format(pos_countDomestic, "00000") & Format(neg_countDomestic, "00000") & Format(pos_amtDomestic, "000000000") & Format(neg_amtDomestic, "000000000") & vbNewLine
        'write out file
        '.FileName = Format(remit_dtp.Value, "yyyyMMdd") & "_AGY023pay.txt"
        With SaveFileDialog1
            .Title = "Save file"
            .Filter = "TXT files |*.txt"
            .DefaultExt = ".txt"
            .OverwritePrompt = True
            .FileName = "AGY023paydomestic.txt"
        End With
        Dim filepath As String = SaveFileDialog1.FileName
        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            'My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, fileCommercial, False, ascii)
            'SaveFileDialog1.FileName = Replace(SaveFileDialog1.FileName, "commercial", "domestic")
            My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, fileDomestic, False, ascii)
            MsgBox("report saved")
        Else
            MsgBox("report not saved")
        End If


        Me.Close()

    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub
End Class

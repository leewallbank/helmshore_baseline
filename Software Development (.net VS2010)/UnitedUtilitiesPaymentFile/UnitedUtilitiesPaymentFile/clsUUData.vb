﻿Imports CommonLibrary

Public Class clsUUData
    Public Function GetSourceID(ByVal SourceID As String) As String
        GetSourceID = GetSQLResults("DebtRecovery", "SELECT direct " & _
                                                    "FROM PaySource " & _
                                                    "WHERE _rowID = " & SourceID)
    End Function
    Public Function GetClientRef(ByVal DebtorID As String) As String
        GetClientRef = GetSQLResults("DebtRecovery", "SELECT client_ref " & _
                                                    "FROM Debtor " & _
                                                    "WHERE _rowID = " & DebtorID)
    End Function
End Class

Public Class Form1
    Dim record, file, record2, file2, file3, record3 As String
    Dim arr_table(30, 3)
    Dim hold_table(30, 5)
    Dim trace_table(30, 2)
    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub runbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles runbtn.Click
        'get all Census cases  - ADUR, Horsham AND Mid-Sussex
        exitbtn.Enabled = False
        runbtn.Enabled = False
       
        file = "DebtorID,Client,Scheme,Arrange Started,Arrange Broken,Debt balance,Arrange Amount,Arrange Interval,Agent,Date case loaded" & vbNewLine
        file2 = "DebtorID,Client,Scheme,Hold Started,Hold Ended,Client Request,Agent,Hold reason,Date case loaded" & vbNewLine
        file3 = "DebtorID,Client,Scheme,Trace Started,Trace Ended,Date case loaded" & vbNewLine
        'get list of client schemes

        param2 = "select _rowid, clientID, schemeID from clientScheme where branchID = 1 " &
            " and (clientID = 44 or clientID = 596 or clientID = 600 or clientID = 146)"
        Dim csid_dataset As DataSet = get_dataset("onestep", param2)
        If no_of_rows = 0 Then
            MsgBox("No client schemes found")
            Exit Sub
        End If
        Dim no_of_csids As Integer = no_of_rows
        Dim idx2 As Integer
        For idx2 = 0 To no_of_csids - 1
            'get client name
            Dim clID As Integer = csid_dataset.Tables(0).Rows(idx2).Item(1)
            Dim sch_ID As Integer = csid_dataset.Tables(0).Rows(idx2).Item(2)
            param2 = "select name from Client where _rowid = " & clID
            Dim cl_dataset As DataSet = get_dataset("onestep", param2)
            If no_of_rows = 0 Then
                MsgBox("Can't find client for " & clID)
                Exit Sub
            End If
            Dim cl_name As String = cl_dataset.Tables(0).Rows(0).Item(0)
            'get scheme name
            param2 = "select name from Scheme where _rowid = " & sch_ID
            Dim sch_dataset As DataSet = get_dataset("onestep", param2)
            If no_of_rows = 0 Then
                MsgBox("Can't find scheme for " & sch_ID)
                Exit Sub
            End If
            Dim sch_name As String = sch_dataset.Tables(0).Rows(0).Item(0)
            'get all cases 
            Dim csid As Integer = csid_dataset.Tables(0).Rows(idx2).Item(0)
            param2 = "select _rowid, status, debt_balance, arrange_amount, arrange_interval, _createdDate from Debtor" & _
            " where clientSchemeID = " & csid
            Dim debtor_dataset As DataSet = get_dataset("onestep", param2)
            If no_of_rows = 0 Then
                Continue For
            End If
            Dim debtor_rows As Integer = no_of_rows
            Dim idx3 As Integer
            Dim arr_idx, hold_idx, trace_idx As Integer
            For idx3 = 0 To debtor_rows - 1
                Try
                    ProgressBar1.Value = (idx3 / debtor_rows) * 100
                Catch ex As Exception

                End Try
                Application.DoEvents()
                'get all notes
                Dim debtorID As Integer = debtor_dataset.Tables(0).Rows(idx3).Item(0)
                Dim created_date As Date = debtor_dataset.Tables(0).Rows(idx3).Item(5)
                arr_idx = 0
                hold_idx = 0
                trace_idx = 0
                Dim idx4 As Integer
                For idx4 = 0 To 30
                    trace_table(idx4, 1) = Nothing
                    trace_table(idx4, 2) = Nothing
                Next
                'If debtorID = 5473481 Then
                '    debtorID = 5473481
                'End If

                param2 = "select type, text, _createdDate, _createdBy from Note where debtorID = " & debtorID & _
                " order by _rowid"
                Dim note_dataset As DataSet = get_dataset("onestep", param2)
                Dim note_rows As Integer = no_of_rows
                record = ""
                record2 = ""
                record3 = ""
                For idx4 = 0 To note_rows - 1
                    Dim note_type As String = note_dataset.Tables(0).Rows(idx4).Item(0)
                    If note_type = "Arrangement" Then
                        If arr_idx > 0 And arr_table(arr_idx, 2) = Nothing Then
                            arr_table(arr_idx, 2) = Format(note_dataset.Tables(0).Rows(idx4).Item(2), "dd/MM/yyyy")
                        End If
                        arr_idx += 1
                        arr_table(arr_idx, 1) = Format(note_dataset.Tables(0).Rows(idx4).Item(2), "dd/MM/yyyy")
                        arr_table(arr_idx, 2) = Nothing
                        arr_table(arr_idx, 3) = note_dataset.Tables(0).Rows(idx4).Item(3)
                    End If
                    If note_type = "Broken" Or note_type = "Clear arrange" Then
                        arr_table(arr_idx, 2) = note_dataset.Tables(0).Rows(idx4).Item(2)
                    End If
                    If note_type = "Trace" Then
                        Try
                            If trace_table(trace_idx, 1) = Format(note_dataset.Tables(0).Rows(idx4).Item(2), "dd/MM/yyyy") Then
                                trace_table(trace_idx, 2) = Format(note_dataset.Tables(0).Rows(idx4).Item(2), "dd/MM/yyyy")
                            End If
                        Catch ex As Exception

                        End Try

                        trace_idx += 1
                        trace_table(trace_idx, 1) = Format(note_dataset.Tables(0).Rows(idx4).Item(2), "dd/MM/yyyy")
                    End If
                    If note_type = "Off trace" Then
                        trace_table(trace_idx, 2) = Format(note_dataset.Tables(0).Rows(idx4).Item(2), "dd/MM/yyyy")
                    End If
                    If note_type = "Hold" Then
                        If Microsoft.VisualBasic.Left(note_dataset.Tables(0).Rows(idx4).Item(1), 3) <> "for" Then
                            Continue For
                        End If
                        If InStr(note_dataset.Tables(0).Rows(idx4).Item(1), "days.") = 0 Then
                            Continue For
                        End If
                        If hold_idx > 0 Then
                            If note_dataset.Tables(0).Rows(idx4).Item(2) < CDate(hold_table(hold_idx, 3)) Then
                                hold_table(hold_idx, 3) = note_dataset.Tables(0).Rows(idx4).Item(2)
                            End If
                        End If
                        hold_idx += 1
                        hold_table(hold_idx, 1) = Format(note_dataset.Tables(0).Rows(idx4).Item(2), "dd/MM/yyyy")
                        If InStr(note_dataset.Tables(0).Rows(idx4).Item(1), "client request") > 0 Then
                            hold_table(hold_idx, 2) = "Y"
                        Else
                            hold_table(hold_idx, 2) = "N"
                        End If
                        hold_table(hold_idx, 4) = note_dataset.Tables(0).Rows(idx4).Item(3)
                        hold_table(hold_idx, 5) = note_dataset.Tables(0).Rows(idx4).Item(1)
                        hold_table(hold_idx, 5) = Replace(hold_table(hold_idx, 5), ",", " ")
                        remove_chrs(hold_table(hold_idx, 5))
                        'get hold days
                        Dim end_idx As Integer = InStr(note_dataset.Tables(0).Rows(idx4).Item(1), "days")
                        Dim days_str As String = Mid(note_dataset.Tables(0).Rows(idx4).Item(1), 4, end_idx - 4)
                        Dim days As Integer
                        Try
                            days = days_str
                        Catch ex As Exception
                            days = Nothing
                        End Try
                        Dim hold_end_date As Date = Nothing
                        If days <> Nothing Then
                            hold_end_date = DateAdd(DateInterval.Day, days, hold_table(hold_idx, 1))
                        End If
                        hold_table(hold_idx, 3) = Format(hold_end_date, "dd/MM/yyyy")
                    End If
                    If note_type = "Off hold" Then
                        hold_table(hold_idx, 3) = Format(note_dataset.Tables(0).Rows(idx4).Item(2))
                    End If
                Next
                Dim debt_bal, arr_amt As Decimal
                Dim arr_interval As Integer
                Dim status As String = debtor_dataset.Tables(0).Rows(idx3).Item(1)
                If status = "A" And arr_idx > 0 Then
                    debt_bal = debtor_dataset.Tables(0).Rows(idx3).Item(2)
                    arr_amt = debtor_dataset.Tables(0).Rows(idx3).Item(3)
                    Try
                        arr_interval = debtor_dataset.Tables(0).Rows(idx3).Item(4)
                    Catch ex As Exception
                        arr_interval = Nothing
                    End Try

                End If
                For idx4 = arr_idx To 1 Step -1
                    Dim arr_start As Date = arr_table(idx4, 1)
                    Dim arr_broken As Date
                    Try
                        arr_broken = arr_table(idx4, 2)
                    Catch ex As Exception
                        arr_broken = Nothing
                    End Try
                    record = record & debtorID & "," & cl_name & "," & sch_name & "," & _
                            Format(arr_start, "dd/MM/yyyy") & ","
                    If arr_broken = Nothing Then
                        record = record & ","
                    Else
                        record = record & Format(arr_broken, "dd/MM/yyyy") & ","
                    End If
                    If idx4 = arr_idx And status = "A" Then
                        record = record & Format(debt_bal, "F") & "," & Format(arr_amt, "F") & _
                        "," & arr_interval
                    Else
                        record = record & ",,"
                    End If
                    record = record & "," & arr_table(idx4, 3) & "," & Format(created_date, "dd/MM/yyyy") & vbNewLine
                Next
                If record.Length > 0 Then
                    file = file & record
                End If

                For idx4 = hold_idx To 1 Step -1
                    Dim hold_start As Date = hold_table(idx4, 1)
                    Dim hold_end As Date
                    Try
                        hold_end = hold_table(idx4, 3)
                    Catch ex As Exception
                        hold_end = Nothing
                    End Try
                    record2 = record2 & debtorID & "," & cl_name & "," & sch_name & "," & _
                                           Format(hold_start, "dd/MM/yyyy") & ","
                    If hold_end = Nothing Then
                        record2 = record2 & ","
                    Else
                        record2 = record2 & Format(hold_end, "dd/MM/yyyy") & ","
                    End If
                    record2 = record2 & hold_table(idx4, 2) & "," & hold_table(idx4, 4) & "," & _
                    hold_table(idx4, 5) & "," & Format(created_date, "dd/MM/yyyy") & vbNewLine
                Next
                For idx4 = trace_idx To 1 Step -1
                    Dim trace_start_date As Date = trace_table(idx4, 1)
                    Dim trace_end_date As Date = trace_table(idx4, 2)
                    record3 = record3 & debtorID & "," & cl_name & "," & sch_name & "," & _
                                           Format(trace_start_date, "dd/MM/yyyy") & ","
                    If Format(trace_end_date, "yyyy") = 1 Then
                        record3 = record3 & ","
                    Else
                        record3 = record3 & Format(trace_end_date, "dd/MM/yyyy") & ","
                    End If
                    record3 = record3 & Format(created_date, "dd/MM/yyyy") & vbNewLine
                Next
                If record2.Length > 0 Then
                    file2 = file2 & record2
                End If
                If record3.Length > 0 Then
                    file3 = file3 & record3
                End If
            Next
        Next

        With SaveFileDialog1
            .Title = "Save file"
            .Filter = "TXT files |*.txt"
            .DefaultExt = ".txt"
            .OverwritePrompt = True
            .FileName = "census_arrangements.csv"
        End With
        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, file, False)
            Dim start_idx As Integer = InStr(SaveFileDialog1.FileName, "_")
            SaveFileDialog1.FileName = Microsoft.VisualBasic.Left(SaveFileDialog1.FileName, start_idx) & "holds.csv"
            My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, file2, False)
            SaveFileDialog1.FileName = Microsoft.VisualBasic.Left(SaveFileDialog1.FileName, start_idx) & "trace.csv"
            My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, file3, False)
            MsgBox("reports written")
        Else
            MsgBox("No reports written")
        End If
        Me.Close()
    End Sub
    Private Sub remove_chrs(ByRef in_field As String)
        Dim idx2 As Integer
        Dim out_field As String = ""
        For idx2 = 1 To Microsoft.VisualBasic.Len(in_field)
            If Mid(in_field, idx2, 1) <> Chr(9) And _
              Mid(in_field, idx2, 1) <> Chr(10) And _
            Mid(in_field, idx2, 1) <> Chr(13) Then
                out_field = out_field & Mid(in_field, idx2, 1)
            End If
        Next
        in_field = out_field
    End Sub
End Class

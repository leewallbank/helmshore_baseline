﻿Public Class Form1


    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        ZipFile("C:\AATemp\dialler.txt", "C:\AATemp\zip.zip")
        Me.Close()
    End Sub
    Public Sub CreateZipFile(ByVal Filename As String) 'create a new empty zip file
        'Create Header of Zip File
        Dim Encoder As New System.Text.ASCIIEncoding
        Dim Header As String = "PK" & Chr(5) & Chr(6)
        Header = Header.PadRight(22, Chr(0))

        'Save file - Make sure your file ends with .zip!
        My.Computer.FileSystem.WriteAllBytes(Filename, Encoder.GetBytes(Header), False)

    End Sub
    Public Sub ZipFile(ByVal Input As String, ByVal Filename As String)
        Dim Shell As New Shell32.Shell

        'Create our Zip File
        CreateZipFile(Filename) 'Create a new .zip file

        'Copy the file or folder to it
        Shell.NameSpace(Filename).CopyHere(Input)



    End Sub

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        Dim exePath As String = "C:\Program Files\7-Zip\7z.exe"
        Dim args As String = ""
        ' Dim returnCode As String
        'args = "7z x " + "C:\AATemp\archive.zip" + " o" & """C:\AATemp\"""

        'args = "e RA2138P.7z -oC:\AATemp\925"

        'args = "7z x RA2138P.7z"

        'extracted ok
        'args = "x C:\AATemp\RA2138P.7z -oc:\AATemp"

        'archive OK using folder name
        'args = "a C:\AATemp\RA2138P.7z C:\AATemp\*.pdf -psecret"

        'archive with password OK using filename - no spaces
        'args = "a C:\AATemp\RA2138P.7z C:\AATemp\RA2138P.pdf -psecret"

        'archive with filename with spaces - OK
        args = "a C:\AATemp\RA2138P.7z" & " " & """C:\AATemp\RA2138P A.pdf""" & " -psecret"

        Try
            Process.Start(exePath, args)
        Catch ex As Exception
            MsgBox(ex.Message.ToString)
        End Try


        Me.Close()
    End Sub
End Class

﻿Public Class Form1
    Dim start_date As Date
    Dim end_date As Date = CDate(Year(Now) & "," & Month(Now) & "," & 1)
    'Dim outfile As String = "Debtor,start date, freq,amount due,date due,paid" & vbNewLine


    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub runbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles runbtn.Click


    End Sub
    Private Sub run_report()
        Me.CollectArrangementsTableAdapter.DeleteQuery()

        start_date = DateAdd(DateInterval.Month, -1, end_date)
        'look at all arrangements set up last month for LSC branch 2
        'get all CSIDS for LSC branch 2
        param2 = " select _rowid from clientScheme where clientID = 1045 and branchID = 2"
        Dim cs_ds As DataSet = get_dataset("onestep", param2)
        Dim cs_rows As Integer = no_of_rows - 1
        For cs_idx = 0 To cs_rows
            'get a list of all cases
            Dim cs_ID As Integer = cs_ds.Tables(0).Rows(cs_idx).Item(0)
            param2 = " select _rowid, return_date, status_open_closed from Debtor where clientschemeID = " & cs_ID '&
            ' " and _rowid =5599706"
            Dim debt_ds As DataSet = get_dataset("onestep", param2)
            Dim debt_rows As Integer = no_of_rows - 1
            For debt_idx = 0 To debt_rows
                Try
                    ProgressBar1.Value = (debt_idx / debt_rows) * 100
                Catch ex As Exception

                End Try
                Application.DoEvents()
                Dim debtorID As Integer = debt_ds.Tables(0).Rows(debt_idx).Item(0)
                Dim return_date As Date
                Try
                    return_date = debt_ds.Tables(0).Rows(debt_idx).Item(1)
                Catch ex As Exception
                    return_date = Nothing
                End Try
                Dim status_OC As String = debt_ds.Tables(0).Rows(debt_idx).Item(2)
                If status_OC = "C" And
                    Format(return_date, "yyyy-MM-dd") < Format(start_date, "yyyy-MM-dd") Then
                    Continue For
                End If
                If status_OC = "O" Then
                    return_date = Nothing
                End If
                'see if case in arrangement last month
                param2 = "select type, text, _createdDate from Note where debtorID = " & debtorID &
                    " and (type = 'Broken' or type = 'Arrangement' or type = 'Clear arrange' or type = 'Defer arrange')" &
                    " order by _createdDate"
                Dim note_ds As DataSet = get_dataset("onestep", param2)
                Dim note_rows As Integer = no_of_rows - 1
                Dim in_arr As Boolean = False
                Dim due_date As Date = Nothing
                Dim note_date As Date = Nothing
                Dim note_due_date As Date
                Dim due_amt As Decimal = 0
                Dim due_freq As Integer = 0
                Dim init_due_date As Date = Nothing
                Dim init_note_date As Date = Nothing
                Dim init_amt As Decimal = 0
                Dim paid_amt As Decimal = 0
                For note_idx = 0 To note_rows
                    Dim note_type As String = note_ds.Tables(0).Rows(note_idx).Item(0)
                    note_date = note_ds.Tables(0).Rows(note_idx).Item(2)
                    If return_date <> Nothing Then
                        If Format(note_date, "yyyy-MM-dd") >= Format(return_date, "yyyy-MM-dd") Then
                            Exit For
                        End If
                    End If
                    paid_amt = 0
                    If Format(note_date, "yyyy-MM-dd") < Format(start_date, "yyyy-MM-dd") Then
                        If note_type = "Clear arrange" Or note_type = "Broken" Then
                            in_arr = False
                            due_date = Nothing
                            init_due_date = Nothing
                            Continue For
                        End If
                    End If
                    If note_type = "Clear arrange" Then
                        If Format(init_due_date, "yyyy-MM-dd") = Format(note_date, "yyyy-MM-dd") Then
                            init_due_date = Nothing
                            in_arr = False
                        End If
                        If Format(due_date, "yyyy-MM-dd") = Format(note_date, "yyyy-MM-dd") Then
                            due_date = Nothing
                            in_arr = False
                        End If
                    End If
                    If Format(note_date, "yyyy-MM-dd") >= Format(start_date, "yyyy-MM-dd") And in_arr Then
                        If Format(init_due_date, "MM yyyy") = Format(start_date, "MM yyyy") Then
                            'initial arrangement
                            Dim pay_end_date As Date = DateAdd(DateInterval.Day, 5, init_due_date)
                            param2 = "select amount, date, status, split_debt, split_costs from Payment where debtorID = " & debtorID &
                                " and (status = 'R' or status = 'W')"
                            Dim pay_ds As DataSet = get_dataset("onestep", param2)
                            Dim pay_rows As Integer = no_of_rows - 1
                            For pay_idx = 0 To pay_rows
                                Dim pay_date As Date = pay_ds.Tables(0).Rows(pay_idx).Item(1)
                                If Format(pay_date, "yyyy-MM-dd") < Format(init_note_date, "yyyy-MM-dd") Or
                                    Format(pay_date, "yyyy-MM-dd") > Format(pay_end_date, "yyyy-MM-dd") Then
                                    Continue For
                                End If
                                If pay_ds.Tables(0).Rows(pay_idx).Item(2) = "W" Then
                                    paid_amt += pay_ds.Tables(0).Rows(pay_idx).Item(0)
                                Else
                                    paid_amt = paid_amt + pay_ds.Tables(0).Rows(pay_idx).Item(3) + pay_ds.Tables(0).Rows(pay_idx).Item(4)
                                End If
                            Next
                            'outfile = outfile & debtorID & "," & Format(init_note_date, "dd/MM/yyyy") & ",," & init_amt & "," & init_due_date & "," & paid_amt & vbNewLine
                            Me.CollectArrangementsTableAdapter.InsertQuery(debtorID, init_note_date, 0, init_amt, init_due_date, paid_amt)
                            init_due_date = Nothing
                        End If
                        'due date
                        If due_date <> Nothing Then
                            If Format(due_date, "MM yyyy") < Format(start_date, "MM yyyy") Then
                                due_date = calc_due_date(due_date, due_freq)
                                If note_type = "Broken" And
                                    Format(due_date, "yyyy-MM-dd") >= Format(note_date, "yyyy-MM-dd") Then
                                    in_arr = False
                                    due_date = Nothing
                                End If
                                If in_arr Then
                                    While in_arr And Year(due_date) > 1900 And Format(due_date, "yyyy-MM-dd") < Format(end_date, "yyyy-MM-dd")
                                        If (note_type = "Broken" Or note_type = "Arrangement") And
                                            Format(due_date, "yyyy-MM-dd") >= Format(note_date, "yyyy-MM-dd") Then
                                            in_arr = False
                                            due_date = Nothing
                                            Exit While
                                        End If
                                        paid_amt = 0
                                        Dim pay_start_date As Date = due_date
                                        Dim pay_end_date As Date = DateAdd(DateInterval.Day, 4, due_date)
                                        If due_freq > 28 Then
                                            pay_start_date = CDate(Format(due_date, "yyyy") & "," & Format(due_date, "MM") & ",1")
                                            pay_end_date = DateAdd(DateInterval.Month, 1, pay_start_date)
                                        ElseIf due_freq > 13 Then
                                            pay_start_date = DateAdd(DateInterval.Day, -7, due_date)
                                            pay_end_date = DateAdd(DateInterval.Day, 7, due_date)
                                        ElseIf due_freq = 7 Then
                                            pay_start_date = DateAdd(DateInterval.Day, -3, due_date)
                                        End If

                                        param2 = "select amount, date, status, split_debt, split_costs from Payment where debtorID = " & debtorID &
                                            " and (status = 'R' or status = 'W')"
                                        Dim pay_ds As DataSet = get_dataset("onestep", param2)
                                        Dim pay_rows As Integer = no_of_rows - 1
                                        For pay_idx = 0 To pay_rows
                                            Dim pay_date As Date = pay_ds.Tables(0).Rows(pay_idx).Item(1)
                                            If Format(pay_date, "yyyy-MM-dd") < Format(pay_start_date, "yyyy-MM-dd") Or
                                                Format(pay_date, "yyyy-MM-dd") >= Format(pay_end_date, "yyyy-MM-dd") Then
                                                Continue For
                                            End If
                                            If pay_ds.Tables(0).Rows(pay_idx).Item(2) = "W" Then
                                                paid_amt += pay_ds.Tables(0).Rows(pay_idx).Item(0)
                                            Else
                                                paid_amt = paid_amt + pay_ds.Tables(0).Rows(pay_idx).Item(3) + pay_ds.Tables(0).Rows(pay_idx).Item(4)
                                            End If
                                        Next
                                        'outfile = outfile & debtorID & "," & Format(note_due_date, "dd/MM/yyyy") & "," & due_freq & "," & due_amt & "," & due_date & "," & paid_amt & vbNewLine
                                        Me.CollectArrangementsTableAdapter.InsertQuery(debtorID, note_due_date, due_freq, due_amt, due_date, paid_amt)
                                        due_date = DateAdd(DateInterval.Day, due_freq, due_date)
                                    End While

                                End If
                            End If
                        End If
                    End If
                    If Format(note_date, "yyyy-MM-dd") > Format(end_date, "yyyy-MM-dd") Then
                        Exit For
                    End If
                    Select Case note_type
                        Case "Arrangement"
                            in_arr = True
                            Dim note_text As String = LCase(note_ds.Tables(0).Rows(note_idx).Item(1))
                            Dim initial_idx As String = InStr(note_text, "initial payment of ")
                            If initial_idx > 0 And initial_idx < 20 Then
                                Dim dot_idx As Integer = InStr(note_text, ".")
                                init_amt = Mid(note_text, initial_idx + 18, dot_idx + 2 - initial_idx - 17)
                                init_due_date = CDate(Mid(note_text, dot_idx + 10, 13))
                                init_note_date = note_date
                                Dim follow_idx As Integer = InStr(note_text, "followed by")
                                If follow_idx > 0 Then
                                    note_due_date = note_date
                                    note_text = Microsoft.VisualBasic.Right(note_text, note_text.Length - follow_idx)
                                    dot_idx = InStr(note_text, ".")
                                    due_amt = Mid(note_text, 11, dot_idx + 2 - 11)
                                    Dim every_idx As Integer = InStr(note_text, "every")
                                    Dim days_idx As Integer = InStr(note_text, "days")
                                    due_freq = Mid(note_text, every_idx + 5, days_idx - every_idx - 5)
                                    Dim due_idx As Integer = InStr(note_text, "first payment due on")
                                    due_date = Mid(note_text, due_idx + 20, 13)
                                End If
                            Else
                                Dim whole_idx As Integer = InStr(note_text, "whole")
                                If whole_idx > 0 Then
                                    Dim due_idx As Integer = InStr(note_text, "due on")
                                    init_amt = Mid(note_text, 17, due_idx - 17)
                                    init_due_date = Mid(note_text, due_idx + 6, 12)
                                    init_note_date = note_date
                                    in_arr = True
                                Else
                                    note_due_date = note_date
                                    Dim every_idx As Integer = InStr(note_text, "every")
                                    If every_idx > 0 And every_idx < 15 Then
                                        Dim days_idx As Integer = InStr(note_text, "days")
                                        If days_idx > 0 Then
                                            due_freq = Mid(note_text, every_idx + 5, days_idx - every_idx - 5)
                                            Dim due_idx As Integer = InStr(note_text, "first payment due on")
                                            due_date = Mid(note_text, due_idx + 20, 13)
                                            due_amt = Microsoft.VisualBasic.Left(note_text, every_idx - 1)
                                        End If
                                    End If
                                End If

                            End If
                        Case "Broken"
                            in_arr = False
                        Case "Clear arrange"
                            in_arr = False
                        Case "Defer arrange"
                            Dim note_text As String = LCase(note_ds.Tables(0).Rows(note_idx).Item(1))
                            Dim days_idx As Integer = InStr(note_text, "days")
                            Dim defer_days As Integer
                            Try
                                defer_days = Mid(note_text, 4, days_idx - 4)
                            Catch ex As Exception
                                Continue For
                            End Try
                            due_date = DateAdd(DateInterval.Day, defer_days, due_date)
                    End Select
                Next
                'end of case notes
                If in_arr And Format(init_due_date, "MM yyyy") = Format(start_date, "MM yyyy") Then
                    'initial arrangement
                    Dim pay_end_date As Date = DateAdd(DateInterval.Day, 4, init_due_date)
                    param2 = "select amount, date, status, split_debt, split_costs from Payment where debtorID = " & debtorID &
                        " and (status = 'R' or status = 'W')"
                    Dim pay_ds As DataSet = get_dataset("onestep", param2)
                    Dim pay_rows As Integer = no_of_rows - 1
                    paid_amt = 0
                    For pay_idx = 0 To pay_rows
                        Dim pay_date As Date = pay_ds.Tables(0).Rows(pay_idx).Item(1)
                        If Format(pay_date, "yyyy-MM-dd") < Format(init_note_date, "yyyy-MM-dd") Or
                            Format(pay_date, "yyyy-MM-dd") >= Format(pay_end_date, "yyyy-MM-dd") Then
                            Continue For
                        End If
                        If pay_ds.Tables(0).Rows(pay_idx).Item(2) = "W" Then
                            paid_amt += pay_ds.Tables(0).Rows(pay_idx).Item(0)
                        Else
                            paid_amt = paid_amt + pay_ds.Tables(0).Rows(pay_idx).Item(3) + pay_ds.Tables(0).Rows(pay_idx).Item(4)
                        End If
                    Next
                    'outfile = outfile & debtorID & "," & Format(init_note_date, "dd/MM/yyyy") & ",," & init_amt & "," & init_due_date & "," & paid_amt & vbNewLine
                    Me.CollectArrangementsTableAdapter.InsertQuery(debtorID, init_note_date, 0, init_amt, init_due_date, paid_amt)
                    init_due_date = Nothing
                End If
                If in_arr And due_date <> Nothing And due_date > CDate("jan 1 1900") Then
                    If Format(due_date, "MM yyyy") < Format(start_date, "MM yyyy") Then
                        due_date = calc_due_date(due_date, due_freq)
                        While in_arr And Format(due_date, "yyyy-MM-dd") < Format(end_date, "yyyy-MM-dd")
                            paid_amt = 0
                            Dim pay_start_date As Date = due_date
                            Dim pay_end_date As Date = DateAdd(DateInterval.Day, 5, due_date)
                            If due_freq > 28 Then
                                pay_start_date = CDate(Format(due_date, "yyyy") & "," & Format(due_date, "MM") & ",1")
                                pay_end_date = DateAdd(DateInterval.Month, 1, pay_start_date)
                            ElseIf due_freq > 13 Then
                                pay_start_date = DateAdd(DateInterval.Day, -7, due_date)
                                pay_end_date = DateAdd(DateInterval.Day, 7, due_date)
                            ElseIf due_freq = 7 Then
                                pay_start_date = DateAdd(DateInterval.Day, -3, due_date)
                            End If
                            param2 = "select amount, date, status, split_debt, split_costs from Payment where debtorID = " & debtorID &
                                " and (status = 'R' or status = 'W')"
                            Dim pay_ds As DataSet = get_dataset("onestep", param2)
                            Dim pay_rows As Integer = no_of_rows - 1
                            For pay_idx = 0 To pay_rows
                                Dim pay_date As Date = pay_ds.Tables(0).Rows(pay_idx).Item(1)
                                If Format(pay_date, "yyyy-MM-dd") < Format(pay_start_date, "yyyy-MM-dd") Or
                                    Format(pay_date, "yyyy-MM-dd") >= Format(pay_end_date, "yyyy-MM-dd") Then
                                    Continue For
                                End If
                                If pay_ds.Tables(0).Rows(pay_idx).Item(2) = "W" Then
                                    paid_amt += pay_ds.Tables(0).Rows(pay_idx).Item(0)
                                Else
                                    paid_amt = paid_amt + pay_ds.Tables(0).Rows(pay_idx).Item(3) + pay_ds.Tables(0).Rows(pay_idx).Item(4)
                                End If
                            Next
                            'outfile = outfile & debtorID & "," & Format(note_due_date, "dd/MM/yyyy") & "," & due_freq & "," & due_amt & "," & due_date & "," & paid_amt & vbNewLine
                            Me.CollectArrangementsTableAdapter.InsertQuery(debtorID, note_due_date, due_freq, due_amt, due_date, paid_amt)
                            due_date = DateAdd(DateInterval.Day, due_freq, due_date)
                        End While
                    End If
                End If
            Next
        Next
        'My.Computer.FileSystem.WriteAllText("H:\temp\arr_file.txt", outfile, False)
        'MsgBox("file written")
        Me.Close()
    End Sub
    Function calc_due_date(ByVal due_date As Date, ByVal due_freq As Integer) As Date
        Dim new_due_date As Date = due_date
        While Format(new_due_date, "yyyy-MM-dd") < Format(start_date, "yyyy-MM-dd")
            If due_freq > 28 Then
                new_due_date = DateAdd(DateInterval.Month, 1, new_due_date)
            Else
                new_due_date = DateAdd(DateInterval.Day, due_freq, new_due_date)
            End If
            If due_freq = 0 Then
                'MsgBox("Freq = 0")
                Return (due_date)
            End If
        End While
        Return (new_due_date)
    End Function

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        run_report()

    End Sub
End Class

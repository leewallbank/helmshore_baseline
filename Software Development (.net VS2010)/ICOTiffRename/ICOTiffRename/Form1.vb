﻿Imports CommonLibrary
Imports System.IO
Imports System.Configuration



Public Class Form1
    Private InputFilePath As String, FileName As String, FileExt As String


    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub convertbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles convertbtn.Click
        Dim file_name As String = ""
        Dim new_filename As String = ""
        Dim last_file_name As String = ""
        convertbtn.Enabled = False
        exitbtn.Enabled = False
        Dim debtor As Integer
        Dim pageNo As Integer = 0
        Dim FileDialog As New OpenFileDialog
        If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

        InputFilePath = Path.GetDirectoryName(FileDialog.FileName) & "\"

        FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
        FileExt = Path.GetExtension(FileDialog.FileName)
        Dim excel_file_contents(,) As String = InputFromExcel(FileDialog.FileName)
        Dim tif_filename As String
        Dim maxRow As Integer = UBound(excel_file_contents)
        'now get case number and rename files
        For rowIDX = 1 To maxRow
            Try
                debtor = excel_file_contents(rowIDX, 1)
            Catch ex As Exception
                Continue For
            End Try
            If debtor = 0 Then
                Continue For
            End If
            pageNo += 1
           
            tif_filename = InputFilePath & FileName & "_" & Format(pageNo, "0000") & ".tif"
            'T55417 get new page number from xls
            Dim newPageNo As Integer = 0
            Try
                newPageNo = excel_file_contents(rowIDX, 3)
            Catch ex As Exception

            End Try
            'rename tif file to new name
            ' Dim tif_new_file_name As String = InputFilePath & debtor & "-doc-page001.tif"
            Dim tif_new_file_name As String = InputFilePath & debtor & "-doc-page00" & newPageNo & ".tif"
            Try
                Rename(tif_filename, tif_new_file_name)
            Catch ex As Exception
                MsgBox("Unable to rename file " & tif_filename & vbNewLine & _
                       ex.Message)
                Me.Close()
                Exit Sub
            End Try
        Next

        MsgBox("Completed")
        Me.Close()
    End Sub




End Class

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.remit_dtp = New System.Windows.Forms.DateTimePicker()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.DD_override_tbox = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.exitbtn = New System.Windows.Forms.Button()
        Me.runbtn = New System.Windows.Forms.Button()
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
        Me.invref_tbox = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Late_override_tbox = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.dis_override_tbox = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'remit_dtp
        '
        Me.remit_dtp.Location = New System.Drawing.Point(98, 25)
        Me.remit_dtp.Name = "remit_dtp"
        Me.remit_dtp.Size = New System.Drawing.Size(126, 20)
        Me.remit_dtp.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(124, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(60, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Remit Date"
        '
        'DD_override_tbox
        '
        Me.DD_override_tbox.Location = New System.Drawing.Point(109, 97)
        Me.DD_override_tbox.Name = "DD_override_tbox"
        Me.DD_override_tbox.Size = New System.Drawing.Size(100, 20)
        Me.DD_override_tbox.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(83, 81)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(161, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Direct Debit Paid Direct Override"
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(276, 426)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 4
        Me.exitbtn.Text = "Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'runbtn
        '
        Me.runbtn.Location = New System.Drawing.Point(109, 366)
        Me.runbtn.Name = "runbtn"
        Me.runbtn.Size = New System.Drawing.Size(75, 23)
        Me.runbtn.TabIndex = 5
        Me.runbtn.Text = "Run Reports"
        Me.runbtn.UseVisualStyleBackColor = True
        '
        'invref_tbox
        '
        Me.invref_tbox.Location = New System.Drawing.Point(109, 309)
        Me.invref_tbox.Name = "invref_tbox"
        Me.invref_tbox.Size = New System.Drawing.Size(100, 20)
        Me.invref_tbox.TabIndex = 6
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(114, 293)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(95, 13)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Invoice Reference"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(83, 150)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(167, 13)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "Late Licence Paid Direct Override"
        '
        'Late_override_tbox
        '
        Me.Late_override_tbox.Location = New System.Drawing.Point(109, 166)
        Me.Late_override_tbox.Name = "Late_override_tbox"
        Me.Late_override_tbox.Size = New System.Drawing.Size(100, 20)
        Me.Late_override_tbox.TabIndex = 9
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(79, 230)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(165, 13)
        Me.Label5.TabIndex = 10
        Me.Label5.Text = "Dishonoured Paid Direct Override"
        '
        'dis_override_tbox
        '
        Me.dis_override_tbox.Location = New System.Drawing.Point(109, 246)
        Me.dis_override_tbox.Name = "dis_override_tbox"
        Me.dis_override_tbox.Size = New System.Drawing.Size(100, 20)
        Me.dis_override_tbox.TabIndex = 11
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(376, 481)
        Me.Controls.Add(Me.dis_override_tbox)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Late_override_tbox)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.invref_tbox)
        Me.Controls.Add(Me.runbtn)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.DD_override_tbox)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.remit_dtp)
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "DVLA Invoices"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents remit_dtp As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents DD_override_tbox As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents runbtn As System.Windows.Forms.Button
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
    Friend WithEvents invref_tbox As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Late_override_tbox As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents dis_override_tbox As System.Windows.Forms.TextBox
End Class

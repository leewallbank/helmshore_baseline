﻿Imports CommonLibrary
Imports System.IO

Public Class frmMain

    Private InputFilePath As String, FileName As String, FileExt As String

    Private Sub btnProcessFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcessFile.Click
        Try
            Dim apr1_2013 As Date = CDate("Apr 1, 2013 00:00:00")
            Dim FileDialog As New OpenFileDialog
            Dim OutputFileEarly As String = ""
            Dim OutputFileLate As String = OutputFileEarly
            Dim EarlyCases As Integer = 0
            Dim LateCases As Integer = 0
            Dim InputLineArray() As String
            If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

            InputFilePath = Path.GetDirectoryName(FileDialog.FileName)
            InputFilePath &= "\"
            FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
            FileExt = Path.GetExtension(FileDialog.FileName)

            Dim FileContents() As String = System.IO.File.ReadAllLines(FileDialog.FileName)
            ProgressBar.Maximum = UBound(FileContents)
            Dim rowMax As Integer = UBound(FileContents)

            For rowIDX = 0 To rowMax
                InputLineArray = FileContents(rowIDX).Split(",")
                Dim LODate As Date
                Dim LOdateStr As String = InputLineArray(1)
                'remove quotes
                Dim LOIdx As Integer
                Dim newLOdateStr As String = ""
                For LOIdx = 1 To LOdateStr.Length
                    Dim chct As String = Mid(LOdateStr, LOIdx, 1)
                    If chct <> Chr(34) Then
                        newLOdateStr &= Mid(LOdateStr, LOIdx, 1)
                    End If
                Next

                Try
                    LODate = CDate(newLOdateStr)
                Catch ex As Exception
                    Continue For
                End Try

                If Format(LODate, "yyyy-MM-dd") < Format(apr1_2013, "yyyy-MM-dd") Then
                    OutputFileEarly &= FileContents(rowIDX) & vbNewLine
                    EarlyCases += 1
                Else
                    OutputFileLate &= FileContents(rowIDX) & vbNewLine
                    LateCases += 1
                End If

            Next
            If LateCases > 0 Then
                WriteFile(InputFilePath & FileName & "_LODateLate_standard_4357.txt", OutputFileLate)
                btnViewOutputFileHigh.Enabled = True
            End If
            If EarlyCases > 0 Then
                WriteFile(InputFilePath & FileName & "_LODateEarly_recycled_4356.txt", OutputFileEarly)
                btnViewOutputFileLow.Enabled = True
            End If
            MsgBox("Late Balance Cases = " & LateCases & vbNewLine & _
                   "Early  Balance Cases = " & EarlyCases)
            btnViewInputFile.Enabled = True

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewInputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewInputFile.Click
        Try

            If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start("wordpad.exe", """" & InputFilePath & FileName & FileExt & """")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewOutputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewOutputFileHigh.Click
        Try

            If File.Exists(InputFilePath & FileName & "_LODateLate_standard_4357.txt") Then System.Diagnostics.Process.Start("wordpad.exe", """" & InputFilePath & FileName & "_LODateLate_standard_4357.txt" & """")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Function Split(ByVal Expression As String, ByVal Delimiter As String, ByVal Qualifier As String) As String()
        Dim ReturnValues() As String = Nothing
        Try

            Dim QualifierState As Boolean = False
            Dim StartIndex As Integer = 0
            Dim Values As New System.Collections.ArrayList

            For CharIndex As Integer = 0 To Expression.Length - 1
                If Not Qualifier Is Nothing AndAlso String.Compare(Expression.Substring(CharIndex, Qualifier.Length), Qualifier) = 0 Then
                    QualifierState = Not QualifierState
                ElseIf Not QualifierState AndAlso Not Delimiter Is Nothing AndAlso String.Compare(Expression.Substring(CharIndex, Delimiter.Length), Delimiter) = 0 Then
                    Values.Add(Expression.Substring(StartIndex, CharIndex - StartIndex))
                    StartIndex = CharIndex + 1
                End If
            Next CharIndex

            If StartIndex < Expression.Length Then
                Values.Add(Expression.Substring(StartIndex, Expression.Length - StartIndex))
            End If

            ReDim ReturnValues(Values.Count - 1)
            Values.CopyTo(ReturnValues)


        Catch ex As Exception
            HandleException(ex)
        End Try

        Return ReturnValues
    End Function

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnViewOutputFileLow.Click
        Try

            If File.Exists(InputFilePath & FileName & "_LODateEarly_recycled_4356.txt") Then System.Diagnostics.Process.Start("wordpad.exe", """" & InputFilePath & FileName & "_LODateEarly_recycled_4356.txt" & """")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub
End Class

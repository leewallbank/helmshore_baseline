Public Class displayfrm

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub findbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles findbtn.Click
        disable_buttons()
        get_letter = True
        start_search()
        enable_buttons()
    End Sub
    Private Sub start_search()
        ErrorProvider1.SetError(recvd_fromcombobox, "")
        ErrorProvider1.SetError(cat_combobox, "")
        ErrorProvider1.SetError(against_combobox, "")
        ErrorProvider1.SetError(agentcombobox, "")
        ErrorProvider1.SetError(stage_combobox, "")
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5

        Dim resp
        search_csid_no = 0
        If allrbtn.Checked = True Then
            search_no = 1
        ElseIf caserbtn.Checked = True Then
            Try
                resp = InputBox("Enter case number")
                If resp Is "" Then
                    ProgressBar1.Visible = False
                    Exit Sub
                End If
            Catch ex As Exception
                MsgBox("Case number is not numeric")
                ProgressBar1.Visible = False
                Exit Sub
            End Try
            case_no = resp
            search_no = 2
        ElseIf clrbtn.Checked = True Then
            disp_clientfrm.ShowDialog()
            If cl_no = 0 Then
                MessageBox.Show("No client has been selected")
                ProgressBar1.Visible = False
                Exit Sub
            Else
                'select scheme if required
                If MsgBox("Select all schemes?", MsgBoxStyle.YesNo, "Client Scheme selection") = MsgBoxResult.No Then
                    schform.ShowDialog()
                End If
            End If
            search_no = 3
            'ElseIf agentrbtn.Checked = True Then
            '    agent_dispfrm.ShowDialog()
            '    If agent1 = -1 Then
            '        MessageBox.Show("No agent has been selected")
            '        ProgressBar1.Visible = False
            '        Exit Sub
            '    End If
            '    search_no = 4

            'ElseIf deptrbtn.Checked = True Then
            '    Try
            '        dept_code = InputBox("Enter department number")
            '    Catch ex As Exception
            '        MsgBox("Department number is not numeric")
            '        Exit Sub
            '    End Try
            '    search_no = 5
        ElseIf invrbtn.Checked = True Then
            investdispfrm.ShowDialog()
            If inv_code = 0 Then
                MessageBox.Show("No investigator has been selected")
                ProgressBar1.Visible = False
                Exit Sub
            End If
            search_no = 7
        End If
        founded_search = "A"
        If openrbtn.Checked = True Then
            founded_search = "U"
        ElseIf Foundedrbtn.Checked = True Then
            founded_search = "Y"
        ElseIf unfoundedrbtn.Checked = True Then
            founded_search = "N"
        End If

        held_search = "A"
        'If heldrbtn.Checked = True Then
        '    held_search = "Y"
        'ElseIf notheldrbtn.Checked = True Then
        '    held_search = "N"
        'End If

        completed_search = "A"
        If completedrbtn.Checked = True Then
            completed_search = "Y"
        ElseIf notcomprbtn.Checked = True Then
            completed_search = "N"
        End If
        selected_comp_no = 0
        displayfrm2.ShowDialog()
        ProgressBar1.Visible = False
    End Sub


    Private Sub dispbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dispbtn.Click
        Dim resp
        Try
            resp = InputBox("Enter complaint number")
            If resp Is "" Then
                Exit Sub
            End If
        Catch ex As Exception
            MsgBox("Complaint number not numeric")
            Exit Sub
        End Try
        If Len(resp) = 0 Then
            MsgBox("Complaint number not numeric")
            Exit Sub
        End If
        comp_no = resp
        If env_str = "Prod" And comp_no <= 160 Then
            MsgBox("First complaint in Production is 161")
            Exit Sub
        End If
        displayfrm3.ShowDialog()
    End Sub

    Private Sub displayfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim idx As Integer
        'company changes
        search_branch_no = -1
        branch_cbox.SelectedIndex = -1
        cmpny_cbox.Items.Clear()
        param2 = "select cmpny_code, cmpny_name from Complaint_companies" & _
        " order by cmpny_code"
        Dim cmpny_ds As DataSet = get_dataset("Complaints", param2)
        Dim cmpnyRow As DataRow
        For Each cmpnyRow In cmpny_ds.Tables(0).Rows
            cmpny_cbox.Items.Add(cmpnyRow(1))
            If cmpnyRow(0) = cmpny_no Then
                cmpny_cbox.SelectedItem = cmpnyRow(1)
            End If
        Next
        If super_user Then
            cmpny_cbox.Enabled = True
            cmpny_cbox.Items.Add("ALL")
        Else
            cmpny_cbox.Enabled = False
        End If
        search_recvd_from = -1
        cat_text = ""
        cat_combobox.SelectedIndex = -1
        category_combobox.SelectedIndex = 0
        'replace table adapter
        'Me.Received_fromTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Received_from)
        recvd_fromcombobox.Items.Clear()
        param2 = "select recvd_text from Received_from " & _
        " order by recvd_text"
        Dim recvd_ds As DataSet = get_dataset("complaints", param2)
        For Each row In recvd_ds.Tables(0).Rows
            recvd_fromcombobox.Items.Add(row(0))
        Next
        recvd_fromcombobox.SelectedIndex = -1

        'Me.Complaint_branchesTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Complaint_branches)
        search_branch_no = -1
        branch_cbox.SelectedIndex = -1
        against_combobox.Items.Clear()
        param2 = "select branch_name, branch_code from Complaint_branches " & _
        " where branch_cmpny_no = " & cmpny_no & _
               " order by branch_name"
        Dim branch_ds As DataSet = get_dataset("Complaints", param2)
        Dim branchRow, againstRow As DataRow
        branch_cbox.Items.Clear()
        For Each branchRow In branch_ds.Tables(0).Rows
            branch_cbox.Items.Add(branchRow(0))
            Dim branchCode As Integer = branchRow(1)
            param2 = "select against_code, against_text from Complaint_against" & _
            " where against_branch_no = " & branchCode
            Dim against_ds As DataSet = get_dataset("Complaints", param2)
            For Each againstRow In against_ds.Tables(0).Rows
                against_combobox.Items.Add(againstRow(1))
            Next
        Next

        search_dept = ""
        agentcombobox.Items.Clear()
        'Me.Complaint_againstTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Complaint_against)

        debt_typecombobox.SelectedIndex = 0
        against_combobox.SelectedIndex = -1
        Dim idx2 As Integer = 0
        If cmpny_no > 0 Then
            param2 = "select agnt_code, agnt_name from Complaint_agents" & _
            " order by agnt_name"
            Dim agnt_ds As DataSet = get_dataset("Complaints", param2)
            ReDim search_bailiff_table(no_of_rows, 2)
            For idx2 = 0 To no_of_rows - 1
                search_bailiff_table(idx2, 2) = agnt_ds.Tables(0).Rows(idx2).Item(1)
                search_bailiff_table(idx2, 1) = agnt_ds.Tables(0).Rows(idx2).Item(0)
                agentcombobox.Items.Add(search_bailiff_table(idx2, 2))
            Next
        Else
            param1 = "onestep"
            param2 = "select _rowid, name_fore, name_sur, status from Bailiff" & _
            " where add_mobile_name<>'none' " & _
            " order by name_sur"
            Dim bail_dataset As DataSet = get_dataset(param1, param2)
            If no_of_rows = 1 Then
                Exit Sub
            End If
            ReDim search_bailiff_table(no_of_rows, 2)
            Dim bailiff_name As String
            For idx = 0 To no_of_rows - 1
                Try
                    bailiff_name = Trim(bail_dataset.Tables(0).Rows(idx).Item(2))
                Catch ex As Exception
                    Continue For
                End Try
                If bailiff_name.Length = 0 Then
                    Continue For
                End If
                idx2 += 1
                Dim forename As String
                Try
                    forename = Trim(bail_dataset.Tables(0).Rows(idx).Item(1))
                Catch ex As Exception
                    forename = ""
                End Try
                bailiff_name = bailiff_name & ", " & forename

                If bail_dataset.Tables(0).Rows(idx).Item(3) = "C" Then
                    bailiff_name = bailiff_name & " (C)"
                End If
                search_bailiff_table(idx2, 2) = Trim(bailiff_name)
                search_bailiff_table(idx2, 1) = bail_dataset.Tables(0).Rows(idx).Item(0)
            Next
            agent_rows = idx2

            For idx = 1 To idx2
                agentcombobox.Items.Add(search_bailiff_table(idx, 2))
            Next
            agentcombobox.Items.Add("Various")
        End If
        agentcombobox.SelectedIndex = -1
        agentcombobox.Text = " "
        agent1 = -1
        agent2 = -1

        'add receipt type combobox
        'displayfrm3.Receipt_typeTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Receipt_type)
        receipt_typecombobox.Items.Clear()
        param2 = "select recpt_text from Receipt_type " & _
        " order by recpt_text"
        Dim recpt_ds As DataSet = get_dataset("complaints", param2)
        For Each row In recpt_ds.Tables(0).Rows
            receipt_typecombobox.Items.Add(row(0))
        Next
        receipt_typecombobox.SelectedIndex = -1
        receipt_typecombobox.Text = ""
        search_receipt_type = ""

    End Sub

    Private Sub agentrbtn_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub against_combobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles recvd_fromcombobox.SelectedIndexChanged

    End Sub

    Private Sub recvd_fromcombobox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles recvd_fromcombobox.Validated

    End Sub

    Private Sub cat_combobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cat_combobox.SelectedIndexChanged

    End Sub

    Private Sub cat_combobox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles cat_combobox.Validated

    End Sub

    Private Sub cat_combobox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles cat_combobox.Validating
        If Microsoft.VisualBasic.Len(Trim(cat_combobox.Text)) = 0 Then
            cat_text = ""
        Else
            cat_text = cat_combobox.Text
            If cat_text <> "A" And cat_text <> "B" And cat_text <> "C" And cat_text <> "O" And cat_text <> "P" Then
                ErrorProvider1.SetError(cat_combobox, "Category must be A,B, C, O or P or blank")
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub recvd_fromcombobox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles recvd_fromcombobox.Validating
        recvd_text = recvd_fromcombobox.Text
        If Microsoft.VisualBasic.Len(Trim(recvd_text)) = 0 Then
            recvd_from = -1
        Else
            If recvd_fromcombobox.SelectedIndex = -1 Then
                ErrorProvider1.SetError(recvd_fromcombobox, "Pick one from list or leave blank")
                e.Cancel = True
            Else
                search_recvd_from = recvd_fromcombobox.SelectedIndex
                search_recvd_text = recvd_fromcombobox.Text
            End If
        End If
    End Sub

    Private Sub agentcombobox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles agentcombobox.Validated
        If search_agent_no = 0 Then
            Exit Sub
        End If
        Dim idx As Integer
        If agentcombobox.SelectedItem = "Various" Then
            agent1 = 9999
        Else
            For idx = 1 To agent_rows
                If search_bailiff_table(idx, 2) = agentcombobox.SelectedItem Then
                    If agent1 = -1 Then
                        agent1 = search_bailiff_table(idx, 1)
                    Else
                        agent2 = search_bailiff_table(idx, 1)
                        Exit For
                    End If
                End If
            Next
        End If
    End Sub

    Private Sub agentcombobox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles agentcombobox.Validating
        Dim agent_text As String = agentcombobox.Text
        agent1 = -1
        agent2 = -1
        If Microsoft.VisualBasic.Len(Trim(agent_text)) = 0 Then
            search_agent_no = 0
        Else
            If agentcombobox.SelectedIndex = -1 Then
                ErrorProvider1.SetError(agentcombobox, "Pick one from list or leave blank")
                e.Cancel = True
            Else
                search_agent_no = agentcombobox.SelectedIndex
                search_agent_text = agentcombobox.Text
            End If
        End If
    End Sub

    Private Sub dept_combobox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles against_combobox.Validated
    End Sub


    Private Sub dept_combobox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles against_combobox.Validating
        ErrorProvider1.SetError(against_combobox, "")
        search_dept = against_combobox.Text
        If Microsoft.VisualBasic.Len(Trim(search_dept)) = 0 Then
            search_dept = ""
        Else
            If search_dept.Length > 0 Then
                If against_combobox.SelectedIndex = -1 Then
                    ErrorProvider1.SetError(against_combobox, "Pick one from list or leave blank")
                    e.Cancel = True
                End If
            End If
        End If
    End Sub

    Private Sub find2btn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles find2btn.Click
        disable_buttons()
        get_letter = False
        start_search()
        enable_buttons()
    End Sub

    Private Sub disable_buttons()
        findbtn.Enabled = False
        find2btn.Enabled = False
        exitbtn.Enabled = False
    End Sub
    Private Sub enable_buttons()
        findbtn.Enabled = True
        find2btn.Enabled = True
        exitbtn.Enabled = True
    End Sub

    Private Sub stage_combobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles stage_combobox.SelectedIndexChanged

    End Sub

    Private Sub stage_combobox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles stage_combobox.Validated
        If Microsoft.VisualBasic.Len(Trim(stage_combobox.Text)) = 0 Then
            search_stage_text = ""
        Else
            search_stage_text = stage_combobox.Text
        End If
    End Sub

    Private Sub stage_combobox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles stage_combobox.Validating
        If Microsoft.VisualBasic.Len(Trim(stage_combobox.Text)) > 0 Then
            If stage_combobox.SelectedIndex < 0 Then
                ErrorProvider1.SetError(stage_combobox, "Stage must be blank or one from the list")
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub recvd_cbox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles recvd_cbox.CheckedChanged
        If recvd_cbox.Checked Then
            start_recvd_datepicker.Visible = True
            end_recvd_datepicker.Visible = True
            fromlbl.Visible = True
            tolbl.Visible = True
        Else
            start_recvd_datepicker.Visible = False
            end_recvd_datepicker.Visible = False
            fromlbl.Visible = False
            tolbl.Visible = True
        End If
    End Sub

    Private Sub comp_cbox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles comp_cbox.CheckedChanged
        If comp_cbox.Checked = True Then
            comp_datepicker.Visible = True
        Else
            comp_datepicker.Visible = False
        End If
    End Sub

    Private Sub dept_combobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles against_combobox.SelectedIndexChanged

    End Sub

    Private Sub receipt_typecombobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles receipt_typecombobox.SelectedIndexChanged

    End Sub

    Private Sub receipt_typecombobox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles receipt_typecombobox.Validated
        If Microsoft.VisualBasic.Len(Trim(receipt_typecombobox.Text)) = 0 Then
            search_receipt_type = ""
        Else
            search_receipt_type = receipt_typecombobox.Text
        End If
    End Sub

    Private Sub debt_typecombobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles debt_typecombobox.SelectedIndexChanged

    End Sub

    Private Sub debt_typecombobox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles debt_typecombobox.Validating

    End Sub

    Private Sub receipt_typecombobox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles receipt_typecombobox.Validating
        If Microsoft.VisualBasic.Len(Trim(receipt_typecombobox.Text)) > 0 Then
            If receipt_typecombobox.SelectedIndex < 0 Then
                ErrorProvider1.SetError(receipt_typecombobox, "Receipt Type must be blank or one from the list")
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub branch_cbox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles branch_cbox.Validated
        search_branch_no = branch_cbox.SelectedIndex
        search_branch_name = branch_cbox.Text
        against_combobox.Items.Clear()
        If search_branch_no = -1 Then
            'Me.Complaint_againstTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Complaint_against)
            param2 = "select against_text from Complaint_against" & _
            " order by against_text"
            Dim against_ds As DataSet = get_dataset("Complaints", param2)
            For Each row In against_ds.Tables(0).Rows
                against_combobox.Items.Add(row(0))
            Next
        Else
            'Me.Complaint_againstTableAdapter.FillBy2(Me.PraiseAndComplaintsSQLDataSet.Complaint_against, search_branch_no)

            param2 = "select against_text from Complaint_against" & _
            " where against_branch_no = " & search_branch_no & _
            " order by against_text"
            Dim against_ds As DataSet = get_dataset("Complaints", param2)
            For Each row In against_ds.Tables(0).Rows
                against_combobox.Items.Add(row(0))
            Next
        End If
        against_combobox.SelectedIndex = -1
        against_combobox.Text = ""
        search_dept = ""
    End Sub

    Private Sub branch_cbox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles branch_cbox.SelectedIndexChanged

    End Sub

    Private Sub agentcombobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles agentcombobox.SelectedIndexChanged

    End Sub

    Private Sub cmpny_cbox_Enter(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmpny_cbox.Enter

    End Sub

    Private Sub cmpny_cbox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmpny_cbox.SelectedIndexChanged
        branch_cbox.Text = ""
    End Sub

    Private Sub cmpny_cbox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmpny_cbox.Validated
        param2 = "select branch_name from Complaint_branches "
        If cmpny_cbox.Text <> "ALL" Then
            param2 &= " where(branch_cmpny_no = " & cmpny_cbox.SelectedIndex & ")"
        End If
        param2 &= " order by branch_name"
        Dim branch_ds As DataSet = get_dataset("Complaints", param2)
        Dim branchRow As DataRow
        branch_cbox.Items.Clear()
        For Each branchRow In branch_ds.Tables(0).Rows
            branch_cbox.Items.Add(branchRow(0))
        Next
        cmpny_no = cmpny_cbox.SelectedIndex

    End Sub

    Private Sub hold_rb_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles hold_rb.CheckedChanged

    End Sub

    Private Sub category_combobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles category_combobox.SelectedIndexChanged

    End Sub

    Private Sub name_textbox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles name_textbox.TextChanged

    End Sub
End Class
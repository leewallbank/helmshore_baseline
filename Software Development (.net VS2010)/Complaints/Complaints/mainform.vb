Public Class mainfrm
    '13.12.2010 when client selected, let scheme be selected as well if required
    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub


    Private Sub addbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles addbtn.Click
        add_compfrm.ShowDialog()
    End Sub

    Private Sub updmastbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles updmastbtn.Click
        updmastfrm.ShowDialog()
    End Sub

    Private Sub mainfrm_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        conn.Close()
    End Sub

    Private Sub dispbtn_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If admin_user = False Then
            updmastbtn.Enabled = False
            addbtn.Enabled = False
            nextbtn.Enabled = False
            updbtn.Enabled = False
        End If
    End Sub

    Private Sub updbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles updbtn.Click
        Try
            comp_no = InputBox("Enter complaint number")
        Catch ex As Exception
            MsgBox("enter a valid number")
            Exit Sub
        End Try
        updatecmpfrm.ShowDialog()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        displayfrm.ShowDialog()
    End Sub

    Private Sub outbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ProgressBar1.Visible = True
        search_no = 6
        MsgBox("What is this search?")
        displayfrm2.ShowDialog()
        ProgressBar1.Visible = False
    End Sub

    Private Sub logbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles logbtn.Click
        Dim resp
        Try
            resp = InputBox("Enter complaint number")
            If resp Is "" Then
                Exit Sub
            End If
            comp_no = resp
            If comp_no < 161 And env_str = "Prod" Then
                MsgBox("First complaint number is 161")
                Exit Sub
            End If
        Catch ex As Exception
            MsgBox("enter a valid number")
            Exit Sub
        End Try
        logfrm.ShowDialog()
    End Sub

    Private Sub nextbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles nextbtn.Click
        Try
            'get any priority unallocated complaint
            comp_no = 0
            Try
                comp_no = updatecmpfrm.ComplaintsTableAdapter.ScalarQuery1(cmpny_no)
            Catch ex As Exception
                'get oldest not priority
                Try
                    'comp_no = updatecmpfrm.ComplaintsTableAdapter.ScalarQuery2()

                    'first get none priority with a client number
                    Dim days_left As Integer
                    Dim last_days_left As Integer = 999
                    add_compfrm.ComplaintsTableAdapter.FillBy3(add_compfrm.PraiseAndComplaintsSQLDataSet.Complaints, cmpny_no)
                    Dim row As DataRow
                    Dim idx As Integer = 0
                    Dim cl_turn_days, days_taken As Integer
                    For Each row In add_compfrm.PraiseAndComplaintsSQLDataSet.Complaints.Rows
                        'see if override exists on client turnaround table
                        Dim cl_no As Integer = add_compfrm.PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(5)
                        If client_turnfrm.Client_turnaroundTableAdapter.FillBy(client_turnfrm.PraiseAndComplaintsSQLDataSet.Client_turnaround, cl_no) > 0 Then
                            cl_turn_days = client_turnfrm.PraiseAndComplaintsSQLDataSet.Client_turnaround.Rows(0).Item(1)
                        Else
                            cl_turn_days = 10
                        End If
                        days_taken = DateDiff("d", add_compfrm.PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(1), Now) - _
                                       (DateDiff("w", add_compfrm.PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(1), Now) * 2)
                        If Weekday(add_compfrm.PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(1)) > Weekday(Now) Then
                            days_taken = days_taken - 2
                        End If
                        days_left = cl_turn_days - days_taken
                        If days_left < last_days_left Then
                            last_days_left = days_left
                            comp_no = add_compfrm.PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(0)
                        End If
                        idx += 1
                    Next
                    'now get complaints with no client no
                    idx = 0
                    cl_turn_days = 10
                    add_compfrm.ComplaintsTableAdapter.FillBy4(add_compfrm.PraiseAndComplaintsSQLDataSet.Complaints, cmpny_no)
                    For Each row In add_compfrm.PraiseAndComplaintsSQLDataSet.Complaints.Rows
                        days_taken = DateDiff("d", add_compfrm.PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(1), Now) - _
                                                           (DateDiff("w", add_compfrm.PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(1), Now) * 2)
                        If Weekday(add_compfrm.PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(1)) > Weekday(Now) Then
                            days_taken = days_taken - 2
                        End If
                        days_left = cl_turn_days - days_taken
                        If days_left < last_days_left Then
                            last_days_left = days_left
                            comp_no = add_compfrm.PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(0)
                        End If
                        idx += 1
                    Next
                Catch ex2 As Exception
                    MessageBox.Show(ex.Message)
                End Try
            End Try
            If comp_no = 0 Then
                MessageBox.Show("No complaints to allocate")
                Exit Sub
            End If
            updatecmpfrm.ShowDialog()
        Catch ex As Exception
            MsgBox(ex.ToString)
            exception_string = ex.ToString
            Call error_log()
        End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If sqlCon.State = ConnectionState.Closed Then
            Connect_sqlDb()
        End If
        'upd_txt = "insert into Ethnicity (eth_code, eth_name) values (" & 18 & ",'N/A'" & ")"
        'update_sql(upd_txt)
        'upd_txt = "insert into complaints_type_code (type_code, type_name) values (" & 2 & ",'type code 2'" & ")"
        'update_sql(upd_txt)
        'upd_txt = "insert into stage_escalation (esc_name) values (" & "'stage escalation reason 2'" & ")"
        'update_sql(upd_txt)
        'upd_txt = "insert into actions (action_name) values (" & "'N/A'" & ")"
        'update_sql(upd_txt)
        'upd_txt = "insert into Complaints_resp_clients (comp_resp_cl_no) values (" & "50" & ")"
        'update_sql(upd_txt)
        'upd_txt = "insert into stage_escalation (esc_name) values (" & "'Standard reply'" & ")"
        'update_sql(upd_txt)
        'upd_txt = "insert into Complaint_categories (cat_code, cat_number, cat_text) values (" & "'A'" & "," & "2" & "," & "'A2test'" & ")"
        'update_sql(upd_txt)
        'upd_txt = "insert into Hold_reason (hold_name) values('N/A')"
        'update_sql(upd_txt)
        'upd_txt = "update Complaint_branches set branch_cmpny_no = 0"
        'update_sql(upd_txt)
        'upd_txt = "insert into Complaint_clients (clnt_name) values('Liverpool')"
        'update_sql(upd_txt)
        'upd_txt = "insert into Complaint_Agents (agnt_name) values('Sam Spade')"
        'update_sql(upd_txt)
        'upd_txt = "insert into Complaint_branches (branch_code, branch_name, branch_cmpny_no) values (" & 8 & ",'Swift'" & "," & "1)"
        'update_sql(upd_txt)
        'upd_txt = "insert into Complaint_branches (branch_code, branch_name, branch_cmpny_no) values (" & 9 & ",'Marston'" & "," & "2)"
        'update_sql(upd_txt)
        'upd_txt = "update Investigators set inv_cmpny_no = 0"
        'update_sql(upd_txt)
        'upd_txt = "insert into Complaint_Companies (cmpny_code, cmpny_name) values(1" & "," & "'Swift')"
        'update_sql(upd_txt)
        'upd_txt = "insert into Complaint_Companies (cmpny_code, cmpny_name) values(2" & "," & "'Marston')"
        'upd_txt = "insert into Complaint_branches (branch_code, branch_name, branch_cmpny_no) values (" & 8 & ",'Student Loans'" & "," & "0)"
        'update_sql(upd_txt)
        'upd_txt = "insert into Complaint_branches (branch_code, branch_name, branch_cmpny_no) values (" & 9 & ",'Leagal Aid - RCL'" & "," & "0)"
        'update_sql(upd_txt)
        'upd_txt = "insert into Complaint_categories (cat_code, cat_number, cat_text) values (" & "'A'" & "," & "99" & "," & "'Contentious'" & ")"
        'update_sql(upd_txt)
        'upd_txt = "insert into Complaints_feedback (feedback_code, feedback_name) values (" & 0 & ",'')"
        'update_sql(upd_txt)
        'upd_txt = "insert into Complaints_feedback (feedback_code, feedback_name) values (" & 1 & ",'feedback test')"
        'update_sql(upd_txt)
        'upd_txt = "insert into Complaint_categories (cat_code, cat_number, cat_text) values (" & "'A'" & "," & "9" & "," & "'type code test'" & ")"
        'update_sql(upd_txt)
        'upd_txt = "insert into Complaints_resp2_clients (comp_resp2_cl_no) values(54)"
        'update_sql(upd_txt)
        'upd_txt = "insert into Complaint_categories (cat_code, cat_number, cat_text) values (" & "'P'" & "," & "1" & "," & "'Test P'" & ")"
        'update_sql(upd_txt)
        Me.Close()
    End Sub
End Class

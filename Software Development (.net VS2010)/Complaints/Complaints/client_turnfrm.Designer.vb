<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class client_turnfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.DataGridView1 = New System.Windows.Forms.DataGridView
        Me.cl_no = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Client_name = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Turn_days = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ClientturnaroundBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PraiseAndComplaintsSQLDataSet = New Complaints.PraiseAndComplaintsSQLDataSet
        Me.Client_turnaroundTableAdapter = New Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.Client_turnaroundTableAdapter
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ClientturnaroundBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PraiseAndComplaintsSQLDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AllowUserToOrderColumns = True
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.cl_no, Me.Client_name, Me.Turn_days})
        Me.DataGridView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView1.Location = New System.Drawing.Point(0, 0)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(517, 468)
        Me.DataGridView1.TabIndex = 0
        '
        'cl_no
        '
        Me.cl_no.HeaderText = "Client No"
        Me.cl_no.Name = "cl_no"
        Me.cl_no.ReadOnly = True
        Me.cl_no.Width = 40
        '
        'Client_name
        '
        Me.Client_name.HeaderText = "Client Name"
        Me.Client_name.Name = "Client_name"
        Me.Client_name.ReadOnly = True
        Me.Client_name.Width = 300
        '
        'Turn_days
        '
        Me.Turn_days.HeaderText = "Turnaround days"
        Me.Turn_days.Name = "Turn_days"
        '
        'ClientturnaroundBindingSource
        '
        Me.ClientturnaroundBindingSource.DataMember = "Client_turnaround"
        Me.ClientturnaroundBindingSource.DataSource = Me.PraiseAndComplaintsSQLDataSet
        '
        'PraiseAndComplaintsSQLDataSet
        '
        Me.PraiseAndComplaintsSQLDataSet.DataSetName = "PraiseAndComplaintsSQLDataSet"
        Me.PraiseAndComplaintsSQLDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Client_turnaroundTableAdapter
        '
        Me.Client_turnaroundTableAdapter.ClearBeforeFill = True
        '
        'client_turnfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(517, 468)
        Me.Controls.Add(Me.DataGridView1)
        Me.Name = "client_turnfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Client Turnaround Days"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ClientturnaroundBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PraiseAndComplaintsSQLDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents PraiseAndComplaintsSQLDataSet As Complaints.PraiseAndComplaintsSQLDataSet
    Friend WithEvents ClientturnaroundBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Client_turnaroundTableAdapter As Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.Client_turnaroundTableAdapter
    Friend WithEvents cl_no As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Client_name As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Turn_days As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

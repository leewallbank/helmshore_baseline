<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class investdispfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.invdatagrid = New System.Windows.Forms.DataGridView
        Me.InvcodeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.InvtextDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.InvestigatorsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PraiseAndComplaintsSQLDataSet = New Complaints.PraiseAndComplaintsSQLDataSet
        Me.InvestigatorsTableAdapter = New Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.InvestigatorsTableAdapter
        CType(Me.invdatagrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InvestigatorsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PraiseAndComplaintsSQLDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'invdatagrid
        '
        Me.invdatagrid.AllowUserToAddRows = False
        Me.invdatagrid.AllowUserToDeleteRows = False
        Me.invdatagrid.AutoGenerateColumns = False
        Me.invdatagrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.invdatagrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.InvcodeDataGridViewTextBoxColumn, Me.InvtextDataGridViewTextBoxColumn})
        Me.invdatagrid.DataSource = Me.InvestigatorsBindingSource
        Me.invdatagrid.Dock = System.Windows.Forms.DockStyle.Fill
        Me.invdatagrid.Location = New System.Drawing.Point(0, 0)
        Me.invdatagrid.Name = "invdatagrid"
        Me.invdatagrid.ReadOnly = True
        Me.invdatagrid.Size = New System.Drawing.Size(348, 266)
        Me.invdatagrid.TabIndex = 0
        '
        'InvcodeDataGridViewTextBoxColumn
        '
        Me.InvcodeDataGridViewTextBoxColumn.DataPropertyName = "inv_code"
        Me.InvcodeDataGridViewTextBoxColumn.HeaderText = "Code"
        Me.InvcodeDataGridViewTextBoxColumn.Name = "InvcodeDataGridViewTextBoxColumn"
        Me.InvcodeDataGridViewTextBoxColumn.ReadOnly = True
        Me.InvcodeDataGridViewTextBoxColumn.Width = 50
        '
        'InvtextDataGridViewTextBoxColumn
        '
        Me.InvtextDataGridViewTextBoxColumn.DataPropertyName = "inv_text"
        Me.InvtextDataGridViewTextBoxColumn.HeaderText = "Text"
        Me.InvtextDataGridViewTextBoxColumn.Name = "InvtextDataGridViewTextBoxColumn"
        Me.InvtextDataGridViewTextBoxColumn.ReadOnly = True
        Me.InvtextDataGridViewTextBoxColumn.Width = 250
        '
        'InvestigatorsBindingSource
        '
        Me.InvestigatorsBindingSource.DataMember = "Investigators"
        Me.InvestigatorsBindingSource.DataSource = Me.PraiseAndComplaintsSQLDataSet
        '
        'PraiseAndComplaintsSQLDataSet
        '
        Me.PraiseAndComplaintsSQLDataSet.DataSetName = "PraiseAndComplaintsSQLDataSet"
        Me.PraiseAndComplaintsSQLDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'InvestigatorsTableAdapter
        '
        Me.InvestigatorsTableAdapter.ClearBeforeFill = True
        '
        'investdispfrm
        '
        Me.ClientSize = New System.Drawing.Size(348, 266)
        Me.Controls.Add(Me.invdatagrid)
        Me.Name = "investdispfrm"
        Me.Text = "double click on investigator"
        CType(Me.invdatagrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InvestigatorsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PraiseAndComplaintsSQLDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents invdatagrid As System.Windows.Forms.DataGridView
    Friend WithEvents PraiseAndComplaintsSQLDataSet As Complaints.PraiseAndComplaintsSQLDataSet
    Friend WithEvents InvestigatorsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents InvestigatorsTableAdapter As Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.InvestigatorsTableAdapter
    Friend WithEvents InvcodeDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents InvtextDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    'Friend WithEvents InvestigatorsDataSetBindingSource As System.Windows.Forms.BindingSource
    'Friend WithEvents InvestigatorsDataSet As Complaints.InvestigatorsDataSet
    'Friend WithEvents InvestigatorsBindingSource As System.Windows.Forms.BindingSource
    'Friend WithEvents InvestigatorsTableAdapter As Complaints.InvestigatorsDataSetTableAdapters.InvestigatorsTableAdapter
    'Friend WithEvents InvcodeDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    'Friend WithEvents InvtextDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class investfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.inv_dg = New System.Windows.Forms.DataGridView
        Me.InvestigatorsBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.PraiseAndComplaintsSQLDataSet = New Complaints.PraiseAndComplaintsSQLDataSet
        Me.InvestigatorsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.InvestigatorsTableAdapter = New Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.InvestigatorsTableAdapter
        Me.ComplaintsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ComplaintsTableAdapter = New Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.ComplaintsTableAdapter
        Me.Inv_code_dg = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Inv_text_dg = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.inv_cmpny_name = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.inv_admin = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.inv_deleted = New System.Windows.Forms.DataGridViewCheckBoxColumn
        CType(Me.inv_dg, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InvestigatorsBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PraiseAndComplaintsSQLDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InvestigatorsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComplaintsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'inv_dg
        '
        Me.inv_dg.AllowUserToDeleteRows = False
        Me.inv_dg.AllowUserToOrderColumns = True
        Me.inv_dg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.inv_dg.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Inv_code_dg, Me.Inv_text_dg, Me.inv_cmpny_name, Me.inv_admin, Me.inv_deleted})
        Me.inv_dg.Dock = System.Windows.Forms.DockStyle.Fill
        Me.inv_dg.Location = New System.Drawing.Point(0, 0)
        Me.inv_dg.Name = "inv_dg"
        Me.inv_dg.Size = New System.Drawing.Size(552, 572)
        Me.inv_dg.TabIndex = 0
        '
        'InvestigatorsBindingSource1
        '
        Me.InvestigatorsBindingSource1.DataMember = "Investigators"
        Me.InvestigatorsBindingSource1.DataSource = Me.PraiseAndComplaintsSQLDataSet
        '
        'PraiseAndComplaintsSQLDataSet
        '
        Me.PraiseAndComplaintsSQLDataSet.DataSetName = "PraiseAndComplaintsSQLDataSet"
        Me.PraiseAndComplaintsSQLDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'InvestigatorsBindingSource
        '
        Me.InvestigatorsBindingSource.DataMember = "Investigators"
        Me.InvestigatorsBindingSource.DataSource = Me.PraiseAndComplaintsSQLDataSet
        '
        'InvestigatorsTableAdapter
        '
        Me.InvestigatorsTableAdapter.ClearBeforeFill = True
        '
        'ComplaintsBindingSource
        '
        Me.ComplaintsBindingSource.DataMember = "Complaints"
        Me.ComplaintsBindingSource.DataSource = Me.PraiseAndComplaintsSQLDataSet
        '
        'ComplaintsTableAdapter
        '
        Me.ComplaintsTableAdapter.ClearBeforeFill = True
        '
        'Inv_code_dg
        '
        Me.Inv_code_dg.HeaderText = "Inv Code"
        Me.Inv_code_dg.Name = "Inv_code_dg"
        Me.Inv_code_dg.Visible = False
        '
        'Inv_text_dg
        '
        Me.Inv_text_dg.HeaderText = "Name"
        Me.Inv_text_dg.Name = "Inv_text_dg"
        '
        'inv_cmpny_name
        '
        Me.inv_cmpny_name.HeaderText = "Company"
        Me.inv_cmpny_name.Items.AddRange(New Object() {"Rossendales", "Swift", "Marston"})
        Me.inv_cmpny_name.Name = "inv_cmpny_name"
        Me.inv_cmpny_name.Width = 175
        '
        'inv_admin
        '
        Me.inv_admin.DataPropertyName = "inv_admin"
        Me.inv_admin.HeaderText = "Update"
        Me.inv_admin.Name = "inv_admin"
        '
        'inv_deleted
        '
        Me.inv_deleted.HeaderText = "Deleted"
        Me.inv_deleted.Name = "inv_deleted"
        Me.inv_deleted.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.inv_deleted.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'investfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(552, 572)
        Me.Controls.Add(Me.inv_dg)
        Me.Name = "investfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Complaints Officers"
        CType(Me.inv_dg, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InvestigatorsBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PraiseAndComplaintsSQLDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InvestigatorsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComplaintsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents inv_dg As System.Windows.Forms.DataGridView
    Friend WithEvents PraiseAndComplaintsSQLDataSet As Complaints.PraiseAndComplaintsSQLDataSet
    Friend WithEvents InvestigatorsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents InvestigatorsTableAdapter As Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.InvestigatorsTableAdapter
    Friend WithEvents InvestigatorsBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents ComplaintsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ComplaintsTableAdapter As Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.ComplaintsTableAdapter
    Friend WithEvents Inv_code_dg As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Inv_text_dg As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents inv_cmpny_name As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents inv_admin As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents inv_deleted As System.Windows.Forms.DataGridViewCheckBoxColumn
End Class

Public Class logfrm

    Private Sub logfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        compnotextbox.Text = "Complaint No " & comp_no
        If env_str <> "Prod" Then
            updatecmpfrm.Test_LogTableAdapter.FillBy(updatecmpfrm.Test_LogDataSet.Test_Log, comp_no)
        Else
            Me.LogTableAdapter.FillBy(Me.PraiseAndComplaintsSQLDataSet.Log, comp_no)
        End If
        Dim row As DataRow
        DataGridView1.Rows.Clear()
        If env_str <> "Prod" Then
            For Each row In updatecmpfrm.Test_LogDataSet.Test_Log.Rows
                Dim idx As Integer
                Dim log_date As String = updatecmpfrm.Test_LogDataSet.Test_Log.Rows(idx).Item(2)
                Dim log_code As Integer = updatecmpfrm.Test_LogDataSet.Test_Log.Rows(idx).Item(1)
                Me.InvestigatorsTableAdapter.FillBy(Me.PraiseAndComplaintsSQLDataSet.Investigators, log_code)
                Dim log_user As String = PraiseAndComplaintsSQLDataSet.Investigators.Rows(0).Item(1)
                Dim log_type As String = updatecmpfrm.Test_LogDataSet.Test_Log.Rows(idx).Item(3)
                Dim log_text As String = updatecmpfrm.Test_LogDataSet.Test_Log.Rows(idx).Item(5)
                DataGridView1.Rows.Add(log_date, log_user, log_type, log_text)
                idx += 1
            Next
        Else
            For Each row In Me.PraiseAndComplaintsSQLDataSet.Log.Rows
                Dim idx As Integer
                Dim log_date As String = PraiseAndComplaintsSQLDataSet.Log.Rows(idx).Item(2)
                Dim log_code As Integer = PraiseAndComplaintsSQLDataSet.Log.Rows(idx).Item(1)
                Me.InvestigatorsTableAdapter.FillBy(Me.PraiseAndComplaintsSQLDataSet.Investigators, log_code)
                Dim log_user As String = PraiseAndComplaintsSQLDataSet.Investigators.Rows(0).Item(1)
                Dim log_type As String = PraiseAndComplaintsSQLDataSet.Log.Rows(idx).Item(3)
                Dim log_text As String = PraiseAndComplaintsSQLDataSet.Log.Rows(idx).Item(5)
                DataGridView1.Rows.Add(log_date, log_user, log_type, log_text)
                idx += 1
            Next
        End If
    End Sub

End Class
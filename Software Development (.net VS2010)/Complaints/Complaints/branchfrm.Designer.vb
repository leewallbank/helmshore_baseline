<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class branchfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.branch_cbox = New System.Windows.Forms.ComboBox
        Me.ComplaintbranchesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PraiseAndComplaintsSQLDataSet = New Complaints.PraiseAndComplaintsSQLDataSet
        Me.Label1 = New System.Windows.Forms.Label
        Me.Complaint_branchesTableAdapter = New Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.Complaint_branchesTableAdapter
        Me.exitbtn = New System.Windows.Forms.Button
        CType(Me.ComplaintbranchesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PraiseAndComplaintsSQLDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'branch_cbox
        '
        Me.branch_cbox.FormattingEnabled = True
        Me.branch_cbox.Location = New System.Drawing.Point(59, 89)
        Me.branch_cbox.Name = "branch_cbox"
        Me.branch_cbox.Size = New System.Drawing.Size(141, 21)
        Me.branch_cbox.TabIndex = 0
        '
        'ComplaintbranchesBindingSource
        '
        Me.ComplaintbranchesBindingSource.DataMember = "Complaint_branches"
        Me.ComplaintbranchesBindingSource.DataSource = Me.PraiseAndComplaintsSQLDataSet
        '
        'PraiseAndComplaintsSQLDataSet
        '
        Me.PraiseAndComplaintsSQLDataSet.DataSetName = "PraiseAndComplaintsSQLDataSet"
        Me.PraiseAndComplaintsSQLDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(78, 57)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(122, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Select Branch to update"
        '
        'Complaint_branchesTableAdapter
        '
        Me.Complaint_branchesTableAdapter.ClearBeforeFill = True
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(171, 176)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 2
        Me.exitbtn.Text = "Select"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'branchfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(292, 266)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.branch_cbox)
        Me.Name = "branchfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Select Branch"
        CType(Me.ComplaintbranchesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PraiseAndComplaintsSQLDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents branch_cbox As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents PraiseAndComplaintsSQLDataSet As Complaints.PraiseAndComplaintsSQLDataSet
    Friend WithEvents ComplaintbranchesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Complaint_branchesTableAdapter As Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.Complaint_branchesTableAdapter
    Friend WithEvents exitbtn As System.Windows.Forms.Button
End Class

Public Class deldocfrm

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub doc_ListBox_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles doc_ListBox.DoubleClick
        Dim path As String
        Dim new_path As String

        If env_str <> "Prod" Then
            path = "r:\complaints\Test\" & comp_no & "\" & doc_ListBox.Text
            new_path = "r:\complaints\Test\" & comp_no & "\" & doc_ListBox.Text & ".del"
        Else
            path = "r:\complaints\" & comp_no & "\" & doc_ListBox.Text
            new_path = "r:\complaints\" & comp_no & "\" & doc_ListBox.Text & ".del"
        End If
        If MsgBox("Delete document " & doc_ListBox.Text, MsgBoxStyle.YesNo, "Delete a document") = MsgBoxResult.Yes Then
            Try
                Rename(path, new_path)
            Catch ex As Exception
                If env_str <> "Prod" Then
                    new_path = "r:\complaints\Test\" & comp_no & "\" & doc_ListBox.Text & "x.del"
                Else
                    new_path = "r:\complaints\" & comp_no & "\" & doc_ListBox.Text & "x.del"
                End If
                Try
                    Rename(path, new_path)
                Catch ex2 As Exception
                    MessageBox.Show("Contact IT to delete document")
                    Me.Close()
                End Try
                Dim no_of_docs As Integer = populate_document_combobox_del()
                Me.Close()
            End Try
        End If
        log_text = "Document deleted " & doc_ListBox.Text
        updmastfrm.LogTableAdapter.InsertQuery(log_code, Now, "document deleted", comp_no, log_text)
        doc_change_made = True
        Me.Close()
    End Sub

    Private Sub deldocfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim no_of_docs As Integer = populate_document_combobox_del()
    End Sub

    Private Sub doc_ListBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles doc_ListBox.SelectedIndexChanged

    End Sub
End Class
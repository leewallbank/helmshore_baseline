Public Class NotifyClientfrm
    Dim cl_id As Integer
    Private Sub NotifyClientfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Complaints_resp_clientsTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Complaints_resp_clients)
        updmastfrm.ProgressBar1.Visible = True
        DataGridView1.Rows.Clear()
        Dim idx As Integer
        Dim cl_notify_rows As Integer = 0
        Me.Complaints_resp_clientsTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Complaints_resp_clients)
        cl_notify_rows = Me.PraiseAndComplaintsSQLDataSet.Complaints_resp_clients.Rows.Count
        For idx = 0 To cl_notify_rows - 1
            Dim cl_no As Integer = Me.PraiseAndComplaintsSQLDataSet.Complaints_resp_clients.Rows(idx).Item(0)
            Dim cl_name As String = ""
            param1 = "onestep"
            param2 = "select name from Client where _rowid = " & cl_no
            Dim cl_dataset As DataSet = get_dataset(param1, param2)
            If no_of_rows <> 1 Then
                MsgBox("Unable to access client number " & cl_no)
                Exit Sub
            End If
            cl_name = cl_dataset.Tables(0).Rows(0).Item(0)
            DataGridView1.Rows.Add(cl_no, cl_name)
            updmastfrm.ProgressBar1.Value = idx / cl_notify_rows * 100
        Next
        
    End Sub

    Private Sub DataGridView1_CellValidating(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellValidatingEventArgs) Handles DataGridView1.CellValidating
        If e.ColumnIndex = 0 Then
            Try
                Dim new_row As Boolean = DataGridView1.Rows(e.RowIndex + 1).IsNewRow
            Catch ex As Exception
                Exit Sub
            End Try
            If DataGridView1.Rows(e.RowIndex + 1).IsNewRow Then
                If e.FormattedValue = 0 Then
                    Exit Sub
                End If
                param1 = "onestep"
                param2 = "select name from Client where _rowid = " & e.FormattedValue
                Dim cl_dataset As DataSet = get_dataset(param1, param2)
                If no_of_rows <> 1 Then
                    MsgBox("Client ID not found on onestep")
                    e.Cancel = True
                    Exit Sub
                End If
                DataGridView1.Rows(e.RowIndex).Cells(1).Value = Trim(cl_dataset.Tables(0).Rows(0).Item(0).ToString)
            Else
                If e.FormattedValue <> DataGridView1.Rows(e.RowIndex).Cells(e.ColumnIndex).Value Then
                    MsgBox("Can't update client number - change it back to " & DataGridView1.Rows(e.RowIndex).Cells(e.ColumnIndex).Value)
                    e.Cancel = True
                End If
            End If
        End If
    End Sub
    Private Sub DataGridView1_RowEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.RowEnter
        cl_id = DataGridView1.Rows(e.RowIndex).Cells(0).Value
    End Sub

    Private Sub DataGridView1_RowsRemoved(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowsRemovedEventArgs) Handles DataGridView1.RowsRemoved
        If e.RowIndex >= 0 And cl_id > 0 Then
            Try
                Complaints_resp_clientsTableAdapter.DeleteQuery(cl_id)
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If
    End Sub
    Private Sub DataGridView1_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellValueChanged
        If e.RowIndex < 0 Then
            Exit Sub
        End If
        cl_id = DataGridView1.Rows(e.RowIndex).Cells(0).Value
        If cl_id = 0 Then
            Exit Sub
        End If
        Try
            Complaints_resp_clientsTableAdapter.InsertQuery(cl_id)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Private Sub clientsfrm_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        cl_id = 0
    End Sub

    Private Sub findbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles findbtn.Click
        first_letters = InputBox("Enter first part of client name", "Select client")
        first_letters = Trim(first_letters)
        first_letters = UCase(Microsoft.VisualBasic.Left(first_letters, 1)) & _
        Microsoft.VisualBasic.Right(first_letters, first_letters.Length - 1)
        If first_letters.Length = 0 Then
            MsgBox("NO Letters entered")
            Exit Sub
        End If
        clientfrm.ShowDialog()
    End Sub
End Class
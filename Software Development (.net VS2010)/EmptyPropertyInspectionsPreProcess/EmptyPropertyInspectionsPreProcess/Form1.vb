﻿Imports System.IO

Public Class Form1
    Private InputFilePath As String, FileName As String, FileExt As String, OutFile As String
    Private Sub exitbtn_Click(sender As System.Object, e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub readbtn_Click(sender As System.Object, e As System.EventArgs) Handles readbtn.Click
        Dim FileDialog As New OpenFileDialog
        Dim progressbar As New ProgressBar
        If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

        InputFilePath = Path.GetDirectoryName(FileDialog.FileName)
        InputFilePath &= "\"
        FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
        FileExt = Path.GetExtension(FileDialog.FileName)
        OutFile = "Account No|Property Details|Account Name|Rateable value|VOA Desc|prop Exemption desc|Prop Add Line1|Prop Addr Line2|" & _
            "Prop Addr Line3|Prop AddrLine4|Prop Addr PCode|Debt Addr Line1|Debt Addr Line2|Debt Addr Line3|Debt Addr Line4|Debt Addr PCode|" & _
            "Empty From Date|Empty to Date" & vbNewLine

        'get all docs in same folder
        Dim recordNumber As Integer = 0
        For Each foundFile As String In My.Computer.FileSystem.GetFiles _
              (InputFilePath, FileIO.SearchOption.SearchTopLevelOnly, "*.doc*")
            Dim FileContents() As String = System.IO.File.ReadAllLines(foundFile)
            Dim LineNumber As Integer
            Dim skipLines As Integer = 0
            Dim AccountFound As Boolean = False
            Dim inputLine As String

            For LineNumber = 0 To UBound(FileContents)
                inputLine = FileContents(LineNumber)
                'get account No
                If AccountFound = False Then
                    If InStr(inputLine, "Account No.") > 0 Then
                        AccountFound = True
                    End If
                    Continue For
                End If
                'skip next 2 lines
                LineNumber += 2
                inputLine = FileContents(LineNumber)
                'account no on this line cols 1-10
                Dim AccountNo As String = Trim(Microsoft.VisualBasic.Left(inputLine, 10))
                'property details 12-28
                Dim propDetails As String = Trim(Mid(inputLine, 12, 16))

                Dim rateableValue As String = Trim(Mid(inputLine, 45, 47))
                Dim propDesc As String = Trim(Mid(inputLine, 47, 22))
                Dim propExemptDesc As String = Trim(Microsoft.VisualBasic.Right(inputLine, inputLine.Length - 119))
                'account name is on next line cols 47+
                LineNumber += 1
                inputLine = FileContents(LineNumber)
                Dim accountName As String = Trim(Microsoft.VisualBasic.Right(inputLine, inputLine.Length - 46))

                'next line may be blank or may contain rest of account Name!
                LineNumber += 1
                inputLine = FileContents(LineNumber)
                If inputLine.Length > 0 Then
                    accountName &= " " & Trim(inputLine)
                    accountName = Trim(accountName)
                End If
                'next line is address lines 1 of property addr and debt addr
                LineNumber += 1
                inputLine = FileContents(LineNumber)
                Dim propAddressLine1 As String = Trim(Mid(inputLine, 14, 32))
                Dim debtAddressLine1 As String = Trim(Microsoft.VisualBasic.Right(inputLine, inputLine.Length - 46))

                'next line is address lines2
                LineNumber += 1
                inputLine = FileContents(LineNumber)
                Dim propAddressLine2 As String = Trim(Mid(inputLine, 14, 32))
                Dim debtAddressLine2 As String = Trim(Microsoft.VisualBasic.Right(inputLine, inputLine.Length - 46))

                'next line is address lines3
                LineNumber += 1
                inputLine = FileContents(LineNumber)
                Dim propAddressLine3 As String = Trim(Mid(inputLine, 14, 32))
                Dim debtAddressLine3 As String = Trim(Microsoft.VisualBasic.Right(inputLine, inputLine.Length - 46))

                'next line is address lines4
                LineNumber += 1
                inputLine = FileContents(LineNumber)
                Dim propAddressLine4 As String = Trim(Mid(inputLine, 14, 32))
                Dim debtAddressLine4 As String = Trim(Microsoft.VisualBasic.Right(inputLine, inputLine.Length - 46))

                'next line is postcodes
                LineNumber += 1
                inputLine = FileContents(LineNumber)
                Dim propAddressPCode As String = Trim(Mid(inputLine, 14, 32))
                Dim debtAddressPCode As String = Trim(Microsoft.VisualBasic.Right(inputLine, inputLine.Length - 46))



                'ignore next 2 lines then get empty from date
                LineNumber += 3
                inputLine = FileContents(LineNumber)
                Dim emptyFrom As String = Trim(Mid(inputLine, 75, 10))

                'ignore 1 line then next line is empty to date
                LineNumber += 2
                inputLine = FileContents(LineNumber)
                Dim emptyTo As String = Trim(Mid(inputLine, 75, 10))

                OutFile &= AccountNo & "|" & propDetails & "|" & accountName & "|" & rateableValue & "|" & propDesc & "|" & propExemptDesc & "|" & _
                   propAddressLine1 & "|" & propAddressLine2 & "|" & propAddressLine3 & "|" & propAddressLine4 & "|" & propAddressPCode & "|" & _
                   debtAddressLine1 & "|" & debtAddressLine2 & "|" & debtAddressLine3 & "|" & debtAddressLine4 & "|" & debtAddressPCode & "|" & _
                emptyFrom & "|" & emptyTo & vbNewLine


                'continue to next account number
                AccountFound = False
                recordNumber += 1
            Next
        Next


       

        Dim outFileNAme As String = InputFilePath & FileName & "_preprocess.txt"
        File.WriteAllText(outFileNAme, OutFile)
        MsgBox("File created with " & recordNumber & " records")
        Me.Close()
    End Sub
End Class
